import { isDevMode } from '@angular/core';

import { Action, ActionReducerMap } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';
import { storeFreeze } from 'ngrx-store-freeze';
import { createSelector } from 'reselect';

import {
  IAuthLoginPanel,
  IAuthState, IAdminArcGISSourcesState, IAdminWMSSourcesState,
  IAdminLayersState, IAdminThemesState,
  IThemesState, ILayersState, ILayersBrowserState, IMapsListState, IMapBuilderState,
  IArcGISSource, IArcGISSourceAvabilableLayer, IWMSSource, IWMSSourceAvabilableLayer,
  ILayer, ILayerTheme, ILayerTreeItem, IUser,
  IMap, IMapState, IMapLayer, IMapSelectedFeatures, IMapShowFeatureInfo,
  IPersistentMap, MapCenter, MapImageFormat,
  IGalleryTheme, IGalleryItem, IGalleryState,
  IAdminGalleryThemesState, IAdminGalleryItemsState,
  IAdminUsersState, MapBaseLayer,
  State
} from '../interfaces/index';
import * as fromAuth from './auth';
import * as fromAdminThemes from './admin-themes';
import * as fromAdminArcGISSources from './admin-arcgis-sources';
import * as fromAdminWMSSources from './admin-wms-sources';
import * as fromAdminLayers from './admin-layers';
import * as fromAdminGalleryThemes from './admin-gallery-themes';
import * as fromAdminGalleryItems from './admin-gallery-items';
import * as fromAdminUsers from './admin-users';
import * as fromMap from './map';
import * as fromThemes from './themes';
import * as fromLayers from './layers';
import * as fromLayersBrowser from './layers-browser';
import * as fromMapsList from './maps-list';
import * as fromMapBuilder from './map-builder';
import * as fromGallery from './gallery';


export const reducers: ActionReducerMap<State> = {
  auth: fromAuth.authReducer,
  router: routerReducer,
  adminArcGISSources: fromAdminArcGISSources.adminArcGISSourcesReducer,
  adminWmsSources: fromAdminWMSSources.adminWMSSourcesReducer,
  adminLayers: fromAdminLayers.adminLayersReducer,
  adminThemes: fromAdminThemes.adminThemesReducer,
  adminGalleryThemes: fromAdminGalleryThemes.adminGalleryThemesReducer,
  adminGalleryItems: fromAdminGalleryItems.adminGalleryItemsReducer,
  adminUsers: fromAdminUsers.adminUsersReducer,
  map: fromMap.mapReducer,
  themes: fromThemes.themesReducer,
  layers: fromLayers.layersReducer,
  layersBrowser: fromLayersBrowser.layersBrowserReducer,
  mapsList: fromMapsList.mapsListReducer,
  mapBuilder: fromMapBuilder.mapBuilderReducer,
  gallery: fromGallery.galleryReducer
};


// ADMIN ARCGIS SOURCES REDUCERS

export function getAdminArcGISSourcesState(state: State): IAdminArcGISSourcesState {
  return state.adminArcGISSources;
}
export const getAdminArcGISSources =
  createSelector<State, IArcGISSource[], IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSources
  );
export const getAdminArcGISSourcesLoading =
  createSelector<State, boolean, IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourcesLoading
  );
export const getAdminArcGISSourcesLayers =
  createSelector<State, IArcGISSourceAvabilableLayer[], IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourcesLayers
  );
export const getAdminArcGISSourcesLayersLoading =
  createSelector<State, boolean, IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourcesLayersLoading
  );
export const getAdminArcGISSourcesLayer =
  createSelector<State, IArcGISSourceAvabilableLayer | null, IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourcesLayer
  );
export const getAdminArcGISSourcesLayerLoading =
  createSelector<State, boolean, IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourcesLayerLoading
  );
export const getAdminArcGISSource =
  createSelector<State, IArcGISSource | null, IAdminArcGISSourcesState>(
    getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSource
  );
export const getAdminArcGISSourceLoading = createSelector<State, boolean, IAdminArcGISSourcesState>(
  getAdminArcGISSourcesState, fromAdminArcGISSources.getAdminArcGISSourceLoading
);


// ADMIN WMS SOURCES REDUCERS

export function getAdminWMSSourcesState(state: State): IAdminWMSSourcesState {
  return state.adminWmsSources;
}
export const getAdminWMSSources = createSelector<State, IWMSSource[], IAdminWMSSourcesState>(
  getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSources
);
export const getAdminWMSSourcesLoading = createSelector<State, boolean, IAdminWMSSourcesState>(
  getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourcesLoading
);
export const getAdminWMSSourcesLayers =
  createSelector<State, IWMSSourceAvabilableLayer[], IAdminWMSSourcesState>(
    getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourcesLayers
  );
export const getAdminWMSSourcesLayersLoading =
  createSelector<State, boolean, IAdminWMSSourcesState>(
    getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourcesLayersLoading
  );
export const getAdminWMSSourcesLayer =
  createSelector<State, IWMSSourceAvabilableLayer | null, IAdminWMSSourcesState>(
    getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourcesLayer
  );
export const getAdminWMSSourcesLayerLoading =
  createSelector<State, boolean, IAdminWMSSourcesState>(
    getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourcesLayerLoading
  );
export const getAdminWMSSource = createSelector<State, IWMSSource | null, IAdminWMSSourcesState>(
  getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSource
);
export const getAdminWMSSourceLoading = createSelector<State, boolean, IAdminWMSSourcesState>(
  getAdminWMSSourcesState, fromAdminWMSSources.getAdminWMSSourceLoading
);


// ADMIN LAYERS REDUCERS

export function getAdminLayersState(state: State): IAdminLayersState {
  return state.adminLayers;
}
export const getAdminLayers = createSelector<State, ILayer[], IAdminLayersState>(
  getAdminLayersState, fromAdminLayers.getAdminLayers
);
export const getAdminLayersLoading = createSelector<State, boolean, IAdminLayersState>(
  getAdminLayersState, fromAdminLayers.getAdminLayersLoading
);
export const getAdminLayer = createSelector<State, ILayer | null, IAdminLayersState>(
  getAdminLayersState, fromAdminLayers.getAdminLayer
);
export const getAdminLayerLoading = createSelector<State, boolean, IAdminLayersState>(
  getAdminLayersState, fromAdminLayers.getAdminLayerLoading
);


// ADMIN THEMES REDUCERS

export function getAdminThemesState(state: State): IAdminThemesState {
  return state.adminThemes;
}
export const getAdminTheme = createSelector<State, ILayerTheme | null, IAdminThemesState>(
  getAdminThemesState, fromAdminThemes.getAdminTheme
);
export const getAdminThemeLoading = createSelector<State, boolean, IAdminThemesState>(
  getAdminThemesState, fromAdminThemes.getAdminThemeLoading
);
export const getAdminThemes = createSelector<State, ILayerTheme[], IAdminThemesState>(
  getAdminThemesState, fromAdminThemes.getAdminThemes
);
export const getAdminThemesLoading = createSelector<State, boolean, IAdminThemesState>(
  getAdminThemesState, fromAdminThemes.getAdminThemesLoading
);


// ADMIN GALLERY THEMES REDUCERS

export function getAdminGalleryThemesState(state: State): IAdminGalleryThemesState {
  return state.adminGalleryThemes;
}
export const getAdminGalleryTheme =
  createSelector<State, IGalleryTheme | null, IAdminGalleryThemesState>(
    getAdminGalleryThemesState, fromAdminGalleryThemes.getAdminGalleryTheme
  );
export const getAdminGalleryThemeLoading = createSelector<State, boolean, IAdminGalleryThemesState>(
  getAdminGalleryThemesState, fromAdminGalleryThemes.getAdminGalleryThemeLoading
);
export const getAdminGalleryThemes =
  createSelector<State, IGalleryTheme[], IAdminGalleryThemesState>(
    getAdminGalleryThemesState, fromAdminGalleryThemes.getAdminGalleryThemes
  );
export const getAdminGalleryThemesLoading =
  createSelector<State, boolean, IAdminGalleryThemesState>(
    getAdminGalleryThemesState, fromAdminGalleryThemes.getAdminGalleryThemesLoading
  );


// ADMIN GALLERY ITEMS REDUCERS

export function getAdminGalleryItemsState(state: State): IAdminGalleryItemsState {
  return state.adminGalleryItems;
}
export const getAdminGalleryItem =
  createSelector<State, IGalleryItem | null, IAdminGalleryItemsState>(
    getAdminGalleryItemsState, fromAdminGalleryItems.getAdminGalleryItem
  );
export const getAdminGalleryItemLoading = createSelector<State, boolean, IAdminGalleryItemsState>(
  getAdminGalleryItemsState, fromAdminGalleryItems.getAdminGalleryItemLoading
);
export const getAdminGalleryItems =
  createSelector<State, IGalleryItem[], IAdminGalleryItemsState>(
    getAdminGalleryItemsState, fromAdminGalleryItems.getAdminGalleryItems
  );
export const getAdminGalleryItemsLoading =
  createSelector<State, boolean, IAdminGalleryItemsState>(
    getAdminGalleryItemsState, fromAdminGalleryItems.getAdminGalleryItemsLoading
  );


// ADMIN USERS REDUCERS

export function getAdminUsersState(state: State): IAdminUsersState {
  return state.adminUsers;
}
export const getAdminUser = createSelector<State, IUser | null, IAdminUsersState>(
  getAdminUsersState, fromAdminUsers.getAdminUser
);
export const getAdminUserLoading = createSelector<State, boolean, IAdminUsersState>(
  getAdminUsersState, fromAdminUsers.getAdminUserLoading
);
export const getAdminUsers =
  createSelector<State, IUser[], IAdminUsersState>(
    getAdminUsersState, fromAdminUsers.getAdminUsers
  );
export const getAdminUsersLoading =
  createSelector<State, boolean, IAdminUsersState>(
    getAdminUsersState, fromAdminUsers.getAdminUsersLoading
  );


// AUTH REDUCERS

export function getAuthState(state: State): IAuthState {
  return state.auth;
}
export const getLoginDialogVisible = createSelector<State, boolean, IAuthState>(
  getAuthState, fromAuth.getLoginDialogVisible
);
export const getLoginPanel = createSelector<State, IAuthLoginPanel, IAuthState>(
  getAuthState, fromAuth.getLoginPanel
);
export const getLoggedIn = createSelector<State, boolean, IAuthState>(
  getAuthState, fromAuth.getLoggedIn
);
export const getAuthToken = createSelector<State, string | null, IAuthState>(
  getAuthState, fromAuth.getAuthToken
);
export const getAuthLoading = createSelector<State, boolean, IAuthState>(
  getAuthState, fromAuth.getAuthLoading
);
export const getAuthUser = createSelector<State, IUser | null, IAuthState>(
  getAuthState, fromAuth.getAuthUser
);
export const getRecoverPasswordMailSent = createSelector<State, boolean, IAuthState>(
  getAuthState, fromAuth.getRecoverPasswordMailSent
);
export const getAuthUserIsStaff = createSelector<State, boolean, IAuthState>(
  getAuthState, fromAuth.getAuthUserIsStaff
);


// THEMES REDUCERS

export function getThemesState(state: State): IThemesState {
  return state.themes;
}
export const getTheme = createSelector<State, ILayerTheme | null, IThemesState>(
  getThemesState, fromThemes.getTheme
);
export const getThemeLoading = createSelector<State, boolean, IThemesState>(
  getThemesState, fromThemes.getThemeLoading
);
export const getThemes = createSelector<State, ILayerTheme[], IThemesState>(
  getThemesState, fromThemes.getThemes
);
export const getThemesLoading = createSelector<State, boolean, IThemesState>(
  getThemesState, fromThemes.getThemesLoading
);


// LAYERS REDUCERS

export function getLayersState(state: State): ILayersState {
  return state.layers;
}
export const getLayers = createSelector<State, ILayer[], ILayersState>(
  getLayersState, fromLayers.getLayers
);
export const getLayersLoading = createSelector<State, boolean, ILayersState>(
  getLayersState, fromLayers.getLayersLoading
);
export const getLayersTree = createSelector<State, ILayerTreeItem[], IThemesState, ILayersState>(
  getThemesState, getLayersState, fromLayers.getLayersTree
);
export const getLayersTreeLoading = createSelector<State, boolean, IThemesState, ILayersState>(
  getThemesState, getLayersState,
  (t: IThemesState, l: ILayersState) => t.itemsLoading || l.itemsLoading
);


// MAP REDUCERS

export function getMapState(state: State): IMapState {
  return state.map;
}
export const getMap = createSelector<State, IMap | null, IMapState>(
  getMapState, fromMap.getMap
);
export const getMapLayers = createSelector<State, IMapLayer[], IMapState>(
  getMapState, fromMap.getLayers
);
export const getZoomLevel = createSelector<State, number, IMapState>(
  getMapState, fromMap.getZoomLevel
);
export const getCanZoomIn = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getCanZoomIn
);
export const getCanZoomOut = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getCanZoomOut
);
export const getMapCenter = createSelector<State, MapCenter, IMapState>(
  getMapState, fromMap.getMapCenter
);
export const getCanViewGoBack = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getCanViewGoBack,
);
export const getCanViewGoForward = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getCanViewGoForward
);
export const getSelectedLayer = createSelector<State, IMapLayer | null, IMapState>(
  getMapState, fromMap.getSelectedLayer
);
export const getSelectedFeatures = createSelector<State, IMapSelectedFeatures[], IMapState>(
  getMapState, fromMap.getSelectedFeatures
);
export const getMapSaveImageRequest = createSelector<State, MapImageFormat | null, IMapState>(
  getMapState, fromMap.getMapSaveImageRequest
);
export const getMapPrint = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getMapPrint
);
export const getFilteredFeatures = createSelector<State, IMapSelectedFeatures[], IMapState>(
  getMapState, fromMap.getFilteredFeatures
);
export const getMapTableControlOpen = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getMapTableControlOpen
);
export const getMapLegendControlOpen = createSelector<State, boolean, IMapState>(
  getMapState, fromMap.getMapLegendControlOpen
);
export const getMapBaseLayer = createSelector<State, MapBaseLayer, IMapState>(
  getMapState, fromMap.getMapBaseLayer
);
export const getMapShowFeatureInfo = createSelector<State, IMapShowFeatureInfo | null, IMapState>(
  getMapState, fromMap.getMapShowFeatureInfo
);


// LAYERS BROWSER REDUCERS

export function getLayersBrowserState(state: State): ILayersBrowserState {
  return state.layersBrowser;
}
export const getLayersBrowserVisible = createSelector<State, boolean, ILayersBrowserState>(
  getLayersBrowserState, fromLayersBrowser.getLayersBrowserVisible
);
export const getLayersBrowserSelected = createSelector<State, number[], ILayersBrowserState>(
  getLayersBrowserState, fromLayersBrowser.getLayersBrowserSelected
);
export const getLayersBrowserExpanded = createSelector<State, number[], ILayersBrowserState>(
  getLayersBrowserState, fromLayersBrowser.getLayersBrowserExpanded
);
export const getLayersBrowserFilter = createSelector<State, string | null, ILayersBrowserState>(
  getLayersBrowserState, fromLayersBrowser.getLayersBrowserFilter
);
export const getLayersBrowserIsFiltered = createSelector<State, boolean, ILayersBrowserState>(
  getLayersBrowserState, fromLayersBrowser.getLayersBrowserIsFiltered
);


// MAPS LIST REDUCERS

export function getMapsListState(state: State): IMapsListState {
  return state.mapsList;
}
export const getMapsListLoading = createSelector<State, boolean, IMapsListState>(
  getMapsListState, fromMapsList.getMapsListLoading
);
export const getMapsListMaps = createSelector<State, IPersistentMap[], IMapsListState>(
  getMapsListState, fromMapsList.getMapsListMaps
);
export const getMapsListSelectedMapIdx = createSelector<State, number | null, IMapsListState>(
  getMapsListState, fromMapsList.getMapsListSelectedMapIdx
);
export const getMapsListSelectedMap = createSelector<State, IPersistentMap | null, IMapsListState>(
  getMapsListState, fromMapsList.getMapsListSelectedMap
);


// MAP BUILDER REDUCERS

export function getMapBuilderState(state: State): IMapBuilderState {
  return state.mapBuilder;
}
export const getStyleEditedLayer = createSelector<State, IMapLayer | null, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getStyleEditedLayer
);
export const getTransformOptionsVisibile = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getTransformOptionsVisible
);
export const getCurrentTransform = createSelector<State, number | null, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getCurrentTransform
);
export const getMeasureControlVisible = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMeasureControlVisible
);
export const getElevationProfileControlVisible = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getElevationProfileControlVisible
);
export const getSpatialAnalysisControlVisible = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getSpatialAnalysisControlVisible
);
export const getMapsListVisible = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapsListVisible
);
export const getMapBuilderLoading = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderLoading
);
export const getMapBuilderEditedMap = createSelector<State, IPersistentMap, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderEditedMap
);
export const getMapBuilderEditedIsNewMap = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderEditedIsNewMap
);
export const getMapBuilderEditedMapName = createSelector<State, string | null, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderEditedMapName
);
export const getMapBuilderMapLoading = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderMapLoading
);
export const getMapBuilderMapSaving = createSelector<State, boolean, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderMapSaving
);
export const getMapBuilderError = createSelector<State, any, IMapBuilderState>(
  getMapBuilderState, fromMapBuilder.getMapBuilderError
);


// GALLERY REDUCERS

export function getGalleryState(state: State): IGalleryState {
  return state.gallery;
}
export const getGalleryTheme = createSelector<State, IGalleryTheme | null, IGalleryState>(
  getGalleryState, fromGallery.getGalleryTheme
);
export const getGallerySubThemes = createSelector<State, IGalleryTheme[], IGalleryState>(
  getGalleryState, fromGallery.getGallerySubThemes
);
export const getGalleryItems = createSelector<State, IGalleryItem[], IGalleryState>(
  getGalleryState, fromGallery.getGalleryItems
);
export const getGalleryLoading = createSelector<State, boolean, IGalleryState>(
  getGalleryState, fromGallery.getGalleryLoading
);
export const getGalleryPreviewUrl = createSelector<State, string | null, IGalleryState>(
  getGalleryState, fromGallery.getGalleryPreviewUrl
);
