/**
 * Represents a layer theme (category). Layers
 * are organised in a hyerarchy.
 *
 * @export
 * @interface ILayerTheme
 */
export interface ILayerTheme {
  readonly id: number;

  /**
   * Name of the layer theme
   *
   * @readonly
   * @type {string}
   * @memberOf ILayerTheme
   */
  readonly name: string;

  /**
   * Parent theme (root if not defined)
   *
   * @readonly
   * @type {number | ILayerTheme}
   * @memberOf ILayerTheme
   */
  readonly parent?: number | ILayerTheme;
}
