import { LayerTypes } from './layer';


export type MapLayerLineCap = 'butt' | 'round' | 'circle';
export type MapLayerLineJoin = 'miter' | 'round' | 'bevel';
export type MapLayerPointType = 'circle' | 'image';


export interface IMapLayerStyle {
  stroke: boolean;
  color: string;
  opacity: number;
  weight: number;
  lineCap: MapLayerLineCap;
  lineJoin: MapLayerLineJoin;
  fill: boolean;
  fillColor: string;
  fillOpacity: number;
  pointType: MapLayerPointType;
  radius: number;
}


export interface IMapConditionalLayerStyle {
    prop: string;
    proportionalLegendSize: boolean;
    styles: {
      range: [any, any] | string,
      style: IMapLayerStyle
    }[];
}


export interface IMapLayer {
  id?: number;
  uniqueId: number;
  url?: string;
  name: string;
  label: string;
  layerType: LayerTypes;
  layers?: string;
  options?: L.WMSOptions;
  boundingBox: GeoJSON.Polygon;
  features?: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>;
  zIndex: number;
  visible: boolean;
  style?: IMapLayerStyle | IMapConditionalLayerStyle;
  metadata: string;
  dataLabels?: {[name: string]: string};
}
