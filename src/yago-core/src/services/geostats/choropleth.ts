import { FormGroup } from '@angular/forms';

import { IMapLayer, IMapLayerStyle, IMapConditionalLayerStyle } from '../../interfaces/index';
import { GeoStatisticTransformOption } from './base';
import { IndicatorBasedTransform } from './indicator';


export class ChoroplethTransform extends IndicatorBasedTransform {
  private static _commonOptions: GeoStatisticTransformOption[] = [{
    name: 'classesNumber',
    label: 'Number of classes',
    type: 'number',
    required: true
  }, {
    name: 'classes',
    label: 'Classes',
    type: 'select',
    required: true,
    choices: [
      {label: '', value: ''},
      {label: 'Quantiles', value: 'quantiles'},
      {label: 'Equal intervals', value: 'equal_intervals'}
    ]
  }, {
    name: 'stroke',
    label: 'Stroke',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'color',
    label: 'Stroke color',
    type: 'color',
    required: true,
    defaultValue: '#0000ff'
  }, {
    name: 'opacity',
    label: 'Stroke opacity',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }, {
    name: 'width',
    label: 'Stroke width',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 20,
    step: 0.5
  }, {
    name: 'fill',
    label: 'Fill',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'startFillColor',
    label: 'Start fill color',
    type: 'color',
    required: true,
    defaultValue: '#ff0000'
  }, {
    name: 'endFillColor',
    label: 'End fill color',
    type: 'color',
    required: true,
    defaultValue: '#ffff00'
  }, {
    name: 'fillOpacity',
    label: 'Fill opacity',
    type: 'slider',
    required: true,
    defaultValue: 0.7,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }];

  private static _name = 'Choropleth';
  static getName(): string { return ChoroplethTransform._name; }

  static getOptions(layer: IMapLayer): GeoStatisticTransformOption[] {
    const ind = ChoroplethTransform._getIndicators(layer);
    const indicators = ind.indicators;
    const indicatorsValues = ind.values;

    return (<GeoStatisticTransformOption[]>[{
      name: 'indicator',
      label: 'Indicator',
      type: 'select',
      required: true,
      choices: Object.keys(indicators).map(i => {
        return {
          label: `${i} (${indicators[i]})`,
          value: [i, indicators[i], indicatorsValues[i].length]
        };
      })
    }]).concat(ChoroplethTransform._commonOptions);
  }

  static updateForm(form: FormGroup, values: {[key: string]: any}): void {
    const hasStroke = values['stroke'];
    ['color', 'opacity', 'width']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasStroke && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasStroke && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });
    const hasFill = values['fill'];
    ['startFillColor', 'endFillColor', 'fillOpacity']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasFill && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasFill && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });

    const indicator: any[] = values['indicator'];
    const isStringIndicator = indicator != null &&
      indicator.length === 3 && indicator[1] === 'string';
    const classesCtrl = form.controls['classes'];
    const classesNumberCtrl = form.controls['classesNumber'];
    if (isStringIndicator) {
      if (classesCtrl.enabled) { classesCtrl.disable(); }
      if (classesCtrl.value !== '') { classesCtrl.setValue(''); }

      if (classesNumberCtrl.enabled) { classesNumberCtrl.disable(); }
      if (classesNumberCtrl.value !== indicator[2]) {
        classesNumberCtrl.setValue(indicator[2]);
      }
    } else {
      if (classesNumberCtrl.disabled) { classesNumberCtrl.enable(); }
    }
  }

  static transform(layer: IMapLayer, options: {[key: string]: any}): IMapLayer {
    const newLayer = JSON.parse(JSON.stringify(layer));
    const indicator: any[] = options['indicator'];
    if (indicator.length !== 3) { return newLayer; }
    const indicatorName: string = indicator[0];
    const indicatorType = indicator[1];
    const classes = options['classes'];
    let classesNumber: number;
    let steps: string[] | [number, number][] | null = null;
    if (indicatorType === 'string') {
      steps = ChoroplethTransform._getTextSteps(newLayer, indicatorName);
      classesNumber = steps.length;
    } else {
      try {
        classesNumber = parseInt(options['classesNumber'], 10);
        steps = classes === 'quantiles' ?
          ChoroplethTransform._getQuantilesSteps(newLayer, indicatorName, classesNumber) :
          ChoroplethTransform._getEqualIntervalsSteps(newLayer, indicatorName, classesNumber);
      } catch (e) {
        classesNumber = NaN;
      }
    }
    if (classesNumber == null || isNaN(classesNumber)) {
      return newLayer;
    }
    if (
      (indicatorType === 'number' && classes !== 'quantiles' && classes !== 'equal_intervals') ||
      (indicatorType === 'string' && (classes || '') !== '')
    ) {
      return newLayer;
    }

    if (steps == null) { return layer; }
    const newStyle = ChoroplethTransform._generateLayerStyle(
      <IMapLayerStyle>newLayer.style, steps, options
    );
    if (newStyle == null) { return newLayer; }
    newLayer.style = newStyle;
    return newLayer;
  }

  private static _generateLayerStyle(
    origStyle: IMapLayerStyle, steps: string[]|[number, number][], options: {[key: string]: any}
  ): IMapConditionalLayerStyle | null {
    const indicator: any[] = options['indicator'];
    if (indicator.length !== 3) { return null; }
    const indicatorName: string = indicator[0];
    const stepsNum: number = steps.length;
    const stroke: boolean = options['stroke'] === 'true';
    const color: string = options['color'];
    const opacity: number = parseFloat(options['opacity']);
    const width: number = parseInt(options['width'], 10);
    const startFillColor: string = options['startFillColor'];
    const endFillColor: string = options['endFillColor'];
    const fillOpacity: number = parseFloat(options['fillOpacity']);
    const colors = ChoroplethTransform._getStepsColors(stepsNum - 1, startFillColor, endFillColor);
    const style: IMapConditionalLayerStyle = {
      prop: indicatorName, proportionalLegendSize: false, styles: []
    };

    for (let i = 0 ; i < stepsNum ; i++) {
      style.styles.push({
        range: steps[i],
        style: Object.assign({}, origStyle, {
          stroke,
          color,
          opacity,
          width,
          fill: true,
          fillColor: colors[i],
          fillOpacity
        })
      });
    }
    return style;
  }
}
