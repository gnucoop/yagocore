import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { ILayer, IWMSSource, IWMSSourceAvabilableLayer, State } from '../interfaces/index';
import {
  WMSSourcesActions, WMSSourcesActionTypes,
  WMSSourcesGetAction, WMSSourcesGetSuccessAction,
  WMSSourcesGetFailureAction,
  WMSSourcesListAction, WMSSourcesListSuccessAction,
  WMSSourcesListFailureAction,
  WMSSourcesCreateAction, WMSSourcesCreateSuccessAction,
  WMSSourcesCreateFailureAction,
  WMSSourcesUpdateAction, WMSSourcesUpdateSuccessAction,
  WMSSourcesUpdateFailureAction,
  WMSSourcesGetLayersAction, WMSSourcesGetLayersSuccessAction,
  WMSSourcesGetLayersFailureAction,
  WMSSourcesGetLayerAction, WMSSourcesGetLayerSuccessAction,
  WMSSourcesGetLayerFailureAction,
  WMSSourcesImportLayerAction, WMSSourcesImportLayerSuccessAction,
  WMSSourcesImportLayerFailureAction,
  WMSSourcesDeleteAction, WMSSourcesDeleteFailureAction,
  WMSSourcesDeleteSuccessAction,
  WMSSourcesResetCurrentWMSSource,
  Back, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ModelService } from './model';
import { WMSSourcesManager } from './wms-sources-manager';


@Injectable()
export class WMSSourcesService extends ModelService<
    IWMSSource, WMSSourcesGetAction, WMSSourcesListAction,
    WMSSourcesCreateAction, WMSSourcesUpdateAction, WMSSourcesDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, WMSSourcesGetAction, WMSSourcesListAction,
      WMSSourcesCreateAction, WMSSourcesUpdateAction, WMSSourcesDeleteAction
    );
  }

  getAdminWMSSource(): Observable<IWMSSource | null> {
    return this._store.select(reducers.getAdminWMSSource);
  }

  getAdminWMSSourceLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminWMSSourceLoading);
  }

  availableLayers(id: number, admin = false): void {
    this._store.dispatch(new WMSSourcesGetLayersAction(id, admin));
  }

  availableLayer(id: number, workspace: string, name: string, admin = false): void {
    this._store.dispatch(new WMSSourcesGetLayerAction({id, name, workspace}, admin));
  }

  importAvailableLayer(
    id: number, workspace: string, name: string, data: any, admin = false
  ): void {
    this._store.dispatch(new WMSSourcesImportLayerAction({id, name, workspace, data}, admin));
  }

  resetCurrentWMSSource(admin = false): void {
    this._store.dispatch(new WMSSourcesResetCurrentWMSSource(admin));
  }
}


@Injectable()
export class WMSSourcesEffects {
  @Effect()
  get$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.GET)
    .switchMap((action: WMSSourcesGetAction) =>
      this._manager.get(action.payload)
        .map((source: IWMSSource) => new WMSSourcesGetSuccessAction(source, action.admin))
        .catch((error: any) => Observable.of(new WMSSourcesGetFailureAction(error, action.admin)))
    );

  @Effect()
  load$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.LIST)
    .switchMap((action: WMSSourcesListAction) =>
      this._manager.list(action.payload)
        .map((sources: IWMSSource[]) => new WMSSourcesListSuccessAction(sources, action.admin))
        .catch((error) => Observable.of(new WMSSourcesListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.CREATE)
    .switchMap((action: WMSSourcesCreateAction) =>
      this._manager.create(action.payload)
        .map((s: IWMSSource) => new WMSSourcesCreateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new WMSSourcesCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.UPDATE)
    .switchMap((action: WMSSourcesCreateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: IWMSSource) => new WMSSourcesUpdateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new WMSSourcesUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  getLayer$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.GET_LAYER)
    .switchMap((action: WMSSourcesGetLayerAction) =>
      this._manager.getAvailableLayer(
        action.payload.id, action.payload.workspace, action.payload.name
      )
      .map((layer: IWMSSourceAvabilableLayer) =>
        new WMSSourcesGetLayerSuccessAction(layer, action.admin))
      .catch((error: any) =>
        Observable.of(new WMSSourcesGetLayerFailureAction(error, action.admin)))
    );

  @Effect()
  getLayers$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.GET_LAYERS)
    .switchMap((action: WMSSourcesGetLayersAction) =>
      this._manager.getAvailableLayers(action.payload)
        .map((layers: IWMSSourceAvabilableLayer[]) =>
          new WMSSourcesGetLayersSuccessAction(layers, action.admin))
        .catch((error: any) =>
          Observable.of(new WMSSourcesGetLayersFailureAction(error, action.admin)))
    );

  @Effect()
  importLayer$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.IMPORT_LAYER)
    .switchMap((action: WMSSourcesImportLayerAction) =>
      this._manager.importAvailableLayer(
        action.payload.id, action.payload.workspace, action.payload.name, action.payload.data
      )
      .map((layer: ILayer) => new WMSSourcesImportLayerSuccessAction(layer, action.admin))
      .catch((error: any) =>
        Observable.of(new WMSSourcesImportLayerFailureAction(error, action.admin)))
    );

  @Effect()
  layerImported$: Observable<RouterActions> = this._actions
    .ofType(WMSSourcesActionTypes.IMPORT_LAYER_SUCCESS)
    .map(() => new Back());

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(WMSSourcesActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(WMSSourcesActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.DELETE)
    .switchMap((action: WMSSourcesDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new WMSSourcesDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new WMSSourcesDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<WMSSourcesActions> = this._actions
    .ofType(WMSSourcesActionTypes.DELETE_SUCCESS)
    .map((action: WMSSourcesDeleteSuccessAction) =>
      new WMSSourcesListAction(undefined, action.admin));

  constructor(
    private _actions: Actions, private _manager: WMSSourcesManager
  ) { }
}
