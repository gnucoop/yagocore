#!/bin/bash

PACKAGES=(core
  md)
SEMVER=`pwd`/node_modules/.bin/semver
PWD=`pwd`
BASE_DIR="${PWD}/dist/releases"
UNAME=`uname`
PUBLISH=true
LEVEL=prerelase

for i in "$@"
do
case $i in
    -v|--version-only)
    PUBLISH=false
    ;;
    -m|--minor)
    LEVEL=minor
    ;;
    -M|--major)
    LEVEL=major
    ;;
    -i|--include-icons)
    PACKAGES[${#PACKAGES[*]}]=icons
    shift # past argument=value
    ;;
    -I|--icons-only)
    PACKAGES=(icons
    )
    shift # past argument=value
    ;;
    -p|--package)
    shift
    PACKAGES=($1
    )
    shift
    ;;
    *)
            # unknown option
    ;;
esac
done

for PACKAGE in ${PACKAGES[@]}
do
  PACKAGE_DIR="${BASE_DIR}/yago-${PACKAGE}"
  CURRENT_VERSION=`npm show @yago/${PACKAGE} version`
  if [ -z $CURRENT_VERSION ]; then
    CURRENT_VERSION='0.0.1-alpha.0'
  fi
  NEW_VERSION=`${SEMVER} -i ${LEVEL} ${CURRENT_VERSION}`
  if [[ $UNAME == 'Darwin' ]]; then
    find $PACKAGE_DIR/ -type f -name package.json -print0 | xargs -0 sed -i "" "s/0\.0\.0-PLACEHOLDER/${NEW_VERSION}/g"
  else
    find $PACKAGE_DIR/ -type f -name package.json -print0 | xargs -0 sed -i "s/0\.0\.0-PLACEHOLDER/${NEW_VERSION}/g"
  fi
  if $PUBLISH; then
    cp "${PWD}/.npmrc" $PACKAGE_DIR
    cd $PACKAGE_DIR
    npm publish
    cd $PWD
  fi
done
