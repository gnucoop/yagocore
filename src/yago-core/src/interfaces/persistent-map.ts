import { MapBaseLayer } from './map';
import { IMapLayer } from './map-layer';


export interface IPersistentMap {
  id: number | null;
  name: string | null;
  content: {baseLayer: MapBaseLayer, layers: IMapLayer[]} | null;
}
