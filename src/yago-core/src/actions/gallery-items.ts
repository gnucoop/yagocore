import { BaseAction } from './base';
import { type } from '../utils/index';
import { IApiListOptions, IGalleryItem, IGalleryTheme } from '../interfaces/index';


// tslint:disable-next-line
export const GalleryItemsActionTypes = {
  LIST: type('[Gallery Items] List'),
  LIST_FAILURE: type('[Gallery Items] List failure'),
  LIST_SUCCESS: type('[Gallery Items] List success'),
  GET: type('[Gallery Items] Get'),
  GET_FAILURE: type('[Gallery Items] Get failure'),
  GET_SUCCESS: type('[Gallery Items] Get success'),
  CREATE: type('[Gallery Items] Create'),
  CREATE_FAILURE: type('[Gallery Items] Create failure'),
  CREATE_SUCCESS: type('[Gallery Items] Create success'),
  UPDATE: type('[Gallery Items] Update'),
  UPDATE_FAILURE: type('[Gallery Items] Update failure'),
  UPDATE_SUCCESS: type('[Gallery Items] Update success'),
  DELETE: type('[Gallery Items] Delete'),
  DELETE_FAILURE: type('[Gallery Items] Delete failure'),
  DELETE_SUCCESS: type('[Gallery Items] Delete success'),
  LIST_BY_THEME_ID: type('[Gallery Items] List by theme id'),
  LIST_BY_THEME: type('[Gallery Items] List by theme'),
  LIST_BY_THEME_FAILURE: type('[Gallery Items] List by theme failure'),
  LIST_BY_THEME_SUCCESS: type('[Gallery Items] List by theme success'),
  RESET_CURRENT_ITEM: type('[Gallery Items] Reset current item')
};

export class GalleryItemsListAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST;
  constructor(public payload?: IApiListOptions, public admin = false) { }
}
export class GalleryItemsListFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsListSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_SUCCESS;
  constructor(public payload: IGalleryItem[], public admin = false) { }
}
export class GalleryItemsGetAction implements BaseAction {
  type = GalleryItemsActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}
export class GalleryItemsGetFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsGetSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.GET_SUCCESS;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsCreateAction implements BaseAction {
  type = GalleryItemsActionTypes.CREATE;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsCreateFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.CREATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsCreateSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.CREATE_SUCCESS;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsUpdateAction implements BaseAction {
  type = GalleryItemsActionTypes.UPDATE;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsUpdateFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsUpdateSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsDeleteAction implements BaseAction {
  type = GalleryItemsActionTypes.DELETE;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsDeleteFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsDeleteSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.DELETE_SUCCESS;
  constructor(public payload: IGalleryItem, public admin = false) { }
}
export class GalleryItemsListByThemeIdAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_BY_THEME_ID;
  constructor(
    public payload: {theme: number, options?: IApiListOptions},
    public admin = false
  ) { }
}
export class GalleryItemsListByThemeAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_BY_THEME;
  constructor(
    public payload: {theme?: IGalleryTheme, options?: IApiListOptions},
    public admin = false
  ) { }
}
export class GalleryItemsListByThemeFailureAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_BY_THEME_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryItemsListByThemeSuccessAction implements BaseAction {
  type = GalleryItemsActionTypes.LIST_BY_THEME_SUCCESS;
  constructor(public payload: IGalleryItem[], public admin = false) { }
}
export class GalleryItemsResetCurrentItemAction implements BaseAction {
  type = GalleryItemsActionTypes.RESET_CURRENT_ITEM;
  payload = undefined;
  constructor(public admin = false) { }
}


export type GalleryItemsActions =
  GalleryItemsGetAction | GalleryItemsGetFailureAction | GalleryItemsGetSuccessAction |
  GalleryItemsListAction | GalleryItemsListFailureAction | GalleryItemsListSuccessAction |
  GalleryItemsCreateAction | GalleryItemsCreateFailureAction | GalleryItemsCreateSuccessAction |
  GalleryItemsUpdateAction | GalleryItemsUpdateFailureAction | GalleryItemsUpdateSuccessAction |
  GalleryItemsDeleteAction | GalleryItemsDeleteFailureAction | GalleryItemsDeleteSuccessAction |
  GalleryItemsListByThemeIdAction |
  GalleryItemsListByThemeAction | GalleryItemsListByThemeFailureAction |
  GalleryItemsListByThemeSuccessAction |
  GalleryItemsResetCurrentItemAction;
