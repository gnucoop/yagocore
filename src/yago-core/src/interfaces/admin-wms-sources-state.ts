import { IApiListOptions } from './api';
import { IWMSSource, IWMSSourceAvabilableLayer } from './wms-source';


export interface IAdminWMSSourcesState {
  listOptions: IApiListOptions | null;
  item: IWMSSource | null;
  items: IWMSSource[];
  layer: IWMSSourceAvabilableLayer | null;
  layerLoading: boolean;
  layers: IWMSSourceAvabilableLayer[];
  itemLoading: boolean;
  itemsLoading: boolean;
  layersLoading: boolean;
  error: any;
}
