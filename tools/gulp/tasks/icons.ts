import {task} from 'gulp';
import {join} from 'path';
import {buildConfig, buildScssTask, sequenceTask} from 'yago-build-tools';
import {copyTask} from '../util/task_helpers';

const {packagesDir, outputDir} = buildConfig;


task('yago-icons:assets:copy-assets', copyTask([
  join(packagesDir, 'yago-icons', '**/*.!(ts|spec.ts)')
], join(outputDir, 'packages', 'yago-icons')));

task('yago-icons:assets:scss', buildScssTask(
  join(outputDir, 'packages', 'yago-icons'), join(packagesDir, 'yago-icons')
));

task('yago-icons:build', sequenceTask(
  'yago-icons:assets:copy-assets', 'yago-icons:assets:scss'
));


task('yago-icons:build-release', ['yago-icons:build'], copyTask(
  join(outputDir, 'packages', 'yago-icons'),
  join(outputDir, 'releases', 'yago-icons')
));
