import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {PayloadResult} from '../data-definitions';
import {NgxChartItem, NgxChartResult} from './ngx-definitions';

@Component({
  selector: 'payload-chart',
  templateUrl: './payload-chart.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PayloadChart {

  /** Color scheme for the payload graph. */
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  /** X-Axis label of the Chart. */
  xAxisLabel = 'Date';

  /** Y-Axis label of the Chart. */
  yAxisLabel = 'Payload Size (kb)';

  /** Chart data that is taken by NGX-Charts. */
  chartData: NgxChartResult[];

  /** Payload data that will be rendered in the chart. */
  @Input()
  set data(value: PayloadResult[]) {
    this.chartData = this.createChartResults(value);
  }

  /** Creates a list of ngx-chart results of the Payload results. */
  private createChartResults(data: PayloadResult[]) {
    if (!data) {
      return [];
    }

    // Data snapshot from Firebase is not ordered by timestamp. Before rendering the graph
    // manually sort the results by their timestamp.
    data = data.sort((a, b) => a.timestamp < b.timestamp ? -1 : 1);

    const yagoCoreChartItems: NgxChartItem[] = [];
    const yagoMdChartItems: NgxChartItem[] = [];

    // Walk through every result entry and create a NgxChart item that can be rendered inside of
    // the linear chart.
    data.forEach(result => {
      if (
        !result.yago_core_fesm_2015 || !result.yago_md_fesm_2015
      ) {
        return;
      }

      // Convert the timestamp of the payload result into a date because NGX-Charts can group
      // dates in the x-axis.
      const date = new Date(result.timestamp);

      yagoCoreChartItems.push({ name: date, value: result.yago_core_fesm_2015 });
      yagoMdChartItems.push({ name: date, value: result.yago_md_fesm_2015 });
    });

    return [
      { name: 'yago Core', series: yagoCoreChartItems },
      { name: 'yago MD', series: yagoMdChartItems }
    ];
  }

}

