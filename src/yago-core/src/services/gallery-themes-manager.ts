import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { IApiListOptions, IGalleryTheme } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class GalleryThemesManager extends ModelManager<IGalleryTheme> {
  private _listByParentEndpoint: string;

  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.galleryThemesEndpoint], _apiService, ['get', 'list']);

    this._listByParentEndpoint = `${this.endPoint}/${_config.galleryThemesByParentSubpath}`;
  }

  listByParent(parent?: IGalleryTheme, options?: IApiListOptions): Observable<IGalleryTheme[]> {
    let ep = `${this._listByParentEndpoint}`;
    if (parent != null) {
      ep = `${ep}/${parent.id}`;
    }
    return <Observable<IGalleryTheme[]>>this._apiService.list(ep, options, false);
  }
}
