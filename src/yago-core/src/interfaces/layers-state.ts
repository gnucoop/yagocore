import { IApiListOptions } from './api';
import { ILayer } from './layer';


export interface ILayersState {
  listOptions: IApiListOptions | null;
  items: ILayer[];
  itemsLoading: boolean;
  item: ILayer | null;
  itemLoading: boolean;
  error: any;
}
