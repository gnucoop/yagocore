import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import {
  Http, Headers, Request, RequestOptions, RequestOptionsArgs, RequestMethod, Response
} from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';

import { State } from '../interfaces/index';
import { getAuthToken } from '../reducers/index';


export interface IAuthConfig {
globalHeaders: Array<Object>;
headerName: string;
headerPrefix: string | null;
noJwtError: boolean;
noClientCheck: boolean;
noTokenScheme?: boolean;
tokenName: string;
}


export interface IAuthConfigOptional {
  headerName?: string;
  headerPrefix?: string;
  tokenName?: string;
  tokenGetter?: () => string | Promise<string>;
  noJwtError?: boolean;
  noClientCheck?: boolean;
  globalHeaders?: Array<Object>;
  noTokenScheme?: boolean;
}

export const AUTH_CONFIG = new InjectionToken<IAuthConfigOptional>('AUTH_CONFIG');

export class AuthConfigOptional implements IAuthConfigOptional {
  headerName?: string;
  headerPrefix?: string;
  tokenName?: string;
  tokenGetter?: () => string | Promise<string>;
  noJwtError?: boolean;
  noClientCheck?: boolean;
  globalHeaders?: Array<Object>;
  noTokenScheme?: boolean;
}


export class AuthConfigConsts {
  public static DEFAULT_TOKEN_NAME = 'id_token';
  public static DEFAULT_HEADER_NAME = 'Authorization';
  public static HEADER_PREFIX_BEARER = 'Bearer ';
}


const authConfigDefaults: IAuthConfig = {
  headerName: AuthConfigConsts.DEFAULT_HEADER_NAME,
  headerPrefix: null,
  tokenName: AuthConfigConsts.DEFAULT_TOKEN_NAME,
  noJwtError: false,
  noClientCheck: false,
  globalHeaders: [],
  noTokenScheme: false
};


export class AuthConfig {

  private _config: IAuthConfig;

  constructor(config?: IAuthConfigOptional) {
    config = config || {};
    this._config = objectAssign({}, authConfigDefaults, config);
    if (this._config.headerPrefix) {
      this._config.headerPrefix += ' ';
    } else if (this._config.noTokenScheme) {
      this._config.headerPrefix = '';
    } else {
      this._config.headerPrefix = AuthConfigConsts.HEADER_PREFIX_BEARER;
    }
  }

  public getConfig(): IAuthConfig {
    return this._config;
  }

}

export class AuthHttpError extends Error {
}


@Injectable()
export class AuthHttp {

  private _config: IAuthConfig;
  private _token: string;

  constructor(
    private _http: Http,
    private _store: Store<State>,
    @Inject(AUTH_CONFIG) @Optional() options?: AuthConfigOptional
  ) {
    const conf = new AuthConfig(options);
    this._config = conf.getConfig();

    _store.select(getAuthToken)
      .subscribe((t: string) => this._token = t);
  }

  public setGlobalHeaders(headers: Array<Object>, request: Request | RequestOptionsArgs) {
    if (!request.headers) {
      request.headers = new Headers();
    }
    headers.forEach((header: Object) => {
      const key: string = Object.keys(header)[0];
      const headerValue: string = (header as any)[key];
      (request.headers as Headers).set(key, headerValue);
    });
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    if (typeof url === 'string') {
      return this.get(url, options);
    }

    const req: Request = url as Request;
    return this._requestWithToken(req, this._token);
  }

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: '', method: RequestMethod.Get, url: url }, options);
  }

  public post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: body, method: RequestMethod.Post, url: url }, options);
  }

  public put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: body, method: RequestMethod.Put, url: url }, options);
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: '', method: RequestMethod.Delete, url: url }, options);
  }

  public patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: body, method: RequestMethod.Patch, url: url }, options);
  }

  public head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: '', method: RequestMethod.Head, url: url }, options);
  }

  public options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this._requestHelper({ body: '', method: RequestMethod.Options, url: url }, options);
  }

  private _requestHelper(
    requestArgs: RequestOptionsArgs, additionalOptions?: RequestOptionsArgs
  ): Observable<Response> {
    let options = new RequestOptions(requestArgs);
    if (additionalOptions) {
      options = options.merge(additionalOptions);
    }
    return this.request(new Request(options));
  }

  private _requestWithToken(req: Request, token: string): Observable<Response> {
    if (!this._config.noClientCheck && !tokenNotExpired(undefined, token)) {
      if (!this._config.noJwtError) {
        return new Observable<Response>((obs: any) => {
          obs.error(new AuthHttpError('No JWT present or has expired'));
        });
      }
    } else {
      req.headers.set(this._config.headerName, this._config.headerPrefix + token);
    }

    return this._http.request(req);
  }

}


export class JwtHelper {

  public static urlBase64Decode(str: string): string {
    let output = str.replace(/-/g, '+').replace(/_/g, '/');
    switch (output.length % 4) {
      case 0: { break; }
      case 2: { output += '=='; break; }
      case 3: { output += '='; break; }
      default: {
        throw new Error('Illegal base64url string!');
      }
    }
    return this.b64DecodeUnicode(output);
  }

  private static b64decode(str: string): string {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
    let output = '';

    str = String(str).replace(/=+$/, '');

    if (str.length % 4 === 1) {
      throw new Error('\'atob\' failed: The string to be decoded is not correctly encoded.');
    }

    for (
      let bc = 0, bs: any, buffer: any, idx = 0;
      buffer = str.charAt(idx++);
      // tslint:disable-next-line
      ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
        // tslint:disable-next-line
        bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
    ) {
      buffer = chars.indexOf(buffer);
    }
    return output;
  }

  private static b64DecodeUnicode(str: any) {
    const chars: string[] = [];
    const b64dec: string = this.b64decode(str);
    const b64decLen: number = b64dec.length;
    for (let i = 0; i < b64decLen ; i++) {
      chars.push('%' + ('00' + b64dec.substr(i, 1).charCodeAt(0).toString(16)).slice(-2));
    }
    return decodeURIComponent(chars.join(''));
  }

  public static decodeToken(token: string): any {
    const parts = token.split('.');

    if (parts.length !== 3) {
      throw new Error('JWT must have 3 parts');
    }

    const decoded = this.urlBase64Decode(parts[1]);
    if (!decoded) {
      throw new Error('Cannot decode the token');
    }

    return JSON.parse(decoded);
  }

  public static getTokenExpirationDate(token: string): Date | null {
    let decoded: any;
    decoded = this.decodeToken(token);

    if (!decoded.hasOwnProperty('exp')) {
      return null;
    }

    const date = new Date(0); // The 0 here is the key, which sets the date to the epoch
    date.setUTCSeconds(decoded.exp);

    return date;
  }

  public static isTokenExpired(token: string, offsetSeconds?: number): boolean {
    const date = this.getTokenExpirationDate(token);
    offsetSeconds = offsetSeconds || 0;

    if (date == null) {
      return false;
    }

    return !(date.valueOf() > (new Date().valueOf() + (offsetSeconds * 1000)));
  }
}

export function tokenNotExpired(
  tokenName = AuthConfigConsts.DEFAULT_TOKEN_NAME, jwt?: string
): boolean {

  const token: string | null = jwt || localStorage.getItem(tokenName);

  return token != null && !JwtHelper.isTokenExpired(token);
}

const hasOwnProperty = Object.prototype.hasOwnProperty;
const propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val: any) {
  if (val === null || val === undefined) {
    throw new TypeError('Object.assign cannot be called with null or undefined');
  }

  return Object(val);
}

function objectAssign(target: any, ..._source: any[]) {
  let from: any;
  const to = toObject(target);
  let symbols: any;

  for (let s = 1; s < arguments.length; s++) {
    from = Object(arguments[s]);

    for (const key in from) {
      if (hasOwnProperty.call(from, key)) {
        to[key] = from[key];
      }
    }

    if ((<any>Object).getOwnPropertySymbols) {
      symbols = (<any>Object).getOwnPropertySymbols(from);
      for (let i = 0; i < symbols.length; i++) {
        if (propIsEnumerable.call(from, symbols[i])) {
          to[symbols[i]] = from[symbols[i]];
        }
      }
    }
  }
  return to;
}
