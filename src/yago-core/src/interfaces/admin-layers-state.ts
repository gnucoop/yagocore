import { IApiListOptions } from './api';
import { ILayer } from './layer';


export interface IAdminLayersState {
  listOptions: IApiListOptions | null;
  items: ILayer[];
  itemsLoading: boolean;
  item: ILayer | null;
  itemLoading: boolean;
  error: any;
}
