import { BaseAction } from './base';
import { type } from '../utils/index';
import {
  IApiListOptions, ILayer, IArcGISSource, IArcGISSourceAvabilableLayer
} from '../interfaces/index';


// tslint:disable-next-line
export const ArcGISSourcesActionTypes = {
  GET: type('[ArcGIS Sources] Get'),
  GET_FAILURE: type('[ArcGIS Sources] Get failure'),
  GET_SUCCESS: type('[ArcGIS Sources] Get success'),
  LIST: type('[ArcGIS Sources] List'),
  LIST_FAILURE: type('[ArcGIS Sources] List failure'),
  LIST_SUCCESS: type('[ArcGIS Sources] List success'),
  CREATE: type('[ArcGIS Sources] Create'),
  CREATE_FAILURE: type('[ArcGIS Sources] Create failure'),
  CREATE_SUCCESS: type('[ArcGIS Sources] Create success'),
  UPDATE: type('[ArcGIS Sources] Update'),
  UPDATE_FAILURE: type('[ArcGIS Sources] Update failure'),
  UPDATE_SUCCESS: type('[ArcGIS Sources] Update success'),
  DELETE: type('[ArcGIS Sources] Delete'),
  DELETE_FAILURE: type('[ArcGIS Sources] Delete failure'),
  DELETE_SUCCESS: type('[ArcGIS Sources] Delete success'),
  GET_LAYER: type('[ArcGIS Sources] Get layer'),
  GET_LAYER_FAILURE: type('[ArcGIS Sources] Get layer failure'),
  GET_LAYER_SUCCESS: type('[ArcGIS Sources] Get layer success'),
  GET_LAYERS: type('[ArcGIS Sources] Get layers'),
  GET_LAYERS_FAILURE: type('[ArcGIS Sources] Get layers failure'),
  GET_LAYERS_SUCCESS: type('[ArcGIS Sources] Get layers success'),
  IMPORT_LAYER: type('[ArcGIS Sources] Import layer'),
  IMPORT_LAYER_FAILURE: type('[ArcGIS Sources] Import layer failure'),
  IMPORT_LAYER_SUCCESS: type('[ArcGIS Sources] Import layer success'),
  RESET_CURRENT_ArcGIS_SOURCE: type('[ArcGIS Sources] Reset current ArcGIS source')
};

export class ArcGISSourcesGetAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}

export class ArcGISSourcesGetFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesGetSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_SUCCESS;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesListAction implements BaseAction {
  type = ArcGISSourcesActionTypes.LIST;
  constructor(public payload: IApiListOptions | undefined, public admin = false) { }
}

export class ArcGISSourcesListFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.LIST_FAILURE;

  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesListSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.LIST_SUCCESS;
  constructor(public payload: IArcGISSource[], public admin = false) { }
}

export class ArcGISSourcesCreateAction implements BaseAction {
  type = ArcGISSourcesActionTypes.CREATE;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesCreateFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.CREATE_FAILURE;

  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesCreateSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.CREATE_SUCCESS;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesUpdateAction implements BaseAction {
  type = ArcGISSourcesActionTypes.UPDATE;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesUpdateFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesUpdateSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesDeleteAction implements BaseAction {
  type = ArcGISSourcesActionTypes.DELETE;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesDeleteFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesDeleteSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.DELETE_SUCCESS;
  constructor(public payload: IArcGISSource, public admin = false) { }
}

export class ArcGISSourcesGetLayerAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYER;
  constructor(
    public payload: {id: number, lid: number, basePath: string}, public admin = false
  ) { }
}

export class ArcGISSourcesGetLayerFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesGetLayerSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYER_SUCCESS;
  constructor(public payload: IArcGISSourceAvabilableLayer, public admin = false) { }
}

export class ArcGISSourcesGetLayersAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYERS;
  constructor(public payload: number, public admin = false) { }
}

export class ArcGISSourcesGetLayersFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYERS_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesGetLayersSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.GET_LAYERS_SUCCESS;
  constructor(public payload: IArcGISSourceAvabilableLayer[], public admin = false) { }
}

export class ArcGISSourcesImportLayerAction implements BaseAction {
  type = ArcGISSourcesActionTypes.IMPORT_LAYER;
  constructor(
    public payload: {id: number, lid: number, basePath: string, data: any}, public admin = false
  ) { }
}

export class ArcGISSourcesImportLayerFailureAction implements BaseAction {
  type = ArcGISSourcesActionTypes.IMPORT_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class ArcGISSourcesImportLayerSuccessAction implements BaseAction {
  type = ArcGISSourcesActionTypes.IMPORT_LAYER_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}
export class ArcGISSourcesResetCurrentArcGISSource implements BaseAction {
  type = ArcGISSourcesActionTypes.RESET_CURRENT_ArcGIS_SOURCE;
  payload = undefined;
  constructor(public admin = false) { }
}

export type ArcGISSourcesActions =
  ArcGISSourcesGetAction | ArcGISSourcesGetFailureAction |
  ArcGISSourcesGetSuccessAction |
  ArcGISSourcesListAction | ArcGISSourcesListFailureAction |
  ArcGISSourcesListSuccessAction |
  ArcGISSourcesCreateAction | ArcGISSourcesCreateFailureAction |
  ArcGISSourcesCreateSuccessAction |
  ArcGISSourcesUpdateAction | ArcGISSourcesUpdateFailureAction |
  ArcGISSourcesUpdateSuccessAction |
  ArcGISSourcesDeleteAction | ArcGISSourcesDeleteFailureAction |
  ArcGISSourcesDeleteSuccessAction |
  ArcGISSourcesGetLayerAction | ArcGISSourcesGetLayerFailureAction |
  ArcGISSourcesGetLayerSuccessAction |
  ArcGISSourcesGetLayersAction | ArcGISSourcesGetLayersFailureAction |
  ArcGISSourcesGetLayersSuccessAction |
  ArcGISSourcesImportLayerAction | ArcGISSourcesImportLayerFailureAction |
  ArcGISSourcesImportLayerSuccessAction |
  ArcGISSourcesResetCurrentArcGISSource;
