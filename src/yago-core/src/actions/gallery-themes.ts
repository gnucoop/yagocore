import { BaseAction } from './base';
import { type } from '../utils/index';
import { IApiListOptions, IGalleryTheme } from '../interfaces/index';


// tslint:disable-next-line
export const GalleryThemesActionTypes = {
  LIST: type('[Gallery Themes] List'),
  LIST_FAILURE: type('[Gallery Themes] List failure'),
  LIST_SUCCESS: type('[Gallery Themes] List success'),
  GET: type('[Gallery Themes] Get'),
  GET_FAILURE: type('[Gallery Themes] Get failure'),
  GET_SUCCESS: type('[Gallery Themes] Get success'),
  CREATE: type('[Gallery Themes] Create'),
  CREATE_FAILURE: type('[Gallery Themes] Create failure'),
  CREATE_SUCCESS: type('[Gallery Themes] Create success'),
  UPDATE: type('[Gallery Themes] Update'),
  UPDATE_FAILURE: type('[Gallery Themes] Update failure'),
  UPDATE_SUCCESS: type('[Gallery Themes] Update success'),
  DELETE: type('[Gallery Themes] Delete'),
  DELETE_FAILURE: type('[Gallery Themes] Delete failure'),
  DELETE_SUCCESS: type('[Gallery Themes] Delete success'),
  LIST_BY_PARENT_ID: type('[Gallery Themes] List by parent id'),
  LIST_BY_PARENT: type('[Gallery Themes] List by parent'),
  LIST_BY_PARENT_FAILURE: type('[Gallery Themes] List by parent failure'),
  LIST_BY_PARENT_SUCCESS: type('[Gallery Themes] List by parent success'),
  RESET_CURRENT_THEME: type('[Gallery Themes] Reset current theme')
};

export class GalleryThemesListAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST;
  constructor(public payload: IApiListOptions | undefined = undefined, public admin = false) { }
}
export class GalleryThemesListFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesListSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_SUCCESS;
  constructor(public payload: IGalleryTheme[], public admin = false) { }
}
export class GalleryThemesGetAction implements BaseAction {
  type = GalleryThemesActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}
export class GalleryThemesGetFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesGetSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.GET_SUCCESS;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesCreateAction implements BaseAction {
  type = GalleryThemesActionTypes.CREATE;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesCreateFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.CREATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesCreateSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.CREATE_SUCCESS;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesUpdateAction implements BaseAction {
  type = GalleryThemesActionTypes.UPDATE;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesUpdateFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesUpdateSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesDeleteAction implements BaseAction {
  type = GalleryThemesActionTypes.DELETE;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesDeleteFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesDeleteSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.DELETE_SUCCESS;
  constructor(public payload: IGalleryTheme, public admin = false) { }
}
export class GalleryThemesListByParentIdAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_BY_PARENT_ID;
  constructor(
    public payload: {parent: number, options?: IApiListOptions},
    public admin = false
  ) { }
}
export class GalleryThemesListByParentAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_BY_PARENT;
  constructor(
    public payload: {parent?: IGalleryTheme, options?: IApiListOptions},
    public admin = false
  ) { }
}
export class GalleryThemesListByParentFailureAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_BY_PARENT_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class GalleryThemesListByParentSuccessAction implements BaseAction {
  type = GalleryThemesActionTypes.LIST_BY_PARENT_SUCCESS;
  constructor(public payload: IGalleryTheme[], public admin = false) { }
}
export class GalleryThemesResetCurrentThemeAction implements BaseAction {
  type = GalleryThemesActionTypes.RESET_CURRENT_THEME;
  payload = undefined;
  constructor(public admin = false) { }
}


export type GalleryThemesActions =
  GalleryThemesGetAction | GalleryThemesGetFailureAction | GalleryThemesGetSuccessAction |
  GalleryThemesListAction | GalleryThemesListFailureAction | GalleryThemesListSuccessAction |
  GalleryThemesCreateAction | GalleryThemesCreateFailureAction | GalleryThemesCreateSuccessAction |
  GalleryThemesUpdateAction | GalleryThemesUpdateFailureAction | GalleryThemesUpdateSuccessAction |
  GalleryThemesDeleteAction | GalleryThemesDeleteFailureAction | GalleryThemesDeleteSuccessAction |
  GalleryThemesListByParentIdAction |
  GalleryThemesListByParentAction | GalleryThemesListByParentFailureAction |
  GalleryThemesListByParentSuccessAction |
  GalleryThemesResetCurrentThemeAction;
