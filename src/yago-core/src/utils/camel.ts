function _isObject(obj: any): boolean {
  return obj === Object(obj);
}

let toString = Object.prototype.toString;

function _isFunction(obj: any): boolean {
  return typeof(obj) === 'function';
}

function _isArray(obj: any): boolean {
  return toString.call(obj) == '[object Array]';
}

function _isDate(obj: any): boolean {
  return toString.call(obj) == '[object Date]';
}

function _isRegExp(obj: any): boolean {
  return toString.call(obj) == '[object RegExp]';
}

function _isBoolean(obj: any): boolean {
  return toString.call(obj) == '[object Boolean]';
}

// Performant way to determine if obj coerces to a number
function _isNumerical(obj: any): boolean {
  obj = obj - 0;
  return obj === obj;
}

function _processKeys(convert: Function, obj: any, options?: any): any {
  if (!_isObject(obj) || _isDate(obj) || _isRegExp(obj) || _isBoolean(obj) || _isFunction(obj)) {
    return obj;
  }

  let output;
  let i = 0;
  let l = 0;

  if (_isArray(obj)) {
    output = [];
    for (l = obj.length; i < l; i++) {
      output.push(_processKeys(convert, obj[i], options));
    }
  } else {
    output = {};
    for (let key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        output[convert(key, options)] = _processKeys(convert, obj[key], options);
      }
    }
  }
  return output;
}

function separateWords(str: string, options?: any): string {
  options = options || {};
  const separator = options.separator || '_';
  const split = options.split || /(?=[A-Z])/;

  return str.split(split).join(separator);
}

export function camelize(str: string): string {
  if (_isNumerical(str)) {
    return str;
  }
  str = str.replace(/[\-_\s]+(.)?/g, function(_, chr) {
    return chr ? chr.toUpperCase() : '';
  });
  // Ensure 1st char is always lowercase
  return str.substr(0, 1).toLowerCase() + str.substr(1);
}

export function pascalize(str: string): string {
  let camelized = camelize(str);
  // Ensure 1st char is always uppercase
  return camelized.substr(0, 1).toUpperCase() + camelized.substr(1);
}

export function decamelize(str: string, options?: any): string {
  return separateWords(str, options).toLowerCase();
}

// Sets up function which handles processing keys
// allowing the convert function to be modified by a callback
function _processor(convert: Function, options?: any): Function {
  let callback = options && 'process' in options ? options.process : options;

  if (typeof(callback) !== 'function') {
    return convert;
  }

  return function(str, opts?) {
    return callback(str, convert, opts);
  };
}

export function camelizeKeys(object: any, options?: any): any {
  return _processKeys(_processor(camelize, options), object);
}

export function decamelizeKeys(object: any, options?: any): any {
  return _processKeys(_processor(decamelize, options), object, options);
}

export function pascalizeKeys(object: any, options?: any): any {
    return _processKeys(_processor(pascalize, options), object);
}

export function depascalizeKeys(): any {
  return decamelizeKeys.apply(this, arguments);
}
