import { BaseAction } from './base';
import { type } from '../utils/index';
import { ILayer, ILayerTreeItem } from '../interfaces/index';


// tslint:disable-next-line
export const LayersBrowserActionTypes = {
  SHOW: type('[Layers Browser] Show'),
  HIDE: type('[Layers Browser] Hide'),
  RESET_EXPANDED: type('[Layers Browser] Reset expanded'),
  RESET_SELECTION: type('[Layers Browser] Reset selection'),
  TOGGLE_ITEM: type('[Layers Browser] Toggle item'),
  TOGGLE_LAYER: type('[Layers Browser] Toggle layer'),
  SET_FILTER: type('[Layers Browser] Set filter')
};


export class LayersBrowserShowAction implements BaseAction {
  type = LayersBrowserActionTypes.SHOW;
  admin = false;
  constructor(public payload = undefined) { }
}
export class LayersBrowserHideAction implements BaseAction {
  type = LayersBrowserActionTypes.HIDE;
  admin = false;
  constructor(public payload = undefined) { }
}
export class LayersBrowserResetExpandedAction implements BaseAction {
  type = LayersBrowserActionTypes.RESET_EXPANDED;
  admin = false;
  payload = undefined;
  constructor() { }
}
export class LayersBrowserResetSelectionAction implements BaseAction {
  type = LayersBrowserActionTypes.RESET_SELECTION;
  admin = false;
  payload = undefined;
  constructor() { }
}
export class LayersBrowserToggleItemAction implements BaseAction {
  type = LayersBrowserActionTypes.TOGGLE_ITEM;
  admin = false;
  constructor(public payload: ILayerTreeItem) { }
}
export class LayersBrowserToggleLayerAction implements BaseAction {
  type = LayersBrowserActionTypes.TOGGLE_LAYER;
  admin = false;
  constructor(public payload: ILayer) { }
}
export class LayersBrowserSetFilterAction implements BaseAction {
  type = LayersBrowserActionTypes.SET_FILTER;
  admin = false;
  constructor(public payload: string | undefined) { }
}


export type LayersBrowserActions = LayersBrowserShowAction | LayersBrowserHideAction |
  LayersBrowserResetExpandedAction | LayersBrowserResetSelectionAction |
  LayersBrowserToggleItemAction | LayersBrowserToggleLayerAction |
  LayersBrowserSetFilterAction;
