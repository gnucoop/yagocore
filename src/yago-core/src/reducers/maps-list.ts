import { createSelector } from 'reselect';

import { IMapsListState, IPersistentMap } from '../interfaces/index';
import { MapsListActions, MapsListActionTypes } from '../actions/index';


const initialState: IMapsListState = {
  maps: [],
  loading: false,
  selected: null,
  error: null
};

export function mapsListReducer(state = initialState, action: MapsListActions) {
  switch (action.type) {

    case MapsListActionTypes.LIST:
    return Object.assign({}, state, {
      loading: true,
      error: null,
      maps: null
    });

    case MapsListActionTypes.LIST_FAILURE:
    return Object.assign({}, state, {
      loading: false,
      error: action.payload,
      maps: null
    });

    case MapsListActionTypes.LIST_SUCCESS:
    return Object.assign({}, state, {
      loading: false,
      error: null,
      maps: action.payload
    });

    case MapsListActionTypes.SELECT:
    return Object.assign({}, state, {
      error: null
    });

    case MapsListActionTypes.SELECT_FAILURE:
    return Object.assign({}, state, {
      selected: null,
      error: action.payload
    });

    case MapsListActionTypes.SELECT_SUCCESS:
    return Object.assign({}, state, {
      selected: action.payload,
      error: null
    });

    default:
    return state;
  }
}

export function getMapsListLoading(state: IMapsListState): boolean {
  return state.loading;
}
export function getMapsListMaps(state: IMapsListState): IPersistentMap[] {
  return state.maps;
}
export function getMapsListSelectedMapIdx(state: IMapsListState): number | null {
  return state.selected;
}
export const getMapsListSelectedMap =
  createSelector<IMapsListState, IPersistentMap | null, number | null, IPersistentMap[]>(
    getMapsListSelectedMapIdx, getMapsListMaps,
    (selectedMap: number | null, maps: IPersistentMap[]) => {
      if (selectedMap == null || maps == null || selectedMap < 0 || selectedMap >= maps.length) {
        return null;
      }
      return maps[selectedMap];
    }
  );
