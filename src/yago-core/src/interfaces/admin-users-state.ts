import { IApiListOptions } from './api';
import { IUser } from './user';


export interface IAdminUsersState {
  listOptions: IApiListOptions | null;
  item: IUser | null;
  itemLoading: boolean;
  items: IUser[];
  itemsLoading: boolean;
  error: any;
}
