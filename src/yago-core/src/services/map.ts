import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import * as turfHelpers from '@turf/helpers';
import { default as turfCentroid } from '@turf/centroid';
import { default as turfBooleanPointInPolygon } from '@turf/boolean-point-in-polygon';

import {
  ILayer, IMap, IMapLayer, IMapSelectedFeatures, IMapShowFeatureInfo,
  IMapSpatialFilter,
  MapBaseLayer, MapCenter, MapImageFormat, MapLayerZIndex,
  MapPanDirection, MapParams, MapZoomDirection,
  MapTableFilter, MapTableCondition, MapSpatialFilterMode,
  State
} from '../interfaces/index';
import {
  MapSetMapAction, MapSetMapParamsAction,
  MapAddLayerAction, MapAddLayerFailureAction, MapAddLayerSuccessAction,
  MapAssignLayerUniqueIdAction,
  MapPanAction, MapSetCenterAction, MapSetCenterZoomAction, MapZoomAction, MapZoomLevelAction,
  MapViewBackAction, MapViewForwardAction,
  MapToggleLayerVisibilityAction, MapToggleLayerVisibilityFailureAction,
  MapToggleLayerVisibilitySuccessAction,
  MapSelectLayerAction, MapSelectLayerFailureAction, MapSelectLayerSuccessAction,
  MapSelectFeaturesAction, MapSelectFeaturesFailureAction, MapSelectFeaturesSuccessAction,
  MapSaveImageAction, MapSaveImageProgressAction, MapSaveImageSuccessAction,
  MapPrintAction, MapPrintCompleteAction,
  MapUpdateLayerStyleAction,
  MapApplyFilterAction, MapApplyFilterFailureAction, MapApplyFilterSuccessAction,
  MapApplySpatialFilterAction, MapApplySpatialFilterFailureAction,
  MapApplySpatialFilterSuccessAction,
  MapCreateFilteredLayerAction, MapCreateFilteredLayerFailureAction,
  MapCreateFilteredLayerSuccessAction,
  MapRemoveLayerAction,
  MapToggleLegendControlAction, MapToggleTableControlAction,
  MapUpdateLayersOrderAction,
  MapSetBaseLayerAction,
  MapShowFeatureInfoAction, MapHideFeatureInfoAction,
  MapChangeLayerLabelAction, MapChangeLayerLabelFailureAction, MapChangeLayerLabelSuccessAction,
  MapActions, MapActionTypes
} from '../actions/index';
import * as reducers from '../reducers/index';


@Injectable()
export class MapService {
  constructor(private _store: Store<State>, private _actions: Actions) { }

  layerStyleUpdates(): Observable<IMapLayer> {
    return this._actions.ofType(MapActionTypes.UPDATE_LAYER_STYLE)
      .map((action: MapUpdateLayerStyleAction) => action.payload);
  }

  getSelectedLayer(): Observable<IMapLayer | null> {
    return this._store.select(reducers.getSelectedLayer);
  }

  getSelectedFeatures(): Observable<IMapSelectedFeatures[]> {
    return this._store.select(reducers.getSelectedFeatures);
  }

  getMap(): Observable<IMap | null> {
    return this._store.select(reducers.getMap);
  }

  getMapLayers(): Observable<IMapLayer[]> {
    return this._store.select(reducers.getMapLayers);
  }

  getCenter(): Observable<MapCenter> {
    return this._store.select(reducers.getMapCenter);
  }

  getZoomLevel(): Observable<number> {
    return this._store.select(reducers.getZoomLevel);
  }

  setMap(map: IMap): void {
    this._store.dispatch(new MapSetMapAction(map));
  }

  setMapParams(params: MapParams): void {
    this._store.dispatch(new MapSetMapParamsAction(params));
  }

  addLayer(layer: ILayer): void {
    this._store.dispatch(new MapAddLayerAction(layer));
  }

  zoom(direction: MapZoomDirection): void {
    this._store.dispatch(new MapZoomAction(direction));
  }

  setZoomLevel(zoomLevel: number): void {
    this._store.dispatch(new MapZoomLevelAction(zoomLevel));
  }

  pan(direction: MapPanDirection): void {
    this._store.dispatch(new MapPanAction(direction));
  }

  setCenter(center: MapCenter): void {
    this._store.dispatch(new MapSetCenterAction(center));
  }

  setCenterZoom(center: MapCenter, zoomLevel: number): void {
    this._store.dispatch(new MapSetCenterZoomAction({center, zoomLevel}));
  }

  viewBack(): void {
    this._store.dispatch(new MapViewBackAction());
  }

  viewForward(): void {
    this._store.dispatch(new MapViewForwardAction());
  }

  toggleLayerVisibility(layer: IMapLayer): void {
    this._store.dispatch(new MapToggleLayerVisibilityAction(layer.uniqueId));
  }

  selectLayer(layer: IMapLayer): void {
    this._store.dispatch(new MapSelectLayerAction(layer.uniqueId));
  }

  selectFeatures(features: IMapSelectedFeatures[]): void {
    this._store.dispatch(new MapSelectFeaturesAction(features));
  }

  getLayers(): Observable<IMapLayer[]> {
    return this._store.select(reducers.getMapLayers);
  }

  saveImage(format: MapImageFormat): void {
    this._store.dispatch(new MapSaveImageAction(format));
  }

  saveImageInProgress(): void {
    this._store.dispatch(new MapSaveImageProgressAction());
  }

  saveImageSuccess(): void {
    this._store.dispatch(new MapSaveImageSuccessAction());
  }

  getSaveImageRequest(): Observable<MapImageFormat | null> {
    return this._store.select(reducers.getMapSaveImageRequest);
  }

  print(): void {
    this._store.dispatch(new MapPrintAction());
  }

  printComplete(): void {
    this._store.dispatch(new MapPrintCompleteAction());
  }

  getPrint(): Observable<boolean> {
    return this._store.select(reducers.getMapPrint);
  }

  applyFilter(layer: IMapLayer, filter: MapTableFilter): void {
    this._store.dispatch(new MapApplyFilterAction({layer, filter}));
  }

  applySpatialFilter(
    layer: IMapLayer, polygon: number[][], mode: MapSpatialFilterMode = 'centroid'
  ): void {
    this._store.dispatch(new MapApplySpatialFilterAction({layer, polygon, mode}));
  }

  createFilteredLayer(layer: IMapLayer, filter: MapTableFilter): void {
    this._store.dispatch(new MapCreateFilteredLayerAction({layer, filter}));
  }

  getFilteredFeatures(): Observable<IMapSelectedFeatures[]> {
    return this._store.select(reducers.getFilteredFeatures);
  }

  removeLayer(layer: IMapLayer): void {
    this._store.dispatch(new MapRemoveLayerAction(layer));
  }

  getTableControlOpen(): Observable<boolean> {
    return this._store.select(reducers.getMapTableControlOpen);
  }

  toggleTableControl(): void {
    this._store.dispatch(new MapToggleTableControlAction());
  }

  getLegendControlOpen(): Observable<boolean> {
    return this._store.select(reducers.getMapLegendControlOpen);
  }

  toggleLegendControl(): void {
    this._store.dispatch(new MapToggleLegendControlAction());
  }

  updateLayersOrder(order: MapLayerZIndex[]): void {
    this._store.dispatch(new MapUpdateLayersOrderAction(order));
  }

  getBaseLayer(): Observable<MapBaseLayer> {
    return this._store.select(reducers.getMapBaseLayer);
  }

  setBaseLayer(baseLayer: MapBaseLayer): void {
    this._store.dispatch(new MapSetBaseLayerAction(baseLayer));
  }

  showFeatureInfo(feature: IMapShowFeatureInfo): void {
    this._store.dispatch(new MapShowFeatureInfoAction(feature));
  }

  hideFeatureInfo(): void {
    this._store.dispatch(new MapHideFeatureInfoAction());
  }

  getShowFeatureInfo(): Observable<IMapShowFeatureInfo | null> {
    return this._store.select(reducers.getMapShowFeatureInfo);
  }

  changeLayerLabel(layer: IMapLayer, label: string): void {
    return this._store.dispatch(new MapChangeLayerLabelAction({layer, label}));
  }
}

let uniqueLayerId = 0;

@Injectable()
export class MapEffects {
  @Effect()
  setMap$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.SET_MAP)
    .map((action: MapSetMapAction) => action.payload)
    .mergeMap((map: IMap) => {
      return map.layers.map((layer: ILayer) => new MapAddLayerAction(layer));
    });

  @Effect()
  addLayer$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.ADD_LAYER)
    .map((action: MapAddLayerAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .switchMap((r: [ILayer, IMapLayer[]]) =>
      this._layerToMapLayer(r[0], r[1])
        .map((l: IMapLayer) => new MapAddLayerSuccessAction(l))
        .catch((error: any) => Observable.of(new MapAddLayerFailureAction(error)))
    );

  @Effect()
  toggleLayerVisibility$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.TOGGLE_LAYER_VISIBILITY)
    .map((action: MapToggleLayerVisibilityAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [number, IMapLayer[]]) => {
      const layerId = r[0];
      const layers = r[1];
      const layer = layers.find(l => l.uniqueId === layerId);
      if (layer == null) {
        return new MapToggleLayerVisibilityFailureAction('Invalid layer id');
      }
      return new MapToggleLayerVisibilitySuccessAction({id: layerId, visibility: !layer.visible});
    });

  @Effect()
  selectLayer$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.SELECT_LAYER)
    .map((action: MapSelectLayerAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [number, IMapLayer[]]) => {
      const layerId = r[0];
      const layers = r[1];
      const layer = layers.find(l => l.uniqueId === layerId);
      if (layer == null) {
        return new MapSelectLayerFailureAction('Invalid layer id');
      }
      return new MapSelectLayerSuccessAction(layer.uniqueId);
    });

  @Effect()
  selectFeature$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.SELECT_FEATURES)
    .map((action: MapSelectFeaturesAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [IMapSelectedFeatures[], IMapLayer[]]) => {
      const selectedFeatures = r[0];
      const layers = r[1];
      let hasError = false;
      selectedFeatures.forEach((selectedFeature: IMapSelectedFeatures) => {
        const layer = layers.find(l => l.uniqueId === selectedFeature.layer);
        if (layer == null || layer.features == null || layer.features.features == null) {
          hasError = true;
        } else {
          selectedFeature.features.forEach((feature) => {
            const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
            if (features.features.find(f => f.id === feature) == null) {
              hasError = true;
            }
          });
        }
      });
      if (hasError) {
        return new MapSelectFeaturesFailureAction('Invalid feature');
      }
      return new MapSelectFeaturesSuccessAction(selectedFeatures);
    });

  @Effect()
  assignLayerUniqueId$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.ASSIGN_LAYER_UNIQUE_ID)
    .map((action: MapAssignLayerUniqueIdAction) => action.payload)
    .map((layer: IMapLayer) => {
      layer = <IMapLayer>JSON.parse(JSON.stringify(layer));
      layer.uniqueId = ++uniqueLayerId;
      return new MapAddLayerSuccessAction(layer);
    });

  @Effect()
  applyFilter$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.APPLY_FILTER)
    .map((action: MapApplyFilterAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [{layer: IMapLayer, filter: MapTableFilter}, IMapLayer[]]) => {
      const layer = r[0].layer;
      const filter = r[0].filter;
      const layers = r[1];
      if (layers.find(l => l.uniqueId === layer.uniqueId) != null) {
        const selectedFeatures = this._filterLayerFeatures(layer, filter);
        return new MapApplyFilterSuccessAction(selectedFeatures);
      }
      return new MapApplyFilterFailureAction('Invalid layer');
    });

  @Effect()
  applyFilterSuccess$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.APPLY_FILTER_SUCCESS)
    .map((action: MapApplyFilterSuccessAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getSelectedFeatures))
    .map((r: [IMapSelectedFeatures, IMapSelectedFeatures[]]) => {
      let selectedFeatures = r[1].filter(s => s.layer !== r[0].layer);
      selectedFeatures = selectedFeatures.concat(r[0]);
      return new MapSelectFeaturesSuccessAction(selectedFeatures);
    });

  @Effect()
  applySpatialFilter$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.APPLY_SPATIAL_FILTER)
    .map((action: MapApplySpatialFilterAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [IMapSpatialFilter, IMapLayer[]]) => {
      const layer = r[0].layer;
      const polygon = r[0].polygon;
      const mode = r[0].mode;
      const layers = r[1];
      if (layers.find(l => l.uniqueId === layer.uniqueId) != null) {
        const selectedFeatures = this._filterLayerFeaturesByPolygon(layer, polygon, mode);
        return new MapApplyFilterSuccessAction(selectedFeatures);
      }
      return new MapApplyFilterFailureAction('Invalid layer');
    });

  @Effect()
  createFilteredLayer$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.CREATE_FILTERED_LAYER)
    .map((action: MapCreateFilteredLayerAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [{layer: IMapLayer, filter: MapTableFilter}, IMapLayer[]]) => {
      const layer = r[0].layer;
      const filter = r[0].filter;
      const layers = r[1];
      if (layers.find(l => l.uniqueId === layer.uniqueId) != null) {
        const selectedFeatures = this._filterLayerFeatures(layer, filter);
        const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
        const newLayer = Object.assign({}, layer, {
          id: null,
          uniqueId: ++uniqueLayerId,
          label: `Filtered: ${layer.label}`,
          features: Object.assign({}, layer.features, {
            features: features.features
              .filter(f => selectedFeatures.features.indexOf(f.id) > -1)
          })
        });
        return new MapCreateFilteredLayerSuccessAction(newLayer);
      }
      return new MapCreateFilteredLayerFailureAction('Invalid layer');
    });

  @Effect()
  createFilteredLayerSuccess$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.CREATE_FILTERED_LAYER_SUCCESS)
    .map((action: MapCreateFilteredLayerSuccessAction) => action.payload)
    .map((layer: IMapLayer) => new MapAddLayerSuccessAction(layer));

  @Effect()
  showFeatureInfo$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.SHOW_FEATURE_INFO)
    .map((action: MapShowFeatureInfoAction) =>
      new MapSelectFeaturesSuccessAction([action.payload.feature]));

  @Effect()
  changeLayerLabel$: Observable<MapActions> = this._actions
    .ofType(MapActionTypes.CHANGE_LAYER_LABEL)
    .map((action: MapChangeLayerLabelAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [{layer: IMapLayer, label: string}, IMapLayer[]]) => {
      const layer = r[0].layer;
      const label = r[0].label;
      const layers = r[1];
      if (layers.find(l => l.uniqueId === layer.uniqueId) != null) {
        return new MapChangeLayerLabelSuccessAction(Object.assign({}, layer, {label}));
      }
      return new MapChangeLayerLabelFailureAction('Invalid layer');
    });

  constructor(private _actions: Actions, private _store: Store<State>) { }

  private _filterLayerFeaturesByPolygon(
    layer: IMapLayer, polygon: number[][], mode: MapSpatialFilterMode
  ): IMapSelectedFeatures {
    const selectedFeatures: IMapSelectedFeatures = {layer: layer.uniqueId, features: []};
    const gPolygon = turfHelpers.polygon([polygon]);
    const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
    features.features
      .forEach((feature) => {
        let selected = false;
        if (mode === 'centroid') {
          selected = turfBooleanPointInPolygon(turfCentroid(feature), gPolygon);
        } else if (mode === 'whole') {
          let isInside = true;
          const coordinates: number[][] = (<any>feature.geometry).coordinates || [];
          const coordsNum = coordinates.length;
          let i = 0;
          while (isInside && i < coordsNum) {
            isInside = isInside &&
              turfBooleanPointInPolygon(turfHelpers.point(coordinates[i]), gPolygon);
            i++;
          }
          selected = isInside;
        }
        if (selected) {
          selectedFeatures.features.push(feature);
        }
      });
    return selectedFeatures;
  }

  private _filterLayerFeatures(layer: IMapLayer, filter: MapTableFilter): IMapSelectedFeatures {
    const selectedFeatures: IMapSelectedFeatures = {layer: layer.uniqueId, features: []};
    const conditionsNum = filter.conditions.length;
    if (conditionsNum > 0) {
      const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
      features.features
        .forEach((feature) => {
          const props = <any>feature.properties;
          let i = 0;
          let stop = false;
          let result = filter.operator === 'or' ? false : true;
          while (i < conditionsNum && !stop) {
            const condition = filter.conditions[i];
            const cRes = this._evaluateCondition(condition, props);
            switch (filter.operator) {
              case 'and':
              result = result && cRes;
              if (!result) { stop = true; }
              break;
              case 'or':
              result = result || cRes;
              if (result) { stop = true; }
              break;
              case 'not':
              result = result && !cRes;
              if (!result) { stop = true; }
              break;
            }
            i++;
          }
          if (result) {
            selectedFeatures.features.push(feature.id);
          }
        });
    }
    return selectedFeatures;
  }

  private _evaluateCondition(condition: MapTableCondition, props: any): boolean {
    if (condition.field == null) { return false; }
    const val = props[condition.field];
    const param0 = condition.params[0];
    switch (condition.operator) {
      case 'equal':
      return val === param0;
      case 'not_equal':
      return val !== param0;
      case 'less':
      return val < param0;
      case 'greater':
      return val > param0;
      case 'less_equal':
      return val <= param0;
      case 'greater_equal':
      return val >= param0;
      case 'contains':
      return `${val}`.toLowerCase().indexOf(`${param0}`.toLowerCase()) > -1;
      case 'between':
      const param1 = condition.params[1];
      return val >= param0 && val <= param1;
      default:
      return false;
    }
  }

  private _layerToMapLayer(layer: ILayer, layers: IMapLayer[]): Observable<IMapLayer> {
    return new Observable<IMapLayer>((subscriber: Subscriber<IMapLayer>) => {
      let mapLayer: IMapLayer | null = null;
      let zIndex: number;
      if (layers != null && layers.length > 0) {
        zIndex = layers.slice(0).sort((l1, l2) => l1.zIndex - l2.zIndex)[0].zIndex + 1;
      } else {
        zIndex = layer.zIndex != null ? layer.zIndex : 100;
      }
      switch (layer.layerType) {
        case 'VECTOR':
        mapLayer = this._vectorLayerToMapLayer(layer, zIndex);
        break;
        case 'TILE':
        break;
        case 'WMS_RASTER':
        mapLayer = this._wmsLayerToMapLayer(layer, zIndex);
        break;
      }
      if (mapLayer != null) {
        subscriber.next(mapLayer);
        subscriber.complete();
      } else {
        subscriber.error('Invalid layer type');
      }
    });
  }

  private _vectorLayerToMapLayer(layer: ILayer, zIndex: number): IMapLayer {
    let uniqueFeatureId = 0;
    const features: any[] = [];
    const featuresIds: string[] = [];
    const origFeatures = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
    origFeatures.features.forEach((f) => {
      let id = f.id;
      let idStr = `${f.id}`;
      if (f.id == null || featuresIds.indexOf(idStr)) {
        id = idStr = `f_${uniqueFeatureId++}`;
        f = Object.assign({}, f, {
          id: idStr
        });
      }
      featuresIds.push(idStr);
      features.push(f);
    });
    layer = Object.assign({}, layer, {
      features: Object.assign({}, layer.features, {
        features: features
      })
    });
    return {
      id: layer.id,
      uniqueId: ++uniqueLayerId,
      layerType: layer.layerType,
      name: layer.name,
      label: layer.label,
      boundingBox: layer.boundingBox,
      features: layer.features,
      zIndex: zIndex,
      visible: layer.visible != null ? layer.visible : true,
      metadata: layer.metadata,
      dataLabels: layer.dataLabels,
      style: layer.style
    };
  }

  private _wmsLayerToMapLayer(layer: ILayer, zIndex: number): IMapLayer {
    return {
      id: layer.id,
      uniqueId: ++uniqueLayerId,
      layerType: layer.layerType,
      name: layer.name,
      label: layer.label,
      url: layer.url,
      layers: layer.sourceName,
      boundingBox: layer.boundingBox,
      zIndex: zIndex,
      visible: layer.visible != null ? layer.visible : true,
      metadata: layer.metadata,
      dataLabels: layer.dataLabels,
      style: layer.style
    };
  }
}
