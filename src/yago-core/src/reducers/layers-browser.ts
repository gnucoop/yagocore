import { ILayersBrowserState } from '../interfaces/index';
import {
  LayersBrowserActionTypes, LayersBrowserActions,
  LayersBrowserToggleItemAction, LayersBrowserToggleLayerAction
} from '../actions/index';


const initialState: ILayersBrowserState = {
  visible: false,
  selectedLayers: [],
  expandedItems: [],
  filter: null
};


export function layersBrowserReducer(state = initialState, action: LayersBrowserActions) {
  switch (action.type) {

    case LayersBrowserActionTypes.SHOW:
    return Object.assign({}, state, {
      visible: true
    });

    case LayersBrowserActionTypes.HIDE:
    return Object.assign({}, state, {
      visible: false
    });

    case LayersBrowserActionTypes.RESET_EXPANDED:
    return Object.assign({}, state, {
      expandedItems: []
    });

    case LayersBrowserActionTypes.RESET_SELECTION:
    return Object.assign({}, state, {
      selectedLayers: []
    });

    case LayersBrowserActionTypes.TOGGLE_ITEM:
    const expandedItems = state.expandedItems.slice(0);
    const tid = (<LayersBrowserToggleItemAction>action).payload.theme.id;
    const tidx = expandedItems.indexOf(tid);
    if (tidx > -1) {
      expandedItems.splice(tidx, 1);
    } else {
      expandedItems.push(tid);
    }
    return Object.assign({}, state, {
      expandedItems: expandedItems
    });

    case LayersBrowserActionTypes.TOGGLE_LAYER:
    const selectedLayers = state.selectedLayers.slice(0);
    const lid = (<LayersBrowserToggleLayerAction>action).payload.id;
    if (lid != null) {
      const lidx = selectedLayers.indexOf(lid);
      if (lidx > -1) {
        selectedLayers.splice(lidx, 1);
      } else {
        selectedLayers.push(lid);
      }
      return Object.assign({}, state, {
        selectedLayers: selectedLayers
      });
    }

    case LayersBrowserActionTypes.SET_FILTER:
    return Object.assign({}, state, {filter: action.payload});

    default:
    return state;
  }
}

export function getLayersBrowserVisible(state: ILayersBrowserState): boolean {
  return state.visible;
}
export function getLayersBrowserSelected(state: ILayersBrowserState): number[] {
  return state.selectedLayers;
}
export function getLayersBrowserExpanded(state: ILayersBrowserState): number[] {
  return state.expandedItems;
}
export function getLayersBrowserFilter(state: ILayersBrowserState): string | null {
  return state.filter;
}
export function getLayersBrowserIsFiltered(state: ILayersBrowserState): boolean {
  return state.filter != null && state.filter.trim().length > 0;
}
