import { IMapLayer } from './map-layer';
import { IPersistentMap } from './persistent-map';


export interface IMapBuilderState {
  styleEditedLayer: IMapLayer | null;
  transformOptionsVisible: boolean;
  currentTransform: number | null;
  measureControlVisible: boolean;
  elevationProfileControlVisible: boolean;
  spatialAnalysisControlVisible: boolean;
  editedMap: IPersistentMap;
  editedMapChanged: boolean;
  loading: boolean;
  error: any;
  mapsListVisible: boolean;
  mapSaving: boolean;
  mapLoading: boolean;
}
