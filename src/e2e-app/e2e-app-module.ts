import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { E2EAppComponent, HomeComponent } from './e2e-app/e2e-app';
import { MaterialModule } from '@angular/material';
import { E2E_APP_ROUTES } from './e2e-app/routes';


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(E2E_APP_ROUTES),
    NoopAnimationsModule,
    MaterialModule.forRoot(),
  ],
  declarations: [
    E2EAppComponent,
    HomeComponent
  ],
  bootstrap: [E2EAppComponent],
  entryComponents: []
})
export class E2eAppModule { }
