import { ILayerTheme, ILayer, ILayerTreeItem } from '../interfaces/index';


function populateTreeItem(
  theme: ILayerTheme, themes: ILayerTheme[], layers: ILayer[]
): ILayerTreeItem {
  return {
    theme: theme,
    layers: layers.filter(l => l.theme === theme.id || l.theme === theme)
      .sort((l1, l2) => l1.label.toLowerCase().localeCompare(l2.label.toLowerCase())),
    children: themes.filter(t => t.parent === theme.id || t.parent === theme)
      .map((t) => populateTreeItem(t, themes, layers))
  };
}

export function buildLayersTree(themes: ILayerTheme[], layers: ILayer[]): ILayerTreeItem[] {
  return themes.filter(t => t.parent === 0 || t.parent == null)
    .map((t) => populateTreeItem(t, themes, layers));
}
