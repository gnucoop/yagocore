import { FormGroup } from '@angular/forms';

import { default as turfBuffer } from '@turf/buffer';

import { toWgs84 } from 'reproject';

import { IMapLayer } from '../../interfaces/index';
import { crsNameToEpsg, Proj4jsProjections } from '../../utils/index';
import { GeoStatisticTransform, GeoStatisticTransformOption } from './base';


export class BufferTransform extends GeoStatisticTransform {
  private static _options: GeoStatisticTransformOption[] = [{
    name: 'distance',
    label: 'Distance (m)',
    type: 'number',
    required: true
  }, {
    name: 'stroke',
    label: 'Stroke',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'color',
    label: 'Stroke color',
    type: 'color',
    required: true,
    defaultValue: '#0000ff'
  }, {
    name: 'opacity',
    label: 'Stroke opacity',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }, {
    name: 'width',
    label: 'Stroke width',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 20,
    step: 0.5
  }, {
    name: 'fill',
    label: 'Fill',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'fillColor',
    label: 'Fill color',
    type: 'color',
    required: true,
    defaultValue: '#ff0000'
  }, {
    name: 'fillOpacity',
    label: 'Fill opacity',
    type: 'slider',
    required: true,
    defaultValue: 0.7,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }];

  private static _name = 'Buffer';
  static getName(): string { return BufferTransform._name; }

  static getOptions(_layer: IMapLayer): GeoStatisticTransformOption[] {
    return BufferTransform._options;
  }

  static updateForm(form: FormGroup, values: {[key: string]: any}): void {
    const hasStroke = values['stroke'];
    ['color', 'opacity', 'width']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasStroke && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasStroke && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });
    const hasFill = values['fill'];
    ['fillColor', 'fillOpacity']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasFill && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasFill && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });
  }

  static transform(layer: IMapLayer, options: {[key: string]: any}): IMapLayer {
    let newLayer: IMapLayer = crsNameToEpsg(JSON.parse(JSON.stringify(layer)));
    const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>toWgs84(
      <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>newLayer.features,
      <any>null, Proj4jsProjections
    );
    newLayer = Object.assign({}, newLayer, {
      uniqueId: null,
      features: {
        type: features.type,
        bbox: features.bbox,
        features: features.features.map(f => {
          const nf = turfBuffer(f, options['distance'], {units: 'meters'});
          return Object.assign(nf, {
            properties: f.properties
          });
        })
      },
      style: Object.assign({}, newLayer.style, {
        stroke: options['stroke'],
        color: options['color'],
        opacity: options['opacity'],
        width: options['width'],
        fillColor: options['fillColor'],
        fillOpacity: options['fillOpacity']
      })
    });
    return newLayer;
  }
}
