import { BaseAction } from './base';
import { type } from '../utils/index';
import {
  IApiListOptions, ILayer, IWMSSource, IWMSSourceAvabilableLayer
} from '../interfaces/index';


// tslint:disable-next-line
export const WMSSourcesActionTypes = {
  GET: type('[WMS Sources] Get'),
  GET_FAILURE: type('[WMS Sources] Get failure'),
  GET_SUCCESS: type('[WMS Sources] Get success'),
  LIST: type('[WMS Sources] List'),
  LIST_FAILURE: type('[WMS Sources] List failure'),
  LIST_SUCCESS: type('[WMS Sources] List success'),
  CREATE: type('[WMS Sources] Create'),
  CREATE_FAILURE: type('[WMS Sources] Create failure'),
  CREATE_SUCCESS: type('[WMS Sources] Create success'),
  UPDATE: type('[WMS Sources] Update'),
  UPDATE_FAILURE: type('[WMS Sources] Update failure'),
  UPDATE_SUCCESS: type('[WMS Sources] Update success'),
  DELETE: type('[WMS Sources] Delete'),
  DELETE_FAILURE: type('[WMS Sources] Delete failure'),
  DELETE_SUCCESS: type('[WMS Sources] Delete success'),
  GET_LAYER: type('[WMS Sources] Get layer'),
  GET_LAYER_FAILURE: type('[WMS Sources] Get layer failure'),
  GET_LAYER_SUCCESS: type('[WMS Sources] Get layer success'),
  GET_LAYERS: type('[WMS Sources] Get layers'),
  GET_LAYERS_FAILURE: type('[WMS Sources] Get layers failure'),
  GET_LAYERS_SUCCESS: type('[WMS Sources] Get layers success'),
  IMPORT_LAYER: type('[WMS Sources] Import layer'),
  IMPORT_LAYER_FAILURE: type('[WMS Sources] Import layer failure'),
  IMPORT_LAYER_SUCCESS: type('[WMS Sources] Import layer success'),
  RESET_CURRENT_WMS_SOURCE: type('[WMS Sources] Reset current WMS source')
};

export class WMSSourcesGetAction implements BaseAction {
  type = WMSSourcesActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}

export class WMSSourcesGetFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesGetSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_SUCCESS;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesListAction implements BaseAction {
  type = WMSSourcesActionTypes.LIST;
  constructor(public payload?: IApiListOptions, public admin = false) { }
}

export class WMSSourcesListFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.LIST_FAILURE;

  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesListSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.LIST_SUCCESS;
  constructor(public payload: IWMSSource[], public admin = false) { }
}

export class WMSSourcesCreateAction implements BaseAction {
  type = WMSSourcesActionTypes.CREATE;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesCreateFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.CREATE_FAILURE;

  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesCreateSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.CREATE_SUCCESS;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesUpdateAction implements BaseAction {
  type = WMSSourcesActionTypes.UPDATE;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesUpdateFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesUpdateSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesDeleteAction implements BaseAction {
  type = WMSSourcesActionTypes.DELETE;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesDeleteFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesDeleteSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.DELETE_SUCCESS;
  constructor(public payload: IWMSSource, public admin = false) { }
}

export class WMSSourcesGetLayerAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYER;
  constructor(
    public payload: {id: number, name: string, workspace: string}, public admin = false
  ) { }
}

export class WMSSourcesGetLayerFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesGetLayerSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYER_SUCCESS;
  constructor(public payload: IWMSSourceAvabilableLayer, public admin = false) { }
}

export class WMSSourcesGetLayersAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYERS;
  constructor(public payload: number, public admin = false) { }
}

export class WMSSourcesGetLayersFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYERS_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesGetLayersSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.GET_LAYERS_SUCCESS;
  constructor(public payload: IWMSSourceAvabilableLayer[], public admin = false) { }
}

export class WMSSourcesImportLayerAction implements BaseAction {
  type = WMSSourcesActionTypes.IMPORT_LAYER;
  constructor(
    public payload: {id: number, name: string, workspace: string, data: any}, public admin = false
  ) { }
}

export class WMSSourcesImportLayerFailureAction implements BaseAction {
  type = WMSSourcesActionTypes.IMPORT_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class WMSSourcesImportLayerSuccessAction implements BaseAction {
  type = WMSSourcesActionTypes.IMPORT_LAYER_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}
export class WMSSourcesResetCurrentWMSSource implements BaseAction {
  type = WMSSourcesActionTypes.RESET_CURRENT_WMS_SOURCE;
  payload = undefined;
  constructor(public admin = false) { }
}

export type WMSSourcesActions =
  WMSSourcesGetAction | WMSSourcesGetFailureAction |
  WMSSourcesGetSuccessAction |
  WMSSourcesListAction | WMSSourcesListFailureAction |
  WMSSourcesListSuccessAction |
  WMSSourcesCreateAction | WMSSourcesCreateFailureAction |
  WMSSourcesCreateSuccessAction |
  WMSSourcesUpdateAction | WMSSourcesUpdateFailureAction |
  WMSSourcesUpdateSuccessAction |
  WMSSourcesDeleteAction | WMSSourcesDeleteFailureAction |
  WMSSourcesDeleteSuccessAction |
  WMSSourcesGetLayerAction | WMSSourcesGetLayerFailureAction |
  WMSSourcesGetLayerSuccessAction |
  WMSSourcesGetLayersAction | WMSSourcesGetLayersFailureAction |
  WMSSourcesGetLayersSuccessAction |
  WMSSourcesImportLayerAction | WMSSourcesImportLayerFailureAction |
  WMSSourcesImportLayerSuccessAction |
  WMSSourcesResetCurrentWMSSource;
