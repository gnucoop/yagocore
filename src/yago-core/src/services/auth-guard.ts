import { Inject, Injectable, Optional } from '@angular/core';
import {
  CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild
} from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth-service';
import { AuthConfigOptional, tokenNotExpired, AUTH_CONFIG } from './http';


@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(AUTH_CONFIG) @Optional() private options?: AuthConfigOptional
  ) {}

  canActivate(_route: ActivatedRouteSnapshot, _state: RouterStateSnapshot): boolean {
    return this._checkLogin();
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(route, state);
  }

  private _checkLogin(): boolean {
    return tokenNotExpired(
      this.options != null && this.options.tokenName ?
      this.options.tokenName :
      undefined
    );
  }
}


@Injectable()
export class AdminAuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private _authService: AuthService,
    private _router: Router,
    private _authGuard: AuthGuard,
    @Inject(AUTH_CONFIG) @Optional() _options?: AuthConfigOptional
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._authService.getUserIsStaff()
      .map((isStaff: boolean) => this._authGuard.canActivate(route, state) && isStaff);
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.canActivate(route, state);
  }
}
