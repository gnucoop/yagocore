import { FormGroup } from '@angular/forms';

import { IMapLayer } from '../../interfaces/index';

export type GeoStatisticIndicatorType = 'string' | 'number';

export type GeoStatisticTransformOptionType =
  'string' | 'number' | 'slider' | 'color' | 'boolean' | 'select';

export interface GeoStatisticTransformOption {
  name: string;
  label: string;
  type: GeoStatisticTransformOptionType;
  required: boolean;
  defaultValue?: any;
  minValue?: number;
  maxValue?: number;
  step?: number;
  choices?: {label: string, value: any}[];
}

export abstract class GeoStatisticTransform {
  static getName(): string {
    throw new Error('Not implemented');
  }
  static getOptions(_layer: IMapLayer): GeoStatisticTransformOption[] {
    throw new Error('Not implemented');
  }
  static updateForm(_form: FormGroup, _values: {[key: string]: any}): void {
    throw new Error('Not implemented');
  }
  static transform(_layer: IMapLayer, _options: {[key: string]: any}): IMapLayer {
    throw new Error('Not implemented');
  }
}
