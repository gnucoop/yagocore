import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { ILayer, State } from '../interfaces/index';
import {
  LayersActions, LayersActionTypes,
  LayersGetAction, LayersGetFailureAction, LayersGetSuccessAction,
  LayersListAction, LayersListFailureAction, LayersListSuccessAction,
  LayersCreateAction, LayersCreateFailureAction, LayersCreateSuccessAction,
  LayersUpdateAction, LayersUpdateFailureAction, LayersUpdateSuccessAction,
  LayersDeleteAction, LayersDeleteFailureAction, LayersDeleteSuccessAction,
  Back, RouterActions
} from '../actions/index';
import { ModelService } from './model';
import { LayersManager } from './layers-manager';


@Injectable()
export class LayersService extends ModelService<
    ILayer, LayersGetAction, LayersListAction, LayersCreateAction, LayersUpdateAction,
    LayersDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, LayersGetAction, LayersListAction, LayersCreateAction, LayersUpdateAction,
      LayersDeleteAction
    );
  }
}


@Injectable()
export class LayersEffects {
  @Effect()
  get$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.GET)
    .switchMap((action: LayersGetAction) =>
      this._manager.get(action.payload)
        .map((layer: ILayer) => new LayersGetSuccessAction(layer, action.admin))
        .catch((error: any) => Observable.of(new LayersGetFailureAction(error, action.admin)))
    );

  @Effect()
  list$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.LIST)
    .switchMap((action: LayersListAction) =>
      this._manager.list(action.payload)
        .map((layers: ILayer[]) => new LayersListSuccessAction(layers, action.admin))
        .catch((error: any) => Observable.of(new LayersListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.CREATE)
    .switchMap((action: LayersCreateAction) =>
      this._manager.create(action.payload)
        .map((s: ILayer) => new LayersCreateSuccessAction(s, action.admin))
        .catch((error: any) => Observable.of(new LayersCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.UPDATE)
    .filter((action: LayersUpdateAction) => action.payload.id != null)
    .switchMap((action: LayersUpdateAction) =>
      this._manager.update(<number>action.payload.id, action.payload)
        .map((s: ILayer) => new LayersUpdateSuccessAction(s, action.admin))
        .catch((error: any) => Observable.of(new LayersUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(LayersActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(LayersActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.DELETE)
    .filter((action: LayersDeleteAction) => action.payload.id != null)
    .switchMap((action: LayersDeleteAction) =>
      this._manager.delete(<number>action.payload.id)
        .map(() => new LayersDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new LayersDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<LayersActions> = this._actions
    .ofType(LayersActionTypes.DELETE_SUCCESS)
    .map((action: LayersDeleteSuccessAction) => new LayersListAction(undefined, action.admin));

  constructor(
    private _actions: Actions, private _manager: LayersManager
  ) { }
}
