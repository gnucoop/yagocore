import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { Effect, Actions } from '@ngrx/effects';

import {
  Go, Back, Forward, RouterActions, RouterActionTypes
} from '../actions/router';


@Injectable()
export class RouterEffects {
  @Effect({ dispatch: false })
  navigate$: Observable<any> = this.actions$.ofType(RouterActionTypes.GO)
    .map((action: Go) => action.payload)
    .do(({ path, query: queryParams, extras}) =>
      this.router.navigate(path, { queryParams, ...extras }));

  @Effect({ dispatch: false })
  navigateBack$: Observable<any> = this.actions$.ofType(RouterActionTypes.BACK)
    .do(() => this.location.back());

  @Effect({ dispatch: false })
  navigateForward$: Observable<any> = this.actions$.ofType(RouterActionTypes.FORWARD)
    .do(() => this.location.forward());

  constructor(
    private actions$: Actions,
    private router: Router,
    private location: Location
  ) {}
}
