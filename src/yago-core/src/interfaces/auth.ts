import { IUser } from './user';


export interface IAuth {
  username: string;
  password: string;
}

export interface IAuthResponse {
  token: string;
  user: IUser;
}

export interface IAuthRegistration {
  username: string;
  email: string;
  password: string;
  confirm_password: string;
}

export interface IAuthResetPassword {
  uid: string;
  token: string;
  new_password1: string;
  new_password2: string;
}
