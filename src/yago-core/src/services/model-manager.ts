import { Observable } from 'rxjs/Observable';

import { removeSlashes } from '../utils/index';
import { IApiListOptions } from '../interfaces/index';
import { ApiService } from './api';


export abstract class ModelManager<T> {
  private _endPoint: string;
  protected get endPoint(): string { return this._endPoint; }

  private _unrestrictedMethods: string[] = ['get', 'list', 'create', 'update', 'delete'];

  constructor(
    apiRoute: string[], protected _apiService: ApiService, unrestrictedMethods?: string[]
  ) {
    this._endPoint = apiRoute.map(r => removeSlashes(r)).join('/');
    if (unrestrictedMethods != null) {
      this._unrestrictedMethods = unrestrictedMethods.slice(0);
    }
  }

  get(id: number): Observable<T> {
    const auth = this._authForMethod('get');
    return <Observable<T>>this._apiService.get(this._endPoint, id, auth);
  }

  list(options?: IApiListOptions): Observable<T[]> {
    const auth = this._authForMethod('list');
    return <Observable<T[]>>this._apiService.list(this._endPoint, options, auth);
  }

  create(data: T): Observable<T> {
    const auth = this._authForMethod('create');
    return <Observable<T>>this._apiService.create(this._endPoint, data, auth);
  }

  update(id: number, data: T): Observable<T> {
    const auth = this._authForMethod('update');
    return <Observable<T>>this._apiService.update(this._endPoint, id, data, auth);
  }

  delete(id: number): Observable<T> {
    const auth = this._authForMethod('delete');
    return <Observable<T>>this._apiService.delete(this._endPoint, id, auth);
  }

  private _authForMethod(method: string): boolean {
    return this._unrestrictedMethods.indexOf(method) === -1;
  }
}
