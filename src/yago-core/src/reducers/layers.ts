import { IThemesState, ILayersState, ILayer, ILayerTreeItem } from '../interfaces';
import { buildLayersTree } from '../utils/index';
import { LayersActionTypes, LayersActions } from '../actions/index';


const initialState: ILayersState = {
  listOptions: null,
  items: [],
  itemsLoading: false,
  item: null,
  itemLoading: false,
  error: null
};


export function layersReducer(state = initialState, action: LayersActions) {
  if (!action.admin) {
    switch (action.type) {

      case LayersActionTypes.GET:
      return Object.assign({}, state, {
        item: null,
        itemLoading: true,
        error: null
      });

      case LayersActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        item: action.payload,
        itemLoading: false,
        error: null
      });

      case LayersActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        item: null,
        itemLoading: false,
        error: action.payload
      });

      case LayersActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case LayersActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case LayersActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      default:
      return state;

    }
  }
  return state;
}

export function getLayer(state: ILayersState): ILayer | null {
  return state.item;
}
export function getLayerLoading(state: ILayersState): boolean {
  return state.itemLoading;
}
export function getLayers(state: ILayersState): ILayer[] {
  return state.items;
}
export function getLayersLoading(state: ILayersState): boolean {
  return state.itemsLoading;
}
export function getLayersTree(
  themesState: IThemesState, layersState: ILayersState
): ILayerTreeItem[] {
  return buildLayersTree(themesState.items || [], layersState.items || []);
}
