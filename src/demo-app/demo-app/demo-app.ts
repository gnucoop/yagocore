import {Component, ViewEncapsulation} from '@angular/core';


@Component({
  selector: 'app-home',
  template: `
  `
})
export class HomeComponent {}

@Component({
  moduleId: module.id,
  selector: 'app-demo-app',
  providers: [],
  templateUrl: 'demo-app.html',
  styleUrls: ['demo-app.css'],
  encapsulation: ViewEncapsulation.None,
})
export class DemoAppComponent {
  navItems: {name: string, route: string}[] = [
    {name: 'Map builder', route: '/map-builder'}
  ];
}
