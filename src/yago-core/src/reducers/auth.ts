import { IAuthLoginPanel, IAuthState, IUser } from '../interfaces/index';
import { AuthActions, AuthActionTypes } from '../actions/index';


const initialState: IAuthState = {
  error: null,
  token: null,
  user: null,
  loading: false,
  loginDialogVisible: false,
  loginPanel: 'login',
  recoverPasswordMailSent: false
};


export function authReducer(
  state: IAuthState = initialState, action: AuthActions
): IAuthState {
  switch (action.type) {

    case AuthActionTypes.INIT_TOKEN:
    return Object.assign({}, state, {
      token: action.payload
    });

    case AuthActionTypes.INIT_USER:
    return Object.assign({}, state, {
      user: action.payload
    });

    case AuthActionTypes.LOGIN:
    case AuthActionTypes.LOGOUT:
    return Object.assign({}, state, {
      loading: true
    });

    case AuthActionTypes.LOGIN_SUCCESS:
    return Object.assign({}, state, {
      token: action.payload.token,
      user: action.payload.user,
      loading: false
    });

    case AuthActionTypes.LOGOUT_SUCCESS:
    return Object.assign({}, state, {
      token: null,
      user: null,
      loading: false
    });

    case AuthActionTypes.TOKEN_REFRESH_SUCCESS:
    return Object.assign({}, state, {
      token: action.payload.token,
      user: action.payload.user
    });

    case AuthActionTypes.TOKEN_REFRESH_FAILURE:
    const error = action.payload;
    if (error.code === 400) {
      return Object.assign({}, state, {
        token: null,
        user: null,
        error: error
      });
    }
    break;

    case AuthActionTypes.LOGIN_FAILURE:
    case AuthActionTypes.LOGOUT_FAILURE:
    return Object.assign({}, state, {
      loading: false
    });

    default:
    return state;

    case AuthActionTypes.LOGIN_DIALOG_SHOW:
    return Object.assign({}, state, {
      loginDialogVisible: true
    });

    case AuthActionTypes.LOGIN_DIALOG_HIDE:
    return Object.assign({}, state, {
      loginPanel: 'login',
      loginDialogVisible: false,
      recoverPasswordMailSent: false
    });

    case AuthActionTypes.SELECT_LOGIN_PANEL:
    return Object.assign({}, state, {
      loginPanel: action.payload
    });

    case AuthActionTypes.REGISTER:
    return Object.assign({}, state, {
      loading: true,
      error: null
    });

    case AuthActionTypes.REGISTER_SUCCESS:
    return Object.assign({}, state, {
      loading: false,
      error: null
    });

    case AuthActionTypes.REGISTER_FAILURE:
    return Object.assign({}, state, {
      loading: false,
      error: action.payload
    });

    case AuthActionTypes.RECOVER_PASSWORD:
    return Object.assign({}, state, {
      loading: true,
      error: null,
      recoverPasswordMailSent: false
    });

    case AuthActionTypes.RECOVER_PASSWORD_FAILURE:
    return Object.assign({}, state, {
      loading: false,
      error: action.payload,
      recoverPasswordMailSent: false
    });

    case AuthActionTypes.RECOVER_PASSWORD_SUCCESS:
    return Object.assign({}, state, {
      loading: false,
      error: null,
      recoverPasswordMailSent: true
    });

    case AuthActionTypes.RESET_PASSWORD:
    return Object.assign({}, state, {
      loading: false,
      error: null
    });

    case AuthActionTypes.RESET_PASSWORD_FAILURE:
    return Object.assign({}, state, {
      loading: false,
      error: action.payload
    });

    case AuthActionTypes.RECOVER_PASSWORD_SUCCESS:
    return Object.assign({}, state, {
      loading: false,
      error: null
    });
  }

  return state;
}

export function getLoggedIn(state: IAuthState): boolean {
  return state.token != null;
}
export function getAuthToken(state: IAuthState): string | null {
  return state.token;
}
export function getAuthLoading(state: IAuthState): boolean {
  return state.loading;
}
export function getAuthUser(state: IAuthState): IUser | null {
  return state.user;
}
export function getLoginPanel(state: IAuthState): IAuthLoginPanel {
  return state.loginPanel;
}
export function getLoginDialogVisible(state: IAuthState): boolean {
  return state.loginDialogVisible;
}
export function getRecoverPasswordMailSent(state: IAuthState): boolean {
  return state.recoverPasswordMailSent;
}
export function getAuthUserIsStaff(state: IAuthState): boolean {
  return state.user != null && state.user.isStaff;
}
