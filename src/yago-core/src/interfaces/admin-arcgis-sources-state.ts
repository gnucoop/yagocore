import { IApiListOptions } from './api';
import { IArcGISSource, IArcGISSourceAvabilableLayer } from './arcgis-source';


export interface IAdminArcGISSourcesState {
  listOptions: IApiListOptions | null;
  item: IArcGISSource | null;
  items: IArcGISSource[];
  layer: IArcGISSourceAvabilableLayer | null;
  layerLoading: boolean;
  layers: IArcGISSourceAvabilableLayer[];
  itemLoading: boolean;
  itemsLoading: boolean;
  layersLoading: boolean;
  error: any;
}
