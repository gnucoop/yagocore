export interface ILayersBrowserState {
  visible: boolean;
  selectedLayers: number[];
  expandedItems: number[];
  filter: string | null;
}
