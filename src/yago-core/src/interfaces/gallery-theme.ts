export interface IGalleryTheme {
  id: number;
  name: string;
  parent?: number | IGalleryTheme;
}
