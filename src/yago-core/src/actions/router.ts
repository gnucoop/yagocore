import { Action } from '@ngrx/store';
import { NavigationExtras } from '@angular/router';
import { type } from '../utils/index';


// tslint:disable-next-line
export const RouterActionTypes = {
  GO: type('[Router] Go'),
  BACK: type('[Router] Back'),
  FORWARD: type('[Router] Forward')
};


export class Go implements Action {
  readonly type = RouterActionTypes.GO;

  constructor(public payload: {
    path: any[];
    query?: object;
    extras?: NavigationExtras;
  }) {}
}

export class Back implements Action {
  readonly type = RouterActionTypes.BACK;
}

export class Forward implements Action {
  readonly type = RouterActionTypes.FORWARD;
}

export type RouterActions = Go | Back | Forward;
