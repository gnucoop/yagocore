export interface IApiListOptions {
  limit?: number;
  skip?: number;
  sort?: string[] | {
    [propName: string]: 'asc' | 'desc'
  }[];
}
