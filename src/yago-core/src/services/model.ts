import { Store } from '@ngrx/store';

import { IApiListOptions, State } from '../interfaces/index';
import { BaseAction } from '../actions/index';


function createGetAction<T extends BaseAction>(
  type: { new(id: number, admin?: boolean): T }, id: number, admin?: boolean
): T {
  return new type(id, admin);
}
function createListAction<T extends BaseAction>(
  type: { new(options?: IApiListOptions, admin?: boolean): T }, options?: IApiListOptions,
  admin?: boolean
): T {
  return new type(options, admin);
}
function createCreateAction<T extends BaseAction, M>(
  type: { new(data: M, admin?: boolean): T }, data: M, admin?: boolean
): T {
  return new type(data, admin);
}
function createUpdateAction<T extends BaseAction, M>(
  type: { new(data: M, admin?: boolean): T }, data: M, admin?: boolean
): T {
  return new type(data, admin);
}
function createDeleteAction<T extends BaseAction, M>(
  type: { new(data: M, admin?: boolean): T }, data: M, admin?: boolean
): T {
  return new type(data, admin);
}


export abstract class ModelService<
  T, A1 extends BaseAction, A2 extends BaseAction, A3 extends BaseAction, A4 extends BaseAction,
  A5 extends BaseAction> {
  constructor(
    protected _store: Store<State>,
    private _getAction: { new(id: number): A1 },
    private _listAction: { new(options?: IApiListOptions): A2 },
    private _createAction: { new(data: T): A3 },
    private _updateAction: { new(data: T): A4 },
    private _deleteAction: { new(data: T): A5 }
  ) { }

  get(id: number, admin = false): void {
    this._store.dispatch(createGetAction<A1>(this._getAction, id, admin));
  }

  list(options?: IApiListOptions, admin = false): void {
    this._store.dispatch(createListAction<A2>(this._listAction, options, admin));
  }

  create(data: T, admin = false): void {
    this._store.dispatch(createCreateAction<A3, T>(this._createAction, data, admin));
  }

  update(data: T, admin = false): void {
    this._store.dispatch(createUpdateAction<A4, T>(this._updateAction, data, admin));
  }

  delete(data: T, admin = false): void {
    this._store.dispatch(createDeleteAction<A5, T>(this._deleteAction, data, admin));
  }
}
