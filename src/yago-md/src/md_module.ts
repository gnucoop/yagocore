import { NgModule, ModuleWithProviders } from '@angular/core';


@NgModule({
  imports: [],
  exports: [],
  declarations: [],
  entryComponents: []
})
export class YagoMdModule {
  static forRoot(): ModuleWithProviders {
    return {ngModule: YagoMdModule};
  }
}
