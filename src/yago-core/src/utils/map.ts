import { IMap, IMapLayer, IPersistentMap, ILayer, MapBaseLayer } from '../interfaces/index';


export function persistentMapToMap(pMap: IPersistentMap): IMap {
  const id: number = <number>pMap.id;
  const name: string = <string>pMap.name;
  const layers: ILayer[] = pMap.content != null && pMap.content.layers != null ?
    pMap.content.layers.map((l) => <ILayer>Object.assign({}, l)) :
    [];
  const baseLayer = (<{baseLayer: MapBaseLayer, layers: IMapLayer[]}>pMap.content).baseLayer;
  return {id, name, layers, baseLayer};
}

export const defaultMapStyle = {
  radius: 8,
  stroke: true,
  fillColor: '#ff7800',
  color: '#ff0000',
  weight: 1,
  opacity: 1,
  fillOpacity: 0.8
};

export const defCrs: GeoJSON.CoordinateReferenceSystem = {
  type: 'EPSG',
  properties: {
    code: 4326
  }
};

export function crsNameToEpsg(
  layer: IMapLayer,
  defaultCrs: GeoJSON.CoordinateReferenceSystem = defCrs
): IMapLayer {
  const origFeatures = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features;
  const crs = origFeatures.crs;
  if (crs == null) {
    return Object.assign({}, layer, {
      features: Object.assign({}, layer.features, {
        crs: defaultCrs
      })
    });
  }
  if (crs.type === 'name' && crs.properties.name) {
    const epsgPrefix = 'urn:ogc:def:crs:EPSG::';
    const crsName: string = crs.properties.name;
    if (crsName.startsWith(epsgPrefix)) {
      try {
        const newCrs: GeoJSON.CoordinateReferenceSystem = {
          type: 'EPSG',
          properties: {
            code: parseInt(crsName.substring(epsgPrefix.length), 10)
          }
        };
        const features = Object.assign({}, layer.features, {
          crs: newCrs
        });
        layer = Object.assign({}, layer, {
          features: features
        });
      } catch (e) { console.log(e); }
    }
  }
  return layer;
}
