import { BaseAction } from './base';
import { IMapLayerStyle, IMapLayer, IPersistentMap } from '../interfaces/index';
import { type } from '../utils/index';


// tslint:disable-next-line
export const MapBuilderActionTypes = {
  INIT: type('[Map Builder] Init'),
  ADD_SELECTED_LAYERS: type('[Map Builder] Add selected layers'),
  ADD_SELECTED_LAYER: type('[Map Builder] Add selected layer'),
  ADD_SELECTED_LAYER_FAILURE: type('[Map Builder] Add selected layer failure'),
  EDIT_LAYER_STYLE: type('[Map Builder] Edit layer style'),
  SAVE_LAYER_STYLE: type('[Map Builder] Save layer style'),
  CANCEL_EDIT_LAYER_STYLE: type('[Map Builder] Cancel edit layer style'),
  SHOW_TRANSFORM_OPTIONS: type('[Map Builder] Show transform options'),
  HIDE_TRANSFORM_OPTIONS: type('[Map Builder] Hide transform options'),
  CREATE_GEOSTATISTICAL_LAYER: type('[Map Builder] Create geostatiscal layer'),
  CREATE_GEOSTATISTICAL_LAYER_FAILURE: type('[Map Builder] Create geostatiscal layer failure'),
  CREATE_GEOSTATISTICAL_LAYER_SUCCESS: type('[Map Builder] Create geostatiscal layer success'),
  TOGGLE_MEASURE_CONTROL: type('[Map Builder] Toggle measure control'),
  TOGGLE_ELEVATION_PROFILE_CONTROL: type('[Map Builder] Toggle elevation profile control'),
  OPEN_SPATIAL_ANALYSIS_CONTROL: type('[Map Builder] Open spatial analysis control'),
  CLOSE_SPATIAL_ANALYSIS_CONTROL: type('[Map Builder] Close spatial analysis control'),
  TOGGLE_MAPS_LIST: type('[Map Builder] Toggle maps list'),
  LOAD_MAP: type('[Map Builder] Load map'),
  LOAD_MAP_FAILURE: type('[Map Builder] Load map failure'),
  LOAD_MAP_SUCCESS: type('[Map Builder] Load map success'),
  SET_EDITED_MAP_NAME: type('[Map Builder] Set edited map name'),
  SAVE_MAP: type('[Map Builder] Save map'),
  SAVE_MAP_FAILURE: type('[Map Builder] Save map failure'),
  SAVE_MAP_SUCCESS: type('[Map Builder] Save map success'),
  DELETE_MAP: type('[Map Builder] Delete map'),
  DELETE_MAP_FAILURE: type('[Map Builder] Delete map failure'),
  DELETE_MAP_SUCCESS: type('[Map Builder] Delete map success'),
};

export class MapBuilderInitAction implements BaseAction {
  type = MapBuilderActionTypes.INIT;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderAddSelectedLayersAction implements BaseAction {
  type = MapBuilderActionTypes.ADD_SELECTED_LAYERS;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderAddSelectedLayerAction implements BaseAction {
  type = MapBuilderActionTypes.ADD_SELECTED_LAYER;
  admin = false;
  constructor(public payload: number) { }
}
export class MapBuilderAddSelectedLayerFailureAction implements BaseAction {
  type = MapBuilderActionTypes.ADD_SELECTED_LAYER_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapBuilderEditLayerStyleAction implements BaseAction {
  type = MapBuilderActionTypes.EDIT_LAYER_STYLE;
  admin = false;
  constructor(public payload: IMapLayer) { }
}
export class MapBuilderSaveLayerStyleAction implements BaseAction {
  type = MapBuilderActionTypes.SAVE_LAYER_STYLE;
  admin = false;
  constructor(public payload: {layer: IMapLayer, style: IMapLayerStyle}) { }
}
export class MapBuilderCancelEditLayerStyleAction implements BaseAction {
  type = MapBuilderActionTypes.CANCEL_EDIT_LAYER_STYLE;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderShowTransformOptionsAction implements BaseAction {
  type = MapBuilderActionTypes.SHOW_TRANSFORM_OPTIONS;
  admin = false;
  constructor(public payload: number) { }
}
export class MapBuilderHideTransformOptionsAction implements BaseAction {
  type = MapBuilderActionTypes.HIDE_TRANSFORM_OPTIONS;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderCreateGeoStatisticalLayerAction implements BaseAction {
  type = MapBuilderActionTypes.CREATE_GEOSTATISTICAL_LAYER;
  admin = false;
  constructor(public payload: {layer: number, transform: number, options: {[key: string]: any}}) { }
}
export class MapBuilderCreateGeoStatisticalLayerFailureAction implements BaseAction {
  type = MapBuilderActionTypes.CREATE_GEOSTATISTICAL_LAYER_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapBuilderCreateGeoStatisticalLayerSuccessAction implements BaseAction {
  type = MapBuilderActionTypes.CREATE_GEOSTATISTICAL_LAYER_SUCCESS;
  admin = false;
  constructor(public payload: IMapLayer) { }
}
export class MapBuilderToggleMeasureControlAction implements BaseAction {
  type = MapBuilderActionTypes.TOGGLE_MEASURE_CONTROL;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderToggleElevationProfileControlAction implements BaseAction {
  type = MapBuilderActionTypes.TOGGLE_ELEVATION_PROFILE_CONTROL;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderOpenSpatialAnalysisControlAction implements BaseAction {
  type = MapBuilderActionTypes.OPEN_SPATIAL_ANALYSIS_CONTROL;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderCloseSpatialAnalysisControlAction implements BaseAction {
  type = MapBuilderActionTypes.CLOSE_SPATIAL_ANALYSIS_CONTROL;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderToggleMapsListAction implements BaseAction {
  type = MapBuilderActionTypes.TOGGLE_MAPS_LIST;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderLoadMapAction implements BaseAction {
  type = MapBuilderActionTypes.LOAD_MAP;
  admin = false;
  constructor(public payload: number) { }
}
export class MapBuilderLoadMapFailureAction implements BaseAction {
  type = MapBuilderActionTypes.LOAD_MAP_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapBuilderLoadMapSuccessAction implements BaseAction {
  type = MapBuilderActionTypes.LOAD_MAP_SUCCESS;
  admin = false;
  constructor(public payload: IPersistentMap) { }
}
export class MapBuilderSetEditedMapName implements BaseAction {
  type = MapBuilderActionTypes.SET_EDITED_MAP_NAME;
  admin = false;
  constructor(public payload: string) { }
}
export class MapBuilderSaveMapAction implements BaseAction {
  type = MapBuilderActionTypes.SAVE_MAP;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapBuilderSaveMapFailureAction implements BaseAction {
  type = MapBuilderActionTypes.SAVE_MAP_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapBuilderSaveMapSuccessAction implements BaseAction {
  type = MapBuilderActionTypes.SAVE_MAP_SUCCESS;
  admin = false;
  constructor(public payload: IPersistentMap) { }
}
export class MapBuilderDeleteMapAction implements BaseAction {
  type = MapBuilderActionTypes.DELETE_MAP;
  admin = false;
  constructor(public payload: number) { }
}
export class MapBuilderDeleteMapFailureAction implements BaseAction {
  type = MapBuilderActionTypes.DELETE_MAP_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapBuilderDeleteMapSuccessAction implements BaseAction {
  type = MapBuilderActionTypes.DELETE_MAP_SUCCESS;
  admin = false;
  constructor(public payload: IPersistentMap) { }
}

export type MapBuilderActions = MapBuilderInitAction |
  MapBuilderAddSelectedLayersAction | MapBuilderAddSelectedLayerAction |
  MapBuilderAddSelectedLayerFailureAction |
  MapBuilderEditLayerStyleAction | MapBuilderSaveLayerStyleAction |
  MapBuilderCancelEditLayerStyleAction |
  MapBuilderShowTransformOptionsAction | MapBuilderHideTransformOptionsAction |
  MapBuilderCreateGeoStatisticalLayerAction | MapBuilderCreateGeoStatisticalLayerFailureAction |
  MapBuilderCreateGeoStatisticalLayerSuccessAction |
  MapBuilderToggleMeasureControlAction | MapBuilderToggleElevationProfileControlAction |
  MapBuilderOpenSpatialAnalysisControlAction | MapBuilderCloseSpatialAnalysisControlAction |
  MapBuilderToggleMapsListAction |
  MapBuilderLoadMapAction | MapBuilderLoadMapFailureAction | MapBuilderLoadMapSuccessAction |
  MapBuilderSetEditedMapName |
  MapBuilderSaveMapAction | MapBuilderSaveMapFailureAction | MapBuilderSaveMapSuccessAction |
  MapBuilderDeleteMapAction | MapBuilderDeleteMapFailureAction | MapBuilderDeleteMapSuccessAction;
