import { Inject, Injectable } from '@angular/core';

import { ILayer } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class LayersManager extends ModelManager<ILayer> {
  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.layersEndpoint], _apiService, ['get', 'list']);
  }
}
