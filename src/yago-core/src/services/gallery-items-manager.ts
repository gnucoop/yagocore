import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { IApiListOptions, IGalleryItem, IGalleryTheme } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class GalleryItemsManager extends ModelManager<IGalleryItem> {
  private _listByThemeEndpoint: string;

  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.galleryItemsEndpoint], _apiService, ['get', 'list']);

    this._listByThemeEndpoint = `${this.endPoint}/${_config.galleryItemsByThemeSubpath}`;
  }

  listByTheme(theme: IGalleryTheme, options?: IApiListOptions): Observable<IGalleryItem[]> {
    let ep = `${this._listByThemeEndpoint}`;
    if (theme != null) {
      ep = `${ep}/${theme.id}`;
    }
    return <Observable<IGalleryItem[]>>this._apiService.list(ep, options, false);
  }
}
