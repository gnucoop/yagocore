import { RouterReducerState } from '@ngrx/router-store';

import { IAuthState } from './auth-state';
import { IAdminArcGISSourcesState } from './admin-arcgis-sources-state';
import { IAdminWMSSourcesState } from './admin-wms-sources-state';
import { IAdminLayersState } from './admin-layers-state';
import { IAdminThemesState } from './admin-themes-state';
import { IAdminGalleryThemesState } from './admin-gallery-themes-state';
import { IAdminGalleryItemsState } from './admin-gallery-items-state';
import { IAdminUsersState } from './admin-users-state';
import { IMapState } from './map-state';
import { IThemesState } from './themes-state';
import { ILayersState } from './layers-state';
import { ILayersBrowserState } from './layers-browser-state';
import { IMapsListState } from './maps-list-state';
import { IMapBuilderState } from './map-builder-state';
import { IGalleryState } from './gallery-state';


export interface State {
  auth: IAuthState;
  router: RouterReducerState;
  adminArcGISSources: IAdminArcGISSourcesState;
  adminWmsSources: IAdminWMSSourcesState;
  adminLayers: IAdminLayersState;
  adminThemes: IAdminThemesState;
  adminGalleryThemes: IAdminGalleryThemesState;
  adminGalleryItems: IAdminGalleryItemsState;
  adminUsers: IAdminUsersState;
  map: IMapState;
  themes: IThemesState;
  layers: ILayersState;
  layersBrowser: ILayersBrowserState;
  mapsList: IMapsListState;
  mapBuilder: IMapBuilderState;
  gallery: IGalleryState;
}
