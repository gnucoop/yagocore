import 'leaflet';
import 'reproject';

import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';

import { reducers } from './reducers/index';
import {
  RouterEffects,
  AuthHttp, AuthService, AuthGuard, AdminAuthGuard, AuthEffects,
  ApiService,
  ArcGISSourcesManager, ArcGISSourcesService, ArcGISSourcesEffects,
  WMSSourcesManager, WMSSourcesService, WMSSourcesEffects,
  ThemesManager, ThemesService, ThemesEffects,
  LayersManager, LayersService, LayersEffects,
  LayersBrowserService,
  MapService, MapEffects,
  MapsManager,
  MapsListService, MapsListEffects,
  MapBuilderService, MapBuilderEffects,
  GalleryThemesManager, GalleryItemsManager,
  GalleryThemesService, GalleryThemesEffects,
  GalleryItemsService, GalleryItemsEffects,
  GalleryService,
  UsersManager, UsersService, UsersEffects
} from './services/index';


@NgModule({
  imports: [
    HttpModule,
    RouterModule
  ],
  declarations: [],
  entryComponents: []
})
export class YagoCoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: YagoCoreModule,
      providers: [
        AuthHttp, AuthService, AuthGuard, AdminAuthGuard,
        ApiService,
        ArcGISSourcesManager, ArcGISSourcesService,
        WMSSourcesManager, WMSSourcesService,
        ThemesManager, ThemesService,
        LayersManager, LayersService,
        MapService,
        MapsManager,
        MapBuilderService,
        MapsListService,
        LayersBrowserService,
        GalleryThemesManager, GalleryThemesService,
        GalleryItemsManager, GalleryItemsService,
        GalleryService,
        UsersManager, UsersService
      ]
    };
  }
}

@NgModule({
  imports: [
    YagoCoreModule,
    StoreModule.forRoot(reducers),
    StoreRouterConnectingModule,
    EffectsModule.forRoot([
      RouterEffects,
      AuthEffects,
      ArcGISSourcesEffects,
      WMSSourcesEffects,
      ThemesEffects,
      LayersEffects,
      MapEffects,
      MapsListEffects,
      MapBuilderEffects,
      GalleryThemesEffects,
      GalleryItemsEffects,
      UsersEffects
    ])
  ]
})
export class YagoRootModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: YagoRootModule,
      providers: []
    };
  }
}
