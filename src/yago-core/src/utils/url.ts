export function removeSlashes(str: string) {
  return str.replace(/^\/+|\/+$/gm, '');
}

export function composeUrl(parts: string[]) {
  const url = parts.map(p => removeSlashes(p)).join('/');
  return `${url}/`;
}
