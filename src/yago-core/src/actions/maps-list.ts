import { BaseAction } from './base';
import { IPersistentMap } from '../interfaces/index';
import { type } from '../utils/index';


// tslint:disable-next-line
export const MapsListActionTypes = {
  LIST: type('[Maps List] List maps'),
  LIST_FAILURE: type('[Maps List] List maps failure'),
  LIST_SUCCESS: type('[Maps List] List maps success'),
  SELECT: type('[Maps List] Maps list select'),
  SELECT_FAILURE: type('[Maps List] Maps list select failure'),
  SELECT_SUCCESS: type('[Maps List] Maps list select success')
};

export class MapsListListAction implements BaseAction {
  type = MapsListActionTypes.LIST;
  admin = false;
  constructor(public payload = undefined) { }
}
export class MapsListListFailureAction implements BaseAction {
  type = MapsListActionTypes.LIST_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapsListListSuccessAction implements BaseAction {
  type = MapsListActionTypes.LIST_SUCCESS;
  admin = false;
  constructor(public payload: IPersistentMap[]) { }
}
export class MapsListSelectAction implements BaseAction {
  type = MapsListActionTypes.SELECT;
  admin = false;
  constructor(public payload: IPersistentMap) { }
}
export class MapsListSelectFailureAction implements BaseAction {
  type = MapsListActionTypes.SELECT_FAILURE;
  admin = false;
  constructor(public payload: any) { }
}
export class MapsListSelectSuccessAction implements BaseAction {
  type = MapsListActionTypes.SELECT_SUCCESS;
  admin = false;
  constructor(public payload?: number) { }
}

export type MapsListActions =
  MapsListListAction | MapsListListFailureAction | MapsListListSuccessAction |
  MapsListSelectAction | MapsListSelectFailureAction | MapsListSelectSuccessAction;
