import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { Store } from '@ngrx/store';

import {
  GalleryThemesListByParentIdAction, GalleryItemsListByThemeIdAction,
  GalleryThemesListByParentAction, GalleryItemsListByThemeAction,
  GalleryPreviewImageAction, GalleryPreviewImageCloseAction
} from '../actions/index';
import { IGalleryItem, IGalleryTheme, State } from '../interfaces/index';
import * as reducers from '../reducers/index';


@Injectable()
export class GalleryService {
  constructor(private _store: Store<State>) { }

  list(theme?: IGalleryTheme | number): void {
    if (typeof theme === 'number') {
      this._store.dispatch(new GalleryThemesListByParentIdAction({parent: theme}));
      this._store.dispatch(new GalleryItemsListByThemeIdAction({theme}));
    } else {
      this._store.dispatch(new GalleryThemesListByParentAction({parent: theme}));
      this._store.dispatch(new GalleryItemsListByThemeAction({theme}));
    }
  }

  previewImage(item: IGalleryItem): void {
    this._store.dispatch(new GalleryPreviewImageAction(item.attachment));
  }

  closePreviewImage(): void {
    this._store.dispatch(new GalleryPreviewImageCloseAction());
  }

  getTheme(): Observable<IGalleryTheme | null> {
    return this._store.select(reducers.getGalleryTheme);
  }

  getSubThemes(): Observable<IGalleryTheme[]> {
    return this._store.select(reducers.getGallerySubThemes);
  }

  getItems(): Observable<IGalleryItem[]> {
    return this._store.select(reducers.getGalleryItems);
  }

  getLoading(): Observable<boolean> {
    return this._store.select(reducers.getGalleryLoading);
  }

  getPreviewUrl(): Observable<string | null> {
    return this._store.select(reducers.getGalleryPreviewUrl);
  }
}
