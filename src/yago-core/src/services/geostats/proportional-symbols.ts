import { FormGroup } from '@angular/forms';

import {default as turfCentroid} from '@turf/centroid';

import { toWgs84 } from 'reproject';


import { IMapLayer, IMapLayerStyle, IMapConditionalLayerStyle } from '../../interfaces/index';
import { crsNameToEpsg, defCrs, Proj4jsProjections } from '../../utils/index';
import { GeoStatisticTransformOption } from './base';
import { IndicatorBasedTransform } from './indicator';

export class ProportionalSymbolsTransform extends IndicatorBasedTransform {
  private static _commonOptions: GeoStatisticTransformOption[] = [{
    name: 'classesNumber',
    label: 'Number of classes',
    type: 'number',
    required: true
  }, {
    name: 'classes',
    label: 'Classes',
    type: 'select',
    required: true,
    choices: [
      {label: '', value: ''},
      {label: 'Quantiles', value: 'quantiles'},
      {label: 'Equal intervals', value: 'equal_intervals'}
    ]
  }, {
    name: 'minRadius',
    label: 'Minimum radius',
    type: 'number',
    required: true
  }, {
    name: 'maxRadius',
    label: 'Maximum radius',
    type: 'number',
    required: true
  }, {
    name: 'stroke',
    label: 'Stroke',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'color',
    label: 'Stroke color',
    type: 'color',
    required: true,
    defaultValue: '#0000ff'
  }, {
    name: 'opacity',
    label: 'Stroke opacity',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }, {
    name: 'width',
    label: 'Stroke width',
    type: 'slider',
    required: true,
    defaultValue: 1,
    minValue: 0,
    maxValue: 20,
    step: 0.5
  }, {
    name: 'fill',
    label: 'Fill',
    type: 'boolean',
    required: true,
    defaultValue: true
  }, {
    name: 'fillColor',
    label: 'Fill color',
    type: 'color',
    required: true,
    defaultValue: '#ff0000'
  }, {
    name: 'fillOpacity',
    label: 'Fill opacity',
    type: 'slider',
    required: true,
    defaultValue: 0.7,
    minValue: 0,
    maxValue: 1,
    step: 0.05
  }];

  private static _name = 'Proportional symbols';
  static getName(): string { return ProportionalSymbolsTransform._name; }

  static getOptions(layer: IMapLayer): GeoStatisticTransformOption[] {
    const ind = ProportionalSymbolsTransform._getIndicators(layer, ['number']);
    const indicators = ind.indicators;
    const indicatorsValues = ind.values;

    return (<GeoStatisticTransformOption[]>[{
      name: 'indicator',
      label: 'Indicator',
      type: 'select',
      required: true,
      choices: Object.keys(indicators).map(i => {
        return {
          label: `${i} (${indicators[i]})`,
          value: [i, indicators[i], indicatorsValues[i].length]
        };
      })
    }]).concat(ProportionalSymbolsTransform._commonOptions);
  }

  static updateForm(form: FormGroup, values: {[key: string]: any}): void {
    const hasStroke = values['stroke'];
    ['color', 'opacity', 'width']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasStroke && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasStroke && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });
    const hasFill = values['fill'];
    ['startFillColor', 'endFillColor', 'fillOpacity']
      .forEach((cn) => {
        const ctrl = form.controls[cn];
        if (ctrl != null) {
          if (hasFill && ctrl.disabled) {
            ctrl.enable();
          } else if (!hasFill && ctrl.enabled) {
            ctrl.disable();
          }
        }
      });

    const indicator: any[] = values['indicator'];
    const isStringIndicator = indicator != null &&
      indicator.length === 3 && indicator[1] === 'string';
    const classesCtrl = form.controls['classes'];
    const classesNumberCtrl = form.controls['classesNumber'];
    if (isStringIndicator) {
      if (classesCtrl.enabled) { classesCtrl.disable(); }
      if (classesCtrl.value !== '') { classesCtrl.setValue(''); }

      if (classesNumberCtrl.enabled) { classesNumberCtrl.disable(); }
      if (classesNumberCtrl.value !== indicator[2]) {
        classesNumberCtrl.setValue(indicator[2]);
      }
    } else {
      if (classesNumberCtrl.disabled) { classesNumberCtrl.enable(); }
    }
  }

  static transform(layer: IMapLayer, options: {[key: string]: any}): IMapLayer {
    const newLayer: IMapLayer = crsNameToEpsg(JSON.parse(JSON.stringify(layer)));
    const indicator: any[] = options['indicator'];
    if (indicator.length !== 3) { return newLayer; }
    const indicatorName: string = indicator[0];
    const indicatorType = indicator[1];
    const classes = options['classes'];
    let classesNumber: number;
    try {
      classesNumber = parseInt(options['classesNumber'], 10);
    } catch (e) {
      classesNumber = NaN;
    }
    if (classesNumber == null || isNaN(classesNumber)) {
      return newLayer;
    }
    if (
      (indicatorType === 'number' && classes !== 'quantiles' && classes !== 'equal_intervals') ||
      (indicatorType === 'string' && (classes || '') !== '')
    ) {
      return newLayer;
    }

    const steps = indicatorType === 'string' ?
      ProportionalSymbolsTransform._getTextSteps(newLayer, indicatorName) :
      (
        classes === 'quantiles' ?
        ProportionalSymbolsTransform._getQuantilesSteps(newLayer, indicatorName, classesNumber) :
        ProportionalSymbolsTransform._getEqualIntervalsSteps(newLayer, indicatorName, classesNumber)
      );
    if (steps == null) { return layer; }
    const newStyle = ProportionalSymbolsTransform._generateLayerStyle(
      <IMapLayerStyle>newLayer.style, steps, options
    );
    if (newStyle == null) { return newLayer; }
    const features = <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>toWgs84(
      <GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>newLayer.features,
      <any>null, Proj4jsProjections
    );
    features.crs = defCrs;
    features.features = features.features.map(f => {
      const nf = turfCentroid(f);
      nf.properties = f.properties;
      nf.id = f.id;
      return nf;
    });
    newLayer.style = newStyle;
    newLayer.features = features;
    return newLayer;
  }

  private static _generateLayerStyle(
    origStyle: IMapLayerStyle, steps: string[]|[number, number][], options: {[key: string]: any}
  ): IMapConditionalLayerStyle | null {
    const indicator: any[] = options['indicator'];
    if (indicator.length !== 3) { return null; }
    const indicatorName: string = indicator[0];
    const stepsNum: number = steps.length;
    const minRadius: number = parseInt(options['minRadius'], 10);
    const maxRadius: number = parseInt(options['maxRadius'], 10);
    const stroke: boolean = options['stroke'] === 'true' || options['stroke'] === true;
    const color: string = options['color'];
    const opacity: number = parseFloat(options['opacity']);
    const width: number = parseInt(options['width'], 10);
    const fillColor: string = options['fillColor'];
    const fillOpacity: number = parseFloat(options['fillOpacity']);

    const radiusDiff = maxRadius - minRadius;
    const radiusDelta = radiusDiff > 0 ? radiusDiff / stepsNum : 0;
    const style: IMapConditionalLayerStyle = {
      prop: indicatorName, proportionalLegendSize: true, styles: []
    };
    for (let i = 0 ; i < stepsNum ; i++) {
      style.styles.push({
        range: steps[i],
        style: Object.assign({}, origStyle, {
          stroke,
          color,
          opacity,
          width,
          fill: true,
          fillColor: fillColor,
          fillOpacity,
          pointType: 'circle',
          radius: Math.round(minRadius + i * radiusDelta)
        })
      });
    }
    return style;
  }
}
