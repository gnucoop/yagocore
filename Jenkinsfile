#!groovy
node {
  stage 'Build'
    echo 'Getting code...'
    checkout scm
    gitLog = sh(returnStdout: true, script: 'git log -n 1')
    cleanBuild = gitLog.contains('CLEANBUILD')
    noDeploy = gitLog.contains('NODEPLOY')

    echo 'Installing Node packages...'
    if (cleanBuild) {
      sh 'rm -Rf node_modules typings'
    }
    sh 'npm install'
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'build', buildName: 'Build')
    try {
      echo 'Building...'
      sh 'npm run ci:build'
      bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'build', buildName: 'Build')
    } catch(Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'build', buildName: 'Build')
      throw e
    }
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'lint', buildName: 'Linter')
    try {
      echo 'Running linter...'
      sh 'npm run ci:lint'
      bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'lint', buildName: 'Linter')
  } catch(Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'lint', buildName: 'Linter')
      throw e
    }
  stage 'Test'
    bitbucketStatusNotify(buildState: 'INPROGRESS', buildKey: 'test', buildName: 'Test')
    try {
      echo 'Running tests...'
      // sh 'npm run ci:test'
      bitbucketStatusNotify(buildState: 'SUCCESSFUL', buildKey: 'test', buildName: 'Test')
    } catch(Exception e) {
      bitbucketStatusNotify(buildState: 'FAILED', buildKey: 'test', buildName: 'Test')
      throw e
    }
  stage 'Deploy'
    sh 'npm run build:release'
    if (!noDeploy) {
      echo 'Versioning...'
      sh './publish.sh'
    } else {
      echo 'Skipping deploy as per commit message'
    }
  stage 'Publish docs'
    echo 'Creating docs..'
    sh 'npm run docs'
    echo 'Publishing docs..'
    sh 'tar cfj - -C doc . | ssh docs.gnucoop.io "tar xfj - -C dewcocore/"'
}
