import { IGalleryItem, IGalleryTheme } from '../interfaces/index';


export interface IGalleryState {
  currentTheme: IGalleryTheme | null;
  currentSubThemes: IGalleryTheme[];
  currentItems: IGalleryItem[];
  currentBreadcrumbs: IGalleryTheme[];
  subThemesLoading: boolean;
  itemsLoading: boolean;
  subThemesError: any;
  itemsError: any;
  previewUrl: string | null;
}
