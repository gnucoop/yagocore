import { BaseAction } from './base';
import { type } from '../utils/index';
import { IApiListOptions, ILayer } from '../interfaces/index';


// tslint:disable-next-line
export const LayersActionTypes = {
  GET: type('[Layers] Get'),
  GET_FAILURE: type('[Layers] Get failure'),
  GET_SUCCESS: type('[Layers] Get success'),
  CREATE: type('[Layers] Create'),
  CREATE_FAILURE: type('[Layers] Create failure'),
  CREATE_SUCCESS: type('[Layers] Create success'),
  UPDATE: type('[Layers] Update'),
  UPDATE_FAILURE: type('[Layers] Update failure'),
  UPDATE_SUCCESS: type('[Layers] Update success'),
  DELETE: type('[Layers] Delete'),
  DELETE_FAILURE: type('[Layers] Delete failure'),
  DELETE_SUCCESS: type('[Layers] Delete success'),
  LIST: type('[Layers] List'),
  LIST_FAILURE: type('[Layers] Load failure'),
  LIST_SUCCESS: type('[Layers] Load success')
};

export class LayersGetAction implements BaseAction {
  type = LayersActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}

export class LayersGetFailureAction implements BaseAction {
  type = LayersActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class LayersGetSuccessAction implements BaseAction {
  type = LayersActionTypes.GET_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersListAction implements BaseAction {
  type = LayersActionTypes.LIST;
  constructor(public payload?: IApiListOptions, public admin = false) { }
}

export class LayersListFailureAction implements BaseAction {
  type = LayersActionTypes.LIST_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class LayersListSuccessAction implements BaseAction {
  type = LayersActionTypes.LIST_SUCCESS;
  constructor(public payload: ILayer[], public admin = false) { }
}

export class LayersCreateAction implements BaseAction {
  type = LayersActionTypes.CREATE;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersCreateFailureAction implements BaseAction {
  type = LayersActionTypes.CREATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class LayersCreateSuccessAction implements BaseAction {
  type = LayersActionTypes.CREATE_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersUpdateAction implements BaseAction {
  type = LayersActionTypes.UPDATE;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersUpdateFailureAction implements BaseAction {
  type = LayersActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class LayersUpdateSuccessAction implements BaseAction {
  type = LayersActionTypes.UPDATE_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersDeleteAction implements BaseAction {
  type = LayersActionTypes.DELETE;
  constructor(public payload: ILayer, public admin = false) { }
}

export class LayersDeleteFailureAction implements BaseAction {
  type = LayersActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}

export class LayersDeleteSuccessAction implements BaseAction {
  type = LayersActionTypes.DELETE_SUCCESS;
  constructor(public payload: ILayer, public admin = false) { }
}

export type LayersActions =
  LayersGetAction | LayersGetFailureAction | LayersGetSuccessAction |
  LayersListAction | LayersListFailureAction | LayersListSuccessAction |
  LayersCreateAction | LayersCreateFailureAction | LayersCreateSuccessAction |
  LayersUpdateAction | LayersUpdateFailureAction | LayersUpdateSuccessAction |
  LayersDeleteAction | LayersDeleteFailureAction | LayersDeleteSuccessAction;
