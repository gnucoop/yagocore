import { IAdminThemesState, ILayerTheme } from '../interfaces/index';
import { ThemesActionTypes, ThemesActions } from '../actions/index';


const initialState: IAdminThemesState = {
  listOptions: null,
  item: null,
  itemLoading: false,
  items: [],
  itemsLoading: false,
  error: null
};

export function adminThemesReducer(
  state = initialState, action: ThemesActions
): IAdminThemesState {
  if (action.admin) {
    switch (action.type) {

      case ThemesActionTypes.GET:
      return Object.assign({}, state, {
        itemLoading: true,
        item: null,
        error: null
      });

      case ThemesActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        itemLoading: false,
        item: null,
        error: action.payload
      });

      case ThemesActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        itemLoading: false,
        item: action.payload,
        error: null
      });

      case ThemesActionTypes.LIST:
      return Object.assign({}, state, {
        listOptions: action.payload,
        itemsLoading: true,
        items: null,
        error: null
      });

      case ThemesActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: null,
        error: action.payload
      });

      case ThemesActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: action.payload,
        error: null
      });

      case ThemesActionTypes.CREATE_SUCCESS:
      case ThemesActionTypes.UPDATE_SUCCESS:
      case ThemesActionTypes.RESET_CURRENT_THEME:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminTheme(state: IAdminThemesState): ILayerTheme | null {
  return state.item;
}
export function getAdminThemeLoading(state: IAdminThemesState): boolean {
  return state.itemLoading;
}

export function getAdminThemes(state: IAdminThemesState): ILayerTheme[] {
  return state.items;
}
export function getAdminThemesLoading(state: IAdminThemesState): boolean {
  return state.itemsLoading;
}
