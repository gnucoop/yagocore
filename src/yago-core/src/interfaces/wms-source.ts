export interface IWMSSource {
  id: number;
  name: string;
  serverUrl: string;
  workspace: string;
  username: string;
  password: string;
}

export interface IWMSSourceAvabilableLayer {
  workspace: string;
  name: string;
  layerType: 'RASTER' | 'VECTOR';
  boundingBox: GeoJSON.Polygon;
  features?: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>;
}
