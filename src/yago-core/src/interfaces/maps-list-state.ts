import { IPersistentMap } from './persistent-map';


export interface IMapsListState {
  maps: IPersistentMap[];
  loading: boolean;
  error: any;
  selected: number | null;
}
