import { IUser } from './user';

export type IAuthLoginPanel = 'login' | 'register' | 'recover_password';

export interface IAuthState {
  token: string | null;
  error: string | null;
  user: IUser | null;
  loginDialogVisible: boolean;
  loading: boolean;
  loginPanel: IAuthLoginPanel;
  recoverPasswordMailSent: boolean;
}
