import { BaseAction } from './base';
import { type } from '../utils/index';
import {
  ILayer, IMap, IMapLayer, IMapSelectedFeatures, IMapShowFeatureInfo, IMapSpatialFilter,
  MapImageFormat, MapLayerZIndex, MapBaseLayer, MapCenter, MapPanDirection, MapParams,
  MapZoomDirection, MapTableFilter
} from '../interfaces/index';


// tslint:disable-next-line
export const MapActionTypes = {
  SET_MAP: type('[Map] Set map'),
  SET_MAP_PARAMS: type('[Map] Set map params'),
  ADD_LAYER: type('[Map] Add layer'),
  ADD_LAYER_FAILURE: type('[Map] Add layer failure'),
  ADD_LAYER_SUCCESS: type('[Map] Add layer success'),
  ASSIGN_LAYER_UNIQUE_ID: type('[Map] Assign layer unique id'),
  ZOOM: type('[Map] Zoom'),
  ZOOM_LEVEL: type('[Map] Zoom level'),
  PAN: type('[Map] Pan'),
  SET_CENTER: type('[Map] Set center'),
  SET_CENTER_ZOOM: type('[Map] Set center and zoom'),
  VIEW_BACK: type('[Map] View back'),
  VIEW_FORWARD: type('[Map] View forward'),
  TOGGLE_LAYER_VISIBILITY: type('[Map] Toggle layer visibility'),
  TOGGLE_LAYER_VISIBILITY_FAILURE: type('[Map] Toggle layer visibility failure'),
  TOGGLE_LAYER_VISIBILITY_SUCCESS: type('[Map] Toggle layer visibility success'),
  SELECT_LAYER: type('[Map] Select layer'),
  SELECT_LAYER_FAILURE: type('[Map] Select layer failure'),
  SELECT_LAYER_SUCCESS: type('[Map] Select layer success'),
  UPDATE_LAYER_STYLE: type('[Map] Update layer style'),
  SELECT_FEATURES: type('[Map] Select features'),
  SELECT_FEATURES_FAILURE: type('[Map] Select features failure'),
  SELECT_FEATURES_SUCCESS: type('[Map] Select features success'),
  SAVE_IMAGE: type('[Map] Save image'),
  SAVE_IMAGE_PROGRESS: type('[Map] Save image progress'),
  SAVE_IMAGE_SUCCESS: type('[Map] Save image success'),
  PRINT: type('[Map] Print'),
  PRINT_COMPLETE: type('[Map] Print complete'),
  APPLY_FILTER: type('[Map] Apply filter'),
  APPLY_FILTER_FAILURE: type('[Map] Apply filter failure'),
  APPLY_FILTER_SUCCESS: type('[Map] Apply filter success'),
  APPLY_SPATIAL_FILTER: type('[Map] Apply spatial filter'),
  APPLY_SPATIAL_FILTER_FAILURE: type('[Map] Apply spatial filter failure'),
  APPLY_SPATIAL_FILTER_SUCCESS: type('[Map] Apply spatial filter success'),
  CREATE_FILTERED_LAYER: type('[Map] Create filtered layer'),
  CREATE_FILTERED_LAYER_FAILURE: type('[Map] Create filtered layer failure'),
  CREATE_FILTERED_LAYER_SUCCESS: type('[Map] Create filtered layer success'),
  REMOVE_LAYER: type('[Map] Remove layer'),
  REMOVE_LAYER_FAILURE: type('[Map] Remove layer failure'),
  REMOVE_LAYER_SUCCESS: type('[Map] Remove layer success'),
  TOGGLE_LEGEND_CONTROL: type('[Map] Toggle legend control'),
  TOGGLE_TABLE_CONTROL: type('[Map] Toggle table control'),
  UPDATE_LAYERS_ORDER: type('[Map] Update layers order'),
  SET_BASE_LAYER: type('[Map] Set base layer'),
  SHOW_FEATURE_INFO: type('[Map] Show feature info'),
  HIDE_FEATURE_INFO: type('[Map] Hide feature info'),
  CHANGE_LAYER_LABEL: type('[Map] Change layer label'),
  CHANGE_LAYER_LABEL_FAILURE: type('[Map] Change layer label failure'),
  CHANGE_LAYER_LABEL_SUCCESS: type('[Map] Change layer label success')
};

export class MapSetMapAction implements BaseAction {
  type = MapActionTypes.SET_MAP;
  constructor(public payload: IMap, public admin = false) { }
}
export class MapSetMapParamsAction implements BaseAction {
  type = MapActionTypes.SET_MAP_PARAMS;
  constructor(public payload: MapParams, public admin = false) { }
}
export class MapAddLayerAction implements BaseAction {
  type = MapActionTypes.ADD_LAYER;
  constructor(public payload: ILayer, public admin = false) { }
}
export class MapAddLayerFailureAction implements BaseAction {
  type = MapActionTypes.ADD_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapAddLayerSuccessAction implements BaseAction {
  type = MapActionTypes.ADD_LAYER_SUCCESS;
  constructor(public payload: IMapLayer, public admin = false) { }
}
export class MapAssignLayerUniqueIdAction implements BaseAction {
  type = MapActionTypes.ASSIGN_LAYER_UNIQUE_ID;
  constructor(public payload: IMapLayer, public admin = false) { }
}
export class MapZoomAction implements BaseAction {
  type = MapActionTypes.ZOOM;
  constructor(public payload: MapZoomDirection, public admin = false) { }
}
export class MapZoomLevelAction implements BaseAction {
  type = MapActionTypes.ZOOM_LEVEL;
  constructor(public payload: number, public admin = false) { }
}
export class MapPanAction implements BaseAction {
  type = MapActionTypes.PAN;
  constructor(public payload: MapPanDirection, public admin = false) { }
}
export class MapSetCenterAction implements BaseAction {
  type = MapActionTypes.SET_CENTER;
  constructor(public payload: MapCenter, public admin = false) { }
}
export class MapSetCenterZoomAction implements BaseAction {
  type = MapActionTypes.SET_CENTER_ZOOM;
  constructor(public payload: {center: MapCenter, zoomLevel: number}, public admin = false) { }
}
export class MapViewBackAction implements BaseAction {
  type = MapActionTypes.VIEW_BACK;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapViewForwardAction implements BaseAction {
  type = MapActionTypes.VIEW_FORWARD;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapToggleLayerVisibilityAction implements BaseAction {
  type = MapActionTypes.TOGGLE_LAYER_VISIBILITY;
  constructor(public payload: number, public admin = false) { }
}
export class MapToggleLayerVisibilityFailureAction implements BaseAction {
  type = MapActionTypes.TOGGLE_LAYER_VISIBILITY_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapToggleLayerVisibilitySuccessAction implements BaseAction {
  type = MapActionTypes.TOGGLE_LAYER_VISIBILITY_SUCCESS;
  constructor(public payload: {id: number, visibility: boolean}, public admin = false) { }
}
export class MapSelectLayerAction implements BaseAction {
  type = MapActionTypes.SELECT_LAYER;
  constructor(public payload: number, public admin = false) { }
}
export class MapSelectLayerFailureAction implements BaseAction {
  type = MapActionTypes.SELECT_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapSelectLayerSuccessAction implements BaseAction {
  type = MapActionTypes.SELECT_LAYER_SUCCESS;
  constructor(public payload: number, public admin = false) { }
}
export class MapUpdateLayerStyleAction implements BaseAction {
  type = MapActionTypes.UPDATE_LAYER_STYLE;
  constructor(public payload: IMapLayer, public admin = false) { }
}
export class MapSelectFeaturesAction implements BaseAction {
  type = MapActionTypes.SELECT_FEATURES;
  constructor(public payload: IMapSelectedFeatures[], public admin = false) { }
}
export class MapSelectFeaturesFailureAction implements BaseAction {
  type = MapActionTypes.SELECT_FEATURES_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapSelectFeaturesSuccessAction implements BaseAction {
  type = MapActionTypes.SELECT_FEATURES_SUCCESS;
  constructor(public payload: IMapSelectedFeatures[], public admin = false) { }
}
export class MapSaveImageAction implements BaseAction {
  type = MapActionTypes.SAVE_IMAGE;
  constructor(public payload: MapImageFormat, public admin = false) { }
}
export class MapSaveImageProgressAction implements BaseAction {
  type = MapActionTypes.SAVE_IMAGE_PROGRESS;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapSaveImageSuccessAction implements BaseAction {
  type = MapActionTypes.SAVE_IMAGE_SUCCESS;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapPrintAction implements BaseAction {
  type = MapActionTypes.PRINT;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapPrintCompleteAction implements BaseAction {
  type = MapActionTypes.PRINT_COMPLETE;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapApplyFilterAction implements BaseAction {
  type = MapActionTypes.APPLY_FILTER;
  constructor(public payload: {layer: IMapLayer, filter: MapTableFilter}, public admin = false) { }
}
export class MapApplyFilterFailureAction implements BaseAction {
  type = MapActionTypes.APPLY_FILTER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapApplyFilterSuccessAction implements BaseAction {
  type = MapActionTypes.APPLY_FILTER_SUCCESS;
  constructor(public payload: IMapSelectedFeatures, public admin = false) { }
}
export class MapApplySpatialFilterAction implements BaseAction {
  type = MapActionTypes.APPLY_SPATIAL_FILTER;
  constructor(public payload: IMapSpatialFilter, public admin = false) { }
}
export class MapApplySpatialFilterFailureAction implements BaseAction {
  type = MapActionTypes.APPLY_SPATIAL_FILTER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapApplySpatialFilterSuccessAction implements BaseAction {
  type = MapActionTypes.APPLY_FILTER_SUCCESS;
  constructor(public payload: IMapSelectedFeatures, public admin = false) { }
}
export class MapCreateFilteredLayerAction implements BaseAction {
  type = MapActionTypes.CREATE_FILTERED_LAYER;
  constructor(public payload: {layer: IMapLayer, filter: MapTableFilter}, public admin = false) { }
}
export class MapCreateFilteredLayerFailureAction implements BaseAction {
  type = MapActionTypes.CREATE_FILTERED_LAYER_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapCreateFilteredLayerSuccessAction implements BaseAction {
  type = MapActionTypes.CREATE_FILTERED_LAYER_SUCCESS;
  constructor(public payload: IMapLayer, public admin = false) { }
}
export class MapRemoveLayerAction implements BaseAction {
  type = MapActionTypes.REMOVE_LAYER;
  constructor(public payload: IMapLayer, public admin = false) { }
}
export class MapToggleLegendControlAction implements BaseAction {
  type = MapActionTypes.TOGGLE_LEGEND_CONTROL;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapToggleTableControlAction implements BaseAction {
  type = MapActionTypes.TOGGLE_TABLE_CONTROL;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapUpdateLayersOrderAction implements BaseAction {
  type = MapActionTypes.UPDATE_LAYERS_ORDER;
  constructor(public payload: MapLayerZIndex[], public admin = false) { }
}
export class MapSetBaseLayerAction implements BaseAction {
  type = MapActionTypes.SET_BASE_LAYER;
  constructor(public payload: MapBaseLayer, public admin = false) { }
}
export class MapShowFeatureInfoAction implements BaseAction {
  type = MapActionTypes.SHOW_FEATURE_INFO;
  constructor(public payload: IMapShowFeatureInfo, public admin = false) { }
}
export class MapHideFeatureInfoAction implements BaseAction {
  type = MapActionTypes.HIDE_FEATURE_INFO;
  payload = undefined;
  constructor(public admin = false) { }
}
export class MapChangeLayerLabelAction implements BaseAction {
  type = MapActionTypes.CHANGE_LAYER_LABEL;
  constructor(public payload: {layer: IMapLayer, label: string}, public admin = false) { }
}
export class MapChangeLayerLabelFailureAction implements BaseAction {
  type = MapActionTypes.CHANGE_LAYER_LABEL_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class MapChangeLayerLabelSuccessAction implements BaseAction {
  type = MapActionTypes.CHANGE_LAYER_LABEL_SUCCESS;
  constructor(public payload: IMapLayer, public admin = false) { }
}


export type MapActions = MapSetMapAction | MapSetMapParamsAction |
  MapAddLayerAction | MapAddLayerFailureAction | MapAddLayerSuccessAction |
  MapAssignLayerUniqueIdAction |
  MapZoomAction | MapZoomLevelAction | MapPanAction | MapSetCenterAction |
  MapSetCenterZoomAction | MapViewBackAction | MapViewForwardAction |
  MapToggleLayerVisibilityAction | MapToggleLayerVisibilityFailureAction |
  MapToggleLayerVisibilitySuccessAction |
  MapSelectLayerAction | MapSelectLayerFailureAction | MapSelectLayerSuccessAction |
  MapUpdateLayerStyleAction |
  MapSelectFeaturesAction | MapSelectFeaturesFailureAction | MapSelectFeaturesSuccessAction |
  MapSaveImageAction | MapSaveImageProgressAction | MapSaveImageSuccessAction |
  MapPrintAction | MapPrintCompleteAction |
  MapApplyFilterAction | MapApplyFilterFailureAction | MapApplyFilterSuccessAction |
  MapApplySpatialFilterAction | MapApplySpatialFilterFailureAction |
  MapApplySpatialFilterSuccessAction |
  MapCreateFilteredLayerAction | MapCreateFilteredLayerFailureAction |
  MapCreateFilteredLayerSuccessAction |
  MapRemoveLayerAction |
  MapToggleLegendControlAction | MapToggleTableControlAction |
  MapUpdateLayersOrderAction |
  MapSetBaseLayerAction |
  MapShowFeatureInfoAction | MapHideFeatureInfoAction |
  MapChangeLayerLabelAction | MapChangeLayerLabelFailureAction | MapChangeLayerLabelSuccessAction;
