import { IAdminWMSSourcesState, IWMSSource, IWMSSourceAvabilableLayer } from '../interfaces';
import { WMSSourcesActionTypes, WMSSourcesActions } from '../actions/index';


const initialState: IAdminWMSSourcesState = {
  listOptions: null,
  item: null,
  itemLoading: false,
  items: [],
  itemsLoading: false,
  layer: null,
  layerLoading: false,
  layers: [],
  layersLoading: false,
  error: null
};


export function adminWMSSourcesReducer(
  state = initialState, action: WMSSourcesActions
): IAdminWMSSourcesState {
  if (action.admin) {
    switch (action.type) {

      case WMSSourcesActionTypes.GET:
      return Object.assign({}, state, {
        itemLoading: true,
        item: null,
        error: null
      });

      case WMSSourcesActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        itemLoading: false,
        error: null,
        item: action.payload
      });

      case WMSSourcesActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        itemLoading: false,
        error: action.payload,
        item: null
      });

      case WMSSourcesActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case WMSSourcesActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case WMSSourcesActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      case WMSSourcesActionTypes.GET_LAYERS:
      return Object.assign({}, state, {
        layers: null,
        layersLoading: true,
        error: null
      });

      case WMSSourcesActionTypes.GET_LAYERS_FAILURE:
      return Object.assign({}, state, {
        layersLoading: false,
        layers: null,
        error: action.payload
      });

      case WMSSourcesActionTypes.GET_LAYERS_SUCCESS:
      return Object.assign({}, state, {
        layersLoading: false,
        layers: action.payload,
        error: null
      });

      case WMSSourcesActionTypes.GET_LAYER:
      return Object.assign({}, state, {
        layer: null,
        layerLoading: true,
        error: null
      });

      case WMSSourcesActionTypes.GET_LAYER_FAILURE:
      return Object.assign({}, state, {
        layerLoading: false,
        layer: null,
        error: action.payload
      });

      case WMSSourcesActionTypes.GET_LAYER_SUCCESS:
      return Object.assign({}, state, {
        layerLoading: false,
        layer: action.payload,
        error: null
      });

      case WMSSourcesActionTypes.CREATE_SUCCESS:
      case WMSSourcesActionTypes.UPDATE_SUCCESS:
      case WMSSourcesActionTypes.RESET_CURRENT_WMS_SOURCE:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminWMSSources(state: IAdminWMSSourcesState): IWMSSource[] {
  return state.items;
}
export function getAdminWMSSourcesLoading(state: IAdminWMSSourcesState): boolean {
  return state.itemsLoading;
}

export function getAdminWMSSourcesLayers(
  state: IAdminWMSSourcesState
): IWMSSourceAvabilableLayer[] {
  return state.layers;
}
export function getAdminWMSSourcesLayersLoading(state: IAdminWMSSourcesState): boolean {
  return state.layersLoading;
}

export function getAdminWMSSource(state: IAdminWMSSourcesState): IWMSSource | null {
  return state.item;
}
export function getAdminWMSSourceLoading(state: IAdminWMSSourcesState): boolean {
  return state.itemLoading;
}

export function getAdminWMSSourcesLayer(
  state: IAdminWMSSourcesState
): IWMSSourceAvabilableLayer | null {
  return state.layer;
}
export function getAdminWMSSourcesLayerLoading(state: IAdminWMSSourcesState): boolean {
  return state.layerLoading;
}
