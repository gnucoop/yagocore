import { BaseAction } from './base';
import { type } from '../utils/index';
import { IApiListOptions, IUser } from '../interfaces/index';


// tslint:disable-next-line
export const UsersActionTypes = {
  LIST: type('[Users] List'),
  LIST_FAILURE: type('[Users] List failure'),
  LIST_SUCCESS: type('[Users] List success'),
  GET: type('[Users] Get'),
  GET_FAILURE: type('[Users] Get failure'),
  GET_SUCCESS: type('[Users] Get success'),
  CREATE: type('[Users] Create'),
  CREATE_FAILURE: type('[Users] Create failure'),
  CREATE_SUCCESS: type('[Users] Create success'),
  UPDATE: type('[Users] Update'),
  UPDATE_FAILURE: type('[Users] Update failure'),
  UPDATE_SUCCESS: type('[Users] Update success'),
  DELETE: type('[Users] Delete'),
  DELETE_FAILURE: type('[Users] Delete failure'),
  DELETE_SUCCESS: type('[Users] Delete success'),
  RESET_CURRENT_USER: type('[Users] Reset current user')
};

export class UsersListAction implements BaseAction {
  type = UsersActionTypes.LIST;
  constructor(public payload?: IApiListOptions, public admin = false) { }
}
export class UsersListFailureAction implements BaseAction {
  type = UsersActionTypes.LIST_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class UsersListSuccessAction implements BaseAction {
  type = UsersActionTypes.LIST_SUCCESS;
  constructor(public payload: IUser[], public admin = false) { }
}
export class UsersGetAction implements BaseAction {
  type = UsersActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}
export class UsersGetFailureAction implements BaseAction {
  type = UsersActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class UsersGetSuccessAction implements BaseAction {
  type = UsersActionTypes.GET_SUCCESS;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersCreateAction implements BaseAction {
  type = UsersActionTypes.CREATE;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersCreateFailureAction implements BaseAction {
  type = UsersActionTypes.CREATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class UsersCreateSuccessAction implements BaseAction {
  type = UsersActionTypes.CREATE_SUCCESS;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersUpdateAction implements BaseAction {
  type = UsersActionTypes.UPDATE;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersUpdateFailureAction implements BaseAction {
  type = UsersActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class UsersUpdateSuccessAction implements BaseAction {
  type = UsersActionTypes.UPDATE_SUCCESS;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersDeleteAction implements BaseAction {
  type = UsersActionTypes.DELETE;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersDeleteFailureAction implements BaseAction {
  type = UsersActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class UsersDeleteSuccessAction implements BaseAction {
  type = UsersActionTypes.DELETE_SUCCESS;
  constructor(public payload: IUser, public admin = false) { }
}
export class UsersResetCurrentUserAction implements BaseAction {
  type = UsersActionTypes.RESET_CURRENT_USER;
  payload = undefined;
  constructor(public admin = false) { }
}


export type UsersActions =
  UsersGetAction | UsersGetFailureAction | UsersGetSuccessAction |
  UsersListAction | UsersListFailureAction | UsersListSuccessAction |
  UsersCreateAction | UsersCreateFailureAction | UsersCreateSuccessAction |
  UsersUpdateAction | UsersUpdateFailureAction | UsersUpdateSuccessAction |
  UsersDeleteAction | UsersDeleteFailureAction | UsersDeleteSuccessAction |
  UsersResetCurrentUserAction;
