import {
  IAdminArcGISSourcesState, IArcGISSource, IArcGISSourceAvabilableLayer
} from '../interfaces';
import { ArcGISSourcesActionTypes, ArcGISSourcesActions } from '../actions/index';


const initialState: IAdminArcGISSourcesState = {
  listOptions: null,
  item: null,
  itemLoading: false,
  items: [],
  itemsLoading: false,
  layer: null,
  layerLoading: false,
  layers: [],
  layersLoading: false,
  error: null
};


export function adminArcGISSourcesReducer(
  state = initialState, action: ArcGISSourcesActions
): IAdminArcGISSourcesState {
  if (action.admin) {
    switch (action.type) {

      case ArcGISSourcesActionTypes.GET:
      return Object.assign({}, state, {
        itemLoading: true,
        item: null,
        error: null
      });

      case ArcGISSourcesActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        itemLoading: false,
        error: null,
        item: action.payload
      });

      case ArcGISSourcesActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        itemLoading: false,
        error: action.payload,
        item: null
      });

      case ArcGISSourcesActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case ArcGISSourcesActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case ArcGISSourcesActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      case ArcGISSourcesActionTypes.GET_LAYERS:
      return Object.assign({}, state, {
        layers: null,
        layersLoading: true,
        error: null
      });

      case ArcGISSourcesActionTypes.GET_LAYERS_FAILURE:
      return Object.assign({}, state, {
        layersLoading: false,
        layers: null,
        error: action.payload
      });

      case ArcGISSourcesActionTypes.GET_LAYERS_SUCCESS:
      return Object.assign({}, state, {
        layersLoading: false,
        layers: action.payload,
        error: null
      });

      case ArcGISSourcesActionTypes.GET_LAYER:
      return Object.assign({}, state, {
        layer: null,
        layerLoading: true,
        error: null
      });

      case ArcGISSourcesActionTypes.GET_LAYER_FAILURE:
      return Object.assign({}, state, {
        layerLoading: false,
        layer: null,
        error: action.payload
      });

      case ArcGISSourcesActionTypes.GET_LAYER_SUCCESS:
      return Object.assign({}, state, {
        layerLoading: false,
        layer: action.payload,
        error: null
      });

      case ArcGISSourcesActionTypes.CREATE_SUCCESS:
      case ArcGISSourcesActionTypes.UPDATE_SUCCESS:
      case ArcGISSourcesActionTypes.RESET_CURRENT_ArcGIS_SOURCE:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminArcGISSources(state: IAdminArcGISSourcesState): IArcGISSource[] {
  return state.items;
}
export function getAdminArcGISSourcesLoading(state: IAdminArcGISSourcesState): boolean {
  return state.itemsLoading;
}

export function getAdminArcGISSourcesLayers(
  state: IAdminArcGISSourcesState
): IArcGISSourceAvabilableLayer[] {
  return state.layers;
}
export function getAdminArcGISSourcesLayersLoading(state: IAdminArcGISSourcesState): boolean {
  return state.layersLoading;
}

export function getAdminArcGISSource(state: IAdminArcGISSourcesState): IArcGISSource | null {
  return state.item;
}
export function getAdminArcGISSourceLoading(state: IAdminArcGISSourcesState): boolean {
  return state.itemLoading;
}

export function getAdminArcGISSourcesLayer(
  state: IAdminArcGISSourcesState
): IArcGISSourceAvabilableLayer | null {
  return state.layer;
}
export function getAdminArcGISSourcesLayerLoading(state: IAdminArcGISSourcesState): boolean {
  return state.layerLoading;
}
