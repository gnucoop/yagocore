import { IApiListOptions } from './api';
import { IGalleryTheme } from './gallery-theme';


export interface IAdminGalleryThemesState {
  listOptions: IApiListOptions | null;
  item: IGalleryTheme | null;
  itemLoading: boolean;
  items: IGalleryTheme[];
  itemsLoading: boolean;
  error: any;
}
