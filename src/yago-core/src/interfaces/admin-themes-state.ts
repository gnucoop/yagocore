import { IApiListOptions } from './api';
import { ILayerTheme } from './layer-theme';


export interface IAdminThemesState {
  listOptions: IApiListOptions | null;
  item: ILayerTheme | null;
  itemLoading: boolean;
  items: ILayerTheme[];
  itemsLoading: boolean;
  error: any;
}
