import { IWMSSource } from '../interfaces/index';

import { composeUrl } from './url';

export function generateWMSUrl(source: IWMSSource | string): string {
  source = typeof source === 'string' ? source : source.serverUrl;
  return composeUrl([source, 'ows']);
}
