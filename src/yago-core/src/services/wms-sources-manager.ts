import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ILayer, IWMSSource, IWMSSourceAvabilableLayer } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class WMSSourcesManager extends ModelManager<IWMSSource> {
  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.wmsSourcesEndpoint], _apiService, ['list']);
  }

  getAvailableLayers(id: number): Observable<IWMSSourceAvabilableLayer[]> {
    return this._apiService.get(
      this._config.wmsSourcesEndpoint, id, true, this._config.wmsSourcesLayersSubpath
    );
  }

  getAvailableLayer(
    id: number, workspace: string, name: string
  ): Observable<IWMSSourceAvabilableLayer> {
    return this._apiService.get(
      this._config.wmsSourcesEndpoint, id, true,
      `${this._config.wmsSourcesLayerSubpath}/${workspace}:${name}`
    );
  }

  importAvailableLayer(
    id: number, workspace: string, name: string, data: any
  ): Observable<ILayer> {
    return this._apiService.create(
      `${this._config.wmsSourcesEndpoint}/${id}`
      + `/${this._config.wmsSourcesLayerSubpath}/${workspace}:${name}`, data, true
    );
  }
}
