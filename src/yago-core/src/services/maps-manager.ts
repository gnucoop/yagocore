import { Inject, Injectable } from '@angular/core';

import { IPersistentMap } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class MapsManager extends ModelManager<IPersistentMap> {
  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.mapsEndpoint], _apiService, []);
  }
}
