import {task, src, dest} from 'gulp';
import {join} from 'path';
import {copySync} from 'fs-extra';
import {writeFileSync, mkdirpSync} from 'fs-extra';
import {Bundler} from 'scss-bundle';
import {yagoPackages, composeRelease, buildConfig, sequenceTask} from 'yago-build-tools';
import {execTask} from '../util/task_helpers';

// There are no type definitions available for these imports.
const gulpRename = require('gulp-rename');

const {packagesDir, outputDir} = buildConfig;

/** Path to the directory where all releases are created. */
const releasesDir = join(outputDir, 'releases');

/** Path to the output of the yago packages. */
const yagoOutputPaths = yagoPackages.map((p) => join(outputDir, 'packages', `yago-${p}`));

// Path to the sources of the yago package.
const yagoPaths = yagoPackages.map((p) => join(packagesDir, `yago-${p}`));
// Path to the release output of yago.
const releasePaths = yagoPackages.map((p) => join(releasesDir, `yago-${p}`));
// Matches all SCSS files in the library.
const allScssGlobs = yagoPaths.map((p) => join(p, '**/*.scss'));

yagoPackages.forEach((p) => {
  task(`yago-${p}:build-release`, () => composeRelease(`yago-${p}`));
});
/**
 * Overwrite the release task for the yago package.
 */
task('yago:build-release', ['yago:prepare-release'], sequenceTask(
  ...yagoPackages.map((p) => `yago-${p}:build-release`)
));

/**
 * Task that will build the material package. It will also copy all prebuilt themes and build
 * a bundled SCSS file for theming
 */
task('yago:prepare-release', sequenceTask(
  ...yagoPackages.map((p) => `yago-${p}:build`)
));


yagoPackages.forEach((m) => {
  task(`update-package-version:${m}`,
    execTask(join(packagesDir, '..', 'publish.sh'), ['-v', `-p ${m}`]));
});
task('update-package-version',
  execTask(join(packagesDir, '..', 'publish.sh'), ['-v']));
task('yago:build-release:copytoapp', () => {
  const destBase = join(packagesDir, '..', '..', 'yagoweb', 'node_modules', '@yago');
  copySync(join(outputDir, 'releases', 'yago-core'), join(destBase, 'core'));
  copySync(join(outputDir, 'releases', 'yago-md'), join(destBase, 'md'));
});
task('yago:build-release:toapp', sequenceTask(
  'yago:build-release', 'update-package-version', 'yago:build-release:copytoapp'
));
