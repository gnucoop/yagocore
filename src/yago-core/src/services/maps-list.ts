import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import { IPersistentMap, State } from '../interfaces/index';
import { MapsManager } from './maps-manager';
import { MapBuilderService } from './map-builder';
import * as reducers from '../reducers/index';
import {
  MapsListActions, MapsListActionTypes,
  MapsListListAction, MapsListListFailureAction, MapsListListSuccessAction,
  MapsListSelectAction, MapsListSelectFailureAction, MapsListSelectSuccessAction,
  MapBuilderActionTypes
} from '../actions/index';


@Injectable()
export class MapsListService {
  constructor(
    private _store: Store<State>
  ) {}

  getLoading(): Observable<boolean> {
    return this._store.select(reducers.getMapsListLoading);
  }

  getMaps(): Observable<IPersistentMap[]> {
    return this._store.select(reducers.getMapsListMaps);
  }

  getSelectedMapIdx(): Observable<number | null> {
    return this._store.select(reducers.getMapsListSelectedMapIdx);
  }

  getSelectedMap(): Observable<IPersistentMap | null> {
    return this._store.select(reducers.getMapsListSelectedMap);
  }

  listMaps(): void {
    this._store.dispatch(new MapsListListAction());
  }

  selectMap(map: IPersistentMap): void {
    this._store.dispatch(new MapsListSelectAction(map));
  }
}


@Injectable()
export class MapsListEffects {
  @Effect()
  listMaps$: Observable<MapsListActions> = this._actions
    .ofType(MapsListActionTypes.LIST)
    .switchMap(() => this._manager.list()
      .map((maps: IPersistentMap[]) => new MapsListListSuccessAction(maps))
      .catch((err: any) => Observable.of(new MapsListListFailureAction(err)))
    );

  @Effect()
  select$: Observable<MapsListActions> = this._actions
    .ofType(MapsListActionTypes.SELECT)
    .map((action: MapsListSelectAction) => action.payload)
    .withLatestFrom(
      this._service.getMaps().filter(m => m != null && m.length > 0),
      this._service.getSelectedMapIdx()
    )
    .map((r: [IPersistentMap, IPersistentMap[], number]) => {
      const map: IPersistentMap = r[0];
      const maps: IPersistentMap[] = r[1] || [];
      const selected: number = r[2];
      const idx: number = maps.findIndex(m => m.id === map.id);
      if (idx != null) {
        return new MapsListSelectSuccessAction(idx === selected ? undefined : idx);
      }
      return new MapsListSelectFailureAction({});
    });

  @Effect()
  toggleMapsList$: Observable<MapsListActions> = this._actions
    .ofType(MapBuilderActionTypes.TOGGLE_MAPS_LIST)
    .withLatestFrom(this._mapBuilderService.getMapsListVisible())
    .map(() => new MapsListSelectSuccessAction());

  @Effect()
  mapDeleted$: Observable<MapsListActions> = this._actions
    .ofType(MapBuilderActionTypes.DELETE_MAP_SUCCESS)
    .map(() => new MapsListListAction());

  constructor(
    private _service: MapsListService,
    private _mapBuilderService: MapBuilderService,
    private _actions: Actions,
    private _manager: MapsManager
  ) { }
}
