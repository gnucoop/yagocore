import { IGalleryTheme } from './gallery-theme';


export type IGalleryItemType = 'image' | 'document' | 'archive';

export interface IGalleryItem {
  id: number;
  itemType: IGalleryItemType;
  theme: IGalleryTheme;
  name: string;
  description: string;
  attachment: string;
}
