import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { ILayer, IArcGISSource, IArcGISSourceAvabilableLayer, State } from '../interfaces/index';
import {
  ArcGISSourcesActions, ArcGISSourcesActionTypes,
  ArcGISSourcesGetAction, ArcGISSourcesGetSuccessAction,
  ArcGISSourcesGetFailureAction,
  ArcGISSourcesListAction, ArcGISSourcesListSuccessAction,
  ArcGISSourcesListFailureAction,
  ArcGISSourcesCreateAction, ArcGISSourcesCreateSuccessAction,
  ArcGISSourcesCreateFailureAction,
  ArcGISSourcesUpdateAction, ArcGISSourcesUpdateSuccessAction,
  ArcGISSourcesUpdateFailureAction,
  ArcGISSourcesGetLayersAction, ArcGISSourcesGetLayersSuccessAction,
  ArcGISSourcesGetLayersFailureAction,
  ArcGISSourcesGetLayerAction, ArcGISSourcesGetLayerSuccessAction,
  ArcGISSourcesGetLayerFailureAction,
  ArcGISSourcesImportLayerAction, ArcGISSourcesImportLayerSuccessAction,
  ArcGISSourcesImportLayerFailureAction,
  ArcGISSourcesDeleteAction, ArcGISSourcesDeleteFailureAction,
  ArcGISSourcesDeleteSuccessAction,
  ArcGISSourcesResetCurrentArcGISSource,
  Back, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ModelService } from './model';
import { ArcGISSourcesManager } from './arcgis-sources-manager';


@Injectable()
export class ArcGISSourcesService extends ModelService<
    IArcGISSource, ArcGISSourcesGetAction, ArcGISSourcesListAction,
    ArcGISSourcesCreateAction, ArcGISSourcesUpdateAction, ArcGISSourcesDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, ArcGISSourcesGetAction, ArcGISSourcesListAction,
      ArcGISSourcesCreateAction, ArcGISSourcesUpdateAction, ArcGISSourcesDeleteAction
    );
  }

  getAdminArcGISSource(): Observable<IArcGISSource | null> {
    return this._store.select(reducers.getAdminArcGISSource);
  }

  getAdminArcGISSourceLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminArcGISSourceLoading);
  }

  availableLayers(id: number, admin = false): void {
    this._store.dispatch(new ArcGISSourcesGetLayersAction(id, admin));
  }

  availableLayer(id: number, basePath: string, lid: number, admin = false): void {
    this._store.dispatch(new ArcGISSourcesGetLayerAction({id, lid, basePath}, admin));
  }

  importAvailableLayer(
    id: number, basePath: string, lid: number, data: any, admin = false
  ): void {
    this._store.dispatch(new ArcGISSourcesImportLayerAction({id, lid, basePath, data}, admin));
  }

  resetCurrentArcGISSource(admin = false): void {
    this._store.dispatch(new ArcGISSourcesResetCurrentArcGISSource(admin));
  }
}


@Injectable()
export class ArcGISSourcesEffects {
  @Effect()
  get$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.GET)
    .switchMap((action: ArcGISSourcesGetAction) =>
      this._manager.get(action.payload)
        .map((source: IArcGISSource) => new ArcGISSourcesGetSuccessAction(source, action.admin))
        .catch((error: any) =>
          Observable.of(new ArcGISSourcesGetFailureAction(error, action.admin)))
    );

  @Effect()
  load$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.LIST)
    .switchMap((action: ArcGISSourcesListAction) =>
      this._manager.list(action.payload)
        .map((sources: IArcGISSource[]) =>
          new ArcGISSourcesListSuccessAction(sources, action.admin))
        .catch((error) => Observable.of(new ArcGISSourcesListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.CREATE)
    .switchMap((action: ArcGISSourcesCreateAction) =>
      this._manager.create(action.payload)
        .map((s: IArcGISSource) => new ArcGISSourcesCreateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new ArcGISSourcesCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.UPDATE)
    .switchMap((action: ArcGISSourcesCreateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: IArcGISSource) => new ArcGISSourcesUpdateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new ArcGISSourcesUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  getLayer$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.GET_LAYER)
    .switchMap((action: ArcGISSourcesGetLayerAction) =>
      this._manager.getAvailableLayer(
        action.payload.id, action.payload.basePath, action.payload.lid
      )
      .map((layer: IArcGISSourceAvabilableLayer) =>
        new ArcGISSourcesGetLayerSuccessAction(layer, action.admin))
      .catch((error: any) =>
        Observable.of(new ArcGISSourcesGetLayerFailureAction(error, action.admin)))
    );

  @Effect()
  getLayers$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.GET_LAYERS)
    .switchMap((action: ArcGISSourcesGetLayersAction) =>
      this._manager.getAvailableLayers(action.payload)
        .map((layers: IArcGISSourceAvabilableLayer[]) =>
          new ArcGISSourcesGetLayersSuccessAction(layers, action.admin))
        .catch((error: any) =>
          Observable.of(new ArcGISSourcesGetLayersFailureAction(error, action.admin)))
    );

  @Effect()
  importLayer$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.IMPORT_LAYER)
    .switchMap((action: ArcGISSourcesImportLayerAction) =>
      this._manager.importAvailableLayer(
        action.payload.id, action.payload.basePath, action.payload.lid, action.payload.data
      )
      .map((layer: ILayer) => new ArcGISSourcesImportLayerSuccessAction(layer, action.admin))
      .catch((error: any) =>
        Observable.of(new ArcGISSourcesImportLayerFailureAction(error, action.admin)))
    );

  @Effect()
  layerImported$: Observable<RouterActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.IMPORT_LAYER_SUCCESS)
    .map(() => new Back());

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.DELETE)
    .switchMap((action: ArcGISSourcesDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new ArcGISSourcesDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new ArcGISSourcesDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<ArcGISSourcesActions> = this._actions
    .ofType(ArcGISSourcesActionTypes.DELETE_SUCCESS)
    .map((action: ArcGISSourcesDeleteSuccessAction) =>
      new ArcGISSourcesListAction(undefined, action.admin));

  constructor(
    private _actions: Actions, private _manager: ArcGISSourcesManager
  ) { }
}
