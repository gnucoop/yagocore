import { createSelector } from 'reselect';

import * as L from 'leaflet';

import {
  IMap, IMapLayer, IMapState, IMapViewState, IMapSelectedFeatures, IMapShowFeatureInfo,
  MapBaseLayer, MapCenter, MapImageFormat, MapLayerZIndex, MapPanDirection,
  MapParams, MapZoomDirection
} from '../interfaces/index';
import { MapActions, MapActionTypes } from '../actions/index';


function panCenter(center: MapCenter, pan: MapPanDirection, params: MapParams): L.Point {
  switch (pan) {
    case MapPanDirection.Up:
    return L.point(center.lng, center.lat + params.panOffsetY);
    case MapPanDirection.Right:
    return L.point(center.lng + params.panOffsetX, center.lat);
    case MapPanDirection.Down:
    return L.point(center.lng, center.lat - params.panOffsetY);
    case MapPanDirection.Left:
    return L.point(center.lng - params.panOffsetX, center.lat);
    default:
    return L.point(center.lng, center.lat);
  }
}

const initialState: IMapState = {
  item: null,
  layers: [],
  viewState: [{
    zoomLevel: 0,
    mapParams: {
      panOffsetX: 0,
      panOffsetY: 0
    },
    center: {
      lat: 0,
      lng: 0
    },
  }],
  currentViewState: 0,
  selectedLayer: null,
  loading: false,
  error: null,
  selectedFeatures: [],
  filteredFeatures: [],
  saveImageRequest: null,
  saveImageLoading: false,
  print: false,
  legendControlOpen: false,
  tableControlOpen: false,
  baseLayer: 'osm',
  showFeatureInfo: null
};


export function mapReducer(state = initialState, action: MapActions): IMapState {
  switch (action.type) {

    case MapActionTypes.SET_MAP:
    return Object.assign({}, state, {
      item: action.payload,
      layers: null,
      selectedFeatures: [],
      filteredFeatures: []
    });

    case MapActionTypes.SET_MAP_PARAMS:
    const mapParamsViewState = state.viewState[state.currentViewState];
    const newViewStates = state.viewState.slice(0);
    newViewStates[state.currentViewState] = Object.assign({}, mapParamsViewState, {
      mapParams: action.payload
    });
    return Object.assign({}, state, {
      viewState: newViewStates
    });

    case MapActionTypes.ADD_LAYER:
    return Object.assign({}, state, {
      loading: true,
      error: null
    });

    case MapActionTypes.ADD_LAYER_SUCCESS:
    return Object.assign({}, state, {
      loading: false,
      error: null,
      layers: [...(state.layers || []), action.payload]
    });

    case MapActionTypes.ADD_LAYER_FAILURE:
    return Object.assign({}, state, {
      loading: false,
      error: action.payload
    });

    case MapActionTypes.ZOOM:
    case MapActionTypes.ZOOM_LEVEL:
    const zoomViewState = state.viewState[state.currentViewState];
    let newZoom: number = -1;
    if (action.type === MapActionTypes.ZOOM) {
      if (action.payload === MapZoomDirection.Out && zoomViewState.zoomLevel > 0) {
        newZoom = zoomViewState.zoomLevel - 1;
      } else if (action.payload === MapZoomDirection.In && zoomViewState.zoomLevel < 19) {
        newZoom = zoomViewState.zoomLevel + 1;
      }
    } else if (action.type === MapActionTypes.ZOOM_LEVEL) {
      newZoom = action.payload;
    }
    if (newZoom > -1 && newZoom !== zoomViewState.zoomLevel) {
      return Object.assign({}, state, {
        viewState: state.viewState.slice(0, state.currentViewState + 1).concat([
          Object.assign({}, zoomViewState, {
            zoomLevel: newZoom
          })
        ]),
        currentViewState: state.currentViewState + 1
      });
    }
    return state;

    case MapActionTypes.PAN:
    case MapActionTypes.SET_CENTER:
    const panViewState = state.viewState[state.currentViewState];
    const oldCenter = panViewState.center;
    let newCenter: any = {lat: null, lng: null};
    if (action.type === MapActionTypes.PAN) {
      const p = <any>panCenter(oldCenter, action.payload, panViewState.mapParams);
      newCenter = {lat: p.y, lng: p.x};
    } else if (action.type === MapActionTypes.SET_CENTER) {
      newCenter = action.payload;
    }
    if (
      newCenter.lat != null && newCenter.lng &&
      (newCenter.lat !== oldCenter.lat || newCenter.lng !== oldCenter.lng)
    ) {
      return Object.assign({}, state, {
        viewState: state.viewState.slice(0, state.currentViewState + 1).concat([
          Object.assign({}, panViewState, {
            center: newCenter
          })
        ]),
        currentViewState: state.currentViewState + 1
      });
    }
    return state;


    case MapActionTypes.SET_CENTER_ZOOM:
    const centerZoomViewState = state.viewState[state.currentViewState];
    const newCZCenter: MapCenter = action.payload.center;
    const newCZZoom: number = action.payload.zoomLevel;
    return Object.assign({}, state, {
      viewState: state.viewState.slice(0, state.currentViewState + 1).concat([
        Object.assign({}, centerZoomViewState, {
          center: newCZCenter,
          zoomLevel: newCZZoom
        })
      ]),
      currentViewState: state.currentViewState + 1
    });

    case MapActionTypes.VIEW_BACK:
    if (state.currentViewState > 0) {
      return Object.assign({}, state, {
        currentViewState: state.currentViewState - 1
      });
    }
    return state;

    case MapActionTypes.VIEW_FORWARD:
    if (state.currentViewState < state.viewState.length - 1) {
      return Object.assign({}, state, {
        currentViewState: state.currentViewState + 1
      });
    }
    return state;

    case MapActionTypes.TOGGLE_LAYER_VISIBILITY_SUCCESS:
    const vLayers = state.layers.slice(0);
    const vLayerId = vLayers.findIndex(l => l.uniqueId === action.payload.id);
    const vLayer = vLayers[vLayerId];
    vLayers.splice(vLayerId, 1, Object.assign({}, vLayer, {
      visible: action.payload.visibility
    }));
    return Object.assign({}, state, {layers: vLayers});

    case MapActionTypes.UPDATE_LAYER_STYLE:
    const sLayers = state.layers.slice(0);
    const sLayerId = sLayers.findIndex(l => l.uniqueId === action.payload.uniqueId);
    const sLayer = sLayers[sLayerId];
    sLayers.splice(sLayerId, 1, Object.assign({}, sLayer, {
      style: action.payload.style
    }));
    return Object.assign({}, state, {layers: sLayers});

    case MapActionTypes.SELECT_LAYER_SUCCESS:
    return Object.assign({}, state, {
      selectedLayer: action.payload,
      filteredFeatures: []
    });

    case MapActionTypes.SELECT_FEATURES_SUCCESS:
    return Object.assign({}, state, {selectedFeatures: action.payload});

    case MapActionTypes.SAVE_IMAGE:
    return Object.assign({}, state, {saveImageRequest: action.payload, saveImageLoading: false});

    case MapActionTypes.SAVE_IMAGE_PROGRESS:
    return Object.assign({}, state, {saveImageRequest: null, saveImageLoading: true});

    case MapActionTypes.SAVE_IMAGE_SUCCESS:
    return Object.assign({}, state, {saveImageLoading: false});

    case MapActionTypes.PRINT:
    return Object.assign({}, state, {print: true});

    case MapActionTypes.PRINT_COMPLETE:
    return Object.assign({}, state, {print: false});

    case MapActionTypes.APPLY_FILTER_SUCCESS:
    return Object.assign({}, state, {
      filteredFeatures: state.filteredFeatures
        .filter((f) => f.layer !== action.payload.layer)
        .concat([action.payload])
    });

    case MapActionTypes.REMOVE_LAYER:
    return Object.assign({}, state, {
      layers: state.layers.filter(l => l.uniqueId !== action.payload.uniqueId),
      selectedLayer: state.selectedLayer === action.payload.uniqueId ? null : state.selectedLayer,
      selectedFeatures: state.selectedFeatures.filter(s => s.layer !== action.payload.uniqueId),
      filteredFeatures: state.selectedFeatures.filter(s => s.layer !== action.payload.uniqueId)
    });

    case MapActionTypes.TOGGLE_LEGEND_CONTROL:
    return Object.assign({}, state, {
      legendControlOpen: !state.legendControlOpen
    });

    case MapActionTypes.TOGGLE_TABLE_CONTROL:
    return Object.assign({}, state, {
      tableControlOpen: !state.tableControlOpen
    });

    case MapActionTypes.UPDATE_LAYERS_ORDER:
    const oLayers = state.layers.slice(0);
    action.payload.forEach((u: MapLayerZIndex) => {
      const oLayerIdx = oLayers.findIndex(l => l.uniqueId === u.layer);
      if (oLayerIdx > -1) {
        oLayers[oLayerIdx] = Object.assign({}, oLayers[oLayerIdx], {
          zIndex: u.zIndex
        });
      }
    });
    return Object.assign({}, state, {
      layers: oLayers
    });

    case MapActionTypes.SET_BASE_LAYER:
    return Object.assign({}, state, {
      baseLayer: action.payload
    });

    case MapActionTypes.SHOW_FEATURE_INFO:
    return Object.assign({}, state, {
      showFeatureInfo: action.payload
    });

    case MapActionTypes.HIDE_FEATURE_INFO:
    return Object.assign({}, state, {
      showFeatureInfo: null
    });

    case MapActionTypes.CHANGE_LAYER_LABEL_FAILURE:
    return Object.assign({}, state, {
      error: action.payload
    });

    case MapActionTypes.CHANGE_LAYER_LABEL_SUCCESS:
    const newLayer = action.payload;
    return Object.assign({}, state, {
      layers: state.layers
        .filter(l => l.uniqueId !== newLayer.uniqueId)
        .concat([newLayer]),
      error: null
    });

    default:
    return state;

  }
}

export function getMap(state: IMapState): IMap | null {
  return state.item;
}
export function getLayers(state: IMapState): IMapLayer[] {
  return state.layers;
}
export function getCurrentViewState(state: IMapState): IMapViewState {
  return state.viewState[state.currentViewState];
}
function getCurrentZoomLevel(state: IMapViewState): number {
  return state != null ? state.zoomLevel : 0;
}
function getCurrentCanZoomIn(state: IMapViewState): boolean {
  return state != null ? (state.zoomLevel || 0) < 19 : false;
}
function getCurrentCanZoomOut(state: IMapViewState): boolean {
  return state != null ? (state.zoomLevel || 0) > 0 : false;
}
function getCurrentMapCenter(state: IMapViewState): MapCenter {
  return state.center;
}
export const getZoomLevel = createSelector<IMapState, number, IMapViewState>(
  getCurrentViewState, getCurrentZoomLevel
);
export const getCanZoomIn = createSelector<IMapState, boolean, IMapViewState>(
  getCurrentViewState, getCurrentCanZoomIn
);
export const getCanZoomOut = createSelector<IMapState, boolean, IMapViewState>(
  getCurrentViewState, getCurrentCanZoomOut
);
export const getMapCenter = createSelector<IMapState, MapCenter, IMapViewState>(
  getCurrentViewState, getCurrentMapCenter
);
export function getCanViewGoBack(state: IMapState): boolean {
  return state != null ? (state.currentViewState || 0) > 0 : false;
}
export function getCanViewGoForward(state: IMapState): boolean {
  return state != null ?
    (state.currentViewState || 0) < (state.viewState || []).length - 1 :
    false;
}
export function getSelectedLayerId(state: IMapState): number | null {
  return state.selectedLayer;
}
export const getSelectedLayer =
  createSelector<IMapState, IMapLayer | null, number | null, IMapLayer[]>(
    getSelectedLayerId, getLayers, (selectedLayer: number | null, layers: IMapLayer[]) => {
      return (layers || []).find(l => l.uniqueId === selectedLayer) || null;
    }
  );
export function getSelectedFeatures(state: IMapState): IMapSelectedFeatures[] {
  return state.selectedFeatures;
}
export function getMapSaveImageRequest(state: IMapState): MapImageFormat | null {
  return state.saveImageRequest;
}
export function getMapPrint(state: IMapState): boolean {
  return state.print;
}
export function getFilteredFeatures(state: IMapState): IMapSelectedFeatures[] {
  return state.filteredFeatures;
}
export function getMapTableControlOpen(state: IMapState): boolean {
  return state.tableControlOpen;
}
export function getMapLegendControlOpen(state: IMapState): boolean {
  return state.legendControlOpen;
}
export function getMapBaseLayer(state: IMapState): MapBaseLayer {
  return state.baseLayer;
}
export function getMapShowFeatureInfo(state: IMapState): IMapShowFeatureInfo | null {
  return state.showFeatureInfo;
}
