import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import { IApiListOptions, IGalleryItem, IGalleryTheme, State } from '../interfaces/index';
import {
  GalleryItemsActions, GalleryItemsActionTypes,
  GalleryItemsCreateAction, GalleryItemsCreateFailureAction, GalleryItemsCreateSuccessAction,
  GalleryItemsGetAction, GalleryItemsGetFailureAction, GalleryItemsGetSuccessAction,
  GalleryItemsListAction, GalleryItemsListFailureAction, GalleryItemsListSuccessAction,
  GalleryItemsListByThemeIdAction,
  GalleryItemsListByThemeAction, GalleryItemsListByThemeFailureAction,
  GalleryItemsListByThemeSuccessAction,
  GalleryItemsUpdateAction, GalleryItemsUpdateFailureAction, GalleryItemsUpdateSuccessAction,
  GalleryItemsDeleteAction, GalleryItemsDeleteFailureAction, GalleryItemsDeleteSuccessAction,
  GalleryItemsResetCurrentItemAction,
  Back, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ModelService } from './model';
import { GalleryThemesManager } from './gallery-themes-manager';
import { GalleryItemsManager } from './gallery-items-manager';


@Injectable()
export class GalleryItemsService extends ModelService<
    IGalleryItem, GalleryItemsGetAction, GalleryItemsListAction,
    GalleryItemsCreateAction, GalleryItemsUpdateAction, GalleryItemsDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, GalleryItemsGetAction, GalleryItemsListAction,
      GalleryItemsCreateAction, GalleryItemsUpdateAction, GalleryItemsDeleteAction
    );
  }

  getAdminItems(): Observable<IGalleryItem[]> {
    return this._store.select(reducers.getAdminGalleryItems);
  }

  getAdminItem(): Observable<IGalleryItem | null> {
    return this._store.select(reducers.getAdminGalleryItem);
  }

  getAdminItemsLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminGalleryItemsLoading);
  }

  getAdminItemLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminGalleryItemLoading);
  }

  listByTheme(theme?: IGalleryItem, options?: IApiListOptions): void {
    this._store.dispatch(new GalleryItemsListByThemeAction({theme, options}));
  }

  resetCurrentItem(admin = false): void {
    this._store.dispatch(new GalleryItemsResetCurrentItemAction(admin));
  }
}

@Injectable()
export class GalleryItemsEffects {
  @Effect()
  listByParentId$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.LIST_BY_THEME_ID)
    .switchMap((action: GalleryItemsListByThemeIdAction) =>
      this._themesManager.get(action.payload.theme)
        .map((theme: IGalleryTheme) =>
          new GalleryItemsListByThemeAction(
            {theme, options: action.payload.options}, action.admin
          ))
        .catch((err: any) => Observable.of(new GalleryItemsListByThemeFailureAction(err)))
    );

  @Effect()
  listByParent$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.LIST_BY_THEME)
    .switchMap((action: GalleryItemsListByThemeAction) =>
      this._manager.listByTheme(<IGalleryTheme>action.payload.theme, action.payload.options)
        .map((themes: IGalleryItem[]) =>
          new GalleryItemsListByThemeSuccessAction(themes, action.admin))
        .catch((err: any) => Observable.of(new GalleryItemsListByThemeFailureAction(err)))
    );

  @Effect()
  get$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.GET)
    .switchMap((action: GalleryItemsGetAction) =>
      this._manager.get(action.payload)
        .map((theme: IGalleryItem) =>
          new GalleryItemsGetSuccessAction(theme, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryItemsGetFailureAction(error, action.admin)))
    );

  @Effect()
  list$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.LIST)
    .switchMap((action: GalleryItemsListAction) =>
      this._manager.list(action.payload)
        .map((themes: IGalleryItem[]) => new GalleryItemsListSuccessAction(themes, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryItemsListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.CREATE)
    .switchMap((action: GalleryItemsCreateAction) =>
      this._manager.create(action.payload)
        .map((s: IGalleryItem) => new GalleryItemsCreateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryItemsCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.UPDATE)
    .switchMap((action: GalleryItemsUpdateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: IGalleryItem) => new GalleryItemsUpdateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryItemsUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(GalleryItemsActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(GalleryItemsActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.DELETE)
    .switchMap((action: GalleryItemsDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new GalleryItemsDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new GalleryItemsDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<GalleryItemsActions> = this._actions
    .ofType(GalleryItemsActionTypes.DELETE_SUCCESS)
    .map((action: GalleryItemsDeleteSuccessAction) =>
      new GalleryItemsListAction(undefined, action.admin));

  constructor(
    private _actions: Actions,
    private _service: GalleryItemsService,
    private _manager: GalleryItemsManager,
    private _themesManager: GalleryThemesManager
  ) { }
}
