import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Actions, Effect } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { ILayerTheme, State } from '../interfaces/index';
import {
  ThemesActions, ThemesActionTypes,
  ThemesGetAction, ThemesGetFailureAction, ThemesGetSuccessAction,
  ThemesListAction, ThemesListFailureAction, ThemesListSuccessAction,
  ThemesCreateAction, ThemesCreateFailureAction, ThemesCreateSuccessAction,
  ThemesUpdateAction, ThemesUpdateFailureAction, ThemesUpdateSuccessAction,
  ThemesDeleteAction, ThemesDeleteFailureAction, ThemesDeleteSuccessAction,
  ThemesResetCurrentThemeAction,
  Back, RouterActions
} from '../actions/index';
import { ModelService } from './model';
import { ThemesManager } from './themes-manager';


@Injectable()
export class ThemesService extends ModelService<
    ILayerTheme, ThemesGetAction, ThemesListAction, ThemesCreateAction, ThemesUpdateAction,
    ThemesDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, ThemesGetAction, ThemesListAction, ThemesCreateAction, ThemesUpdateAction,
      ThemesDeleteAction
    );
  }

  resetCurrentTheme(admin = false): void {
    this._store.dispatch(new ThemesResetCurrentThemeAction(admin));
  }
}


@Injectable()
export class ThemesEffects {
  @Effect()
  get$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.GET)
    .switchMap((action: ThemesGetAction) =>
      this._manager.get(action.payload)
        .map((theme: ILayerTheme) => new ThemesGetSuccessAction(theme, action.admin))
        .catch((error: any) => Observable.of(new ThemesGetFailureAction(error, action.admin)))
    );

  @Effect()
  list$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.LIST)
    .switchMap((action: ThemesListAction) =>
      this._manager.list(action.payload)
        .map((themes: ILayerTheme[]) => new ThemesListSuccessAction(themes, action.admin))
        .catch((error: any) => Observable.of(new ThemesListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.CREATE)
    .switchMap((action: ThemesCreateAction) =>
      this._manager.create(action.payload)
        .map((s: ILayerTheme) => new ThemesCreateSuccessAction(s, action.admin))
        .catch((error: any) => Observable.of(new ThemesCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.UPDATE)
    .switchMap((action: ThemesUpdateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: ILayerTheme) => new ThemesUpdateSuccessAction(s, action.admin))
        .catch((error: any) => Observable.of(new ThemesUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(ThemesActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(ThemesActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.DELETE)
    .switchMap((action: ThemesDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new ThemesDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new ThemesDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<ThemesActions> = this._actions
    .ofType(ThemesActionTypes.DELETE_SUCCESS)
    .map((action: ThemesDeleteSuccessAction) => new ThemesListAction(undefined, action.admin));

  constructor(
    private _actions: Actions, private _manager: ThemesManager
  ) { }
}
