import { Routes } from '@angular/router';
import { HomeComponent } from './e2e-app';


export const E2E_APP_ROUTES: Routes = [
  {path: '', component: HomeComponent}
];
