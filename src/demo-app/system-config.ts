/** Type declaration for ambient System. */
declare var System: any;

// Apply the CLI SystemJS configuration.
System.config({
  paths: {
    'node:*': 'node_modules/*',
  },
  map: {
    'rxjs': 'node:rxjs',
    'main': 'main.js',

    // Angular specific mappings.
    '@angular/animations': 'node:@angular/animations/bundles/animations.umd.js',
    '@angular/animations/browser': 'node:@angular/animations/bundles/animations-browser.umd.js',
    '@angular/core': 'node:@angular/core/bundles/core.umd.js',
    '@angular/common': 'node:@angular/common/bundles/common.umd.js',
    '@angular/compiler': 'node:@angular/compiler/bundles/compiler.umd.js',
    '@angular/http': 'node:@angular/http/bundles/http.umd.js',
    '@angular/forms': 'node:@angular/forms/bundles/forms.umd.js',
    '@angular/router': 'node:@angular/router/bundles/router.umd.js',
    '@angular/platform-browser': 'node:@angular/platform-browser/bundles/platform-browser.umd.js',
    '@angular/platform-browser/animations':
      'node:@angular/platform-browser/bundles/platform-browser-animations.umd.js',
    '@angular/platform-browser-dynamic':
      'node:@angular/platform-browser-dynamic/bundles/platform-browser-dynamic.umd.js',
    '@angular/cdk': 'node:@angular/cdk/bundles/cdk.umd.js',
    '@angular/cdk/a11y': 'node:@angular/cdk/bundles/cdk-a11y.umd.js',
    '@angular/cdk/bidi': 'node:@angular/cdk/bundles/cdk-bidi.umd.js',
    '@angular/cdk/coercion': 'node:@angular/cdk/bundles/cdk-coercion.umd.js',
    '@angular/cdk/collections': 'node:@angular/cdk/bundles/cdk-collections.umd.js',
    '@angular/cdk/keycodes': 'node:@angular/cdk/bundles/cdk-keycodes.umd.js',
    '@angular/cdk/overlay': 'node:@angular/cdk/bundles/cdk-overlay.umd.js',
    '@angular/cdk/platform': 'node:@angular/cdk/bundles/cdk-platform.umd.js',
    '@angular/cdk/portal': 'node:@angular/cdk/bundles/cdk-portal.umd.js',
    '@angular/cdk/rxjs': 'node:@angular/cdk/bundles/cdk-rxjs.umd.js',
    '@angular/cdk/scrolling': 'node:@angular/cdk/bundles/cdk-scrolling.umd.js',
    '@angular/material': 'node:@angular/material/bundles/material.umd.js',
    '@angular/material/core': 'node:@angular/material/bundles/material-core.umd.js',
    '@angular/material/button': 'node:@angular/material/bundles/material-button.umd.js',
    '@angular/material/icon': 'node:@angular/material/bundles/material-icon.umd.js',
    '@angular/material/list': 'node:@angular/material/bundles/material-list.umd.js',
    '@angular/material/sidenav': 'node:@angular/material/bundles/material-sidenav.umd.js',
    '@angular/material/toolbar': 'node:@angular/material/bundles/material-toolbar.umd.js',

    'traceur': 'node:traceur/bin/traceur.js',
    'color': 'node:color/index.js',
    'color-name': 'node:color-name/index.js',
    'color-string': 'node:color-string/index.js',
    'color-convert': 'node:color-convert/index.js',
    'simple-swizzle': 'node:simple-swizzle/index.js',
    'is-arrayish': 'node:is-arrayish/index.js',

    '@ngrx/core': 'node:@ngrx/core/bundles/core.umd.js',
    '@ngrx/effects': 'node:@ngrx/effects/bundles/effects.umd.js',
    '@ngrx/router-store': 'node:@ngrx/router-store/bundles/router-store.umd.js',
    '@ngrx/store': 'node:@ngrx/store/bundles/store.umd.js',
    'reselect': 'node:reselect/dist/reselect.js',

    '@turf/bbox': 'node:@turf/bbox/index.js',
    '@turf/boolean-point-in-polygon': 'node:@turf/boolean-point-in-polygon/index.js',
    '@turf/buffer': 'node:@turf/buffer/index.js',
    '@turf/center': 'node:@turf/center/index.js',
    '@turf/centroid': 'node:@turf/centroid/index.js',
    '@turf/clone': 'node:@turf/clone/index.js',
    '@turf/distance': 'node:@turf/distance/index.js',
    '@turf/helpers': 'node:@turf/helpers/index.js',
    '@turf/invariant': 'node:@turf/invariant/index.js',
    '@turf/meta': 'node:@turf/meta/index.js',
    '@turf/projection': 'node:@turf/projection/index.js',
    'leaflet': 'node:leaflet/dist/leaflet.js',
    'd3-array': 'node:d3-array/build/d3-array.js',
    'd3-geo': 'node:d3-geo/build/d3-geo.js',
    'proj4': 'node:proj4/dist/proj4.js',
    'reproject': 'node:reproject/index.js',

    'jsts-es': 'node:jsts-es/index.js',
    'tslib': 'node:tslib/tslib.js',

    'pako': 'node:pako/dist/pako.js',

    '@yago/core': 'dist/packages/yago-core/public_api.js'
  },
  packages: {
    // Thirdparty barrels.
    'rxjs': { main: 'index' },
    // Set the default extension for the root package, because otherwise the demo-app can't
    // be built within the production mode. Due to missing file extensions.
    '.': {
      defaultExtension: 'js'
    }
  }
});
