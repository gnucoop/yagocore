import { IGalleryState, IGalleryItem, IGalleryTheme } from '../interfaces/index';
import {
  GalleryItemsActionTypes, GalleryItemsActions,
  GalleryThemesActionTypes, GalleryThemesActions,
  GalleryActionTypes, GalleryActions
} from '../actions/index';


const initialState: IGalleryState = {
  currentTheme: null,
  currentSubThemes: [],
  currentItems: [],
  currentBreadcrumbs: [],
  subThemesLoading: false,
  itemsLoading: false,
  subThemesError: null,
  itemsError: null,
  previewUrl: null
};

export function galleryReducer(
  state = initialState,
  action: GalleryItemsActions | GalleryThemesActions | GalleryActions
) {
  switch (action.type) {

    case GalleryThemesActionTypes.LIST_BY_PARENT:
    return Object.assign({}, state, {
      currentTheme: action.payload.parent,
      currentSubThemes: null,
      subThemesLoading: true,
      subThemesError: null
    });

    case GalleryThemesActionTypes.LIST_BY_PARENT_SUCCESS:
    return Object.assign({}, state, {
      currentSubThemes: action.payload,
      subThemesLoading: false,
      subThemesError: null
    });

    case GalleryThemesActionTypes.LIST_BY_PARENT_FAILURE:
    return Object.assign({}, state, {
      currentSubThemes: null,
      subThemesLoading: false,
      subThemesError: action.payload
    });

    case GalleryItemsActionTypes.LIST_BY_THEME:
    return Object.assign({}, state, {
      currentTheme: action.payload.theme,
      currentItems: null,
      itemsLoading: true,
      itemsError: null
    });

    case GalleryItemsActionTypes.LIST_BY_THEME_SUCCESS:
    return Object.assign({}, state, {
      currentItems: action.payload,
      itemsLoading: false,
      itemsError: null
    });

    case GalleryItemsActionTypes.LIST_BY_THEME_FAILURE:
    return Object.assign({}, state, {
      currentItems: null,
      itemsLoading: false,
      itemsError: action.payload
    });

    case GalleryActionTypes.PREVIEW_IMAGE:
    return Object.assign({}, state, {
      previewUrl: action.payload
    });

    case GalleryActionTypes.PREVIEW_IMAGE_CLOSE:
    return Object.assign({}, state, {
      previewUrl: null
    });

    default:
    return state;

  }
}

export function getGalleryTheme(state: IGalleryState): IGalleryTheme | null {
  return state.currentTheme;
}
export function getGallerySubThemes(state: IGalleryState): IGalleryTheme[] {
  return state.currentSubThemes;
}
export function getGalleryItems(state: IGalleryState): IGalleryItem[] {
  return state.currentItems;
}
export function getGalleryLoading(state: IGalleryState): boolean {
  return state.subThemesLoading || state.itemsLoading;
}
export function getGalleryPreviewUrl(state: IGalleryState): string | null {
  return state.previewUrl;
}
