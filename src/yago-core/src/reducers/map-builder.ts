import { IMapLayer, IPersistentMap, IMapBuilderState } from '../interfaces/index';
import { MapBuilderActions, MapBuilderActionTypes } from '../actions/index';


const emptyMap: IPersistentMap = {id: null, name: null, content: null};


const initialState: IMapBuilderState = {
  styleEditedLayer: null,
  transformOptionsVisible: false,
  currentTransform: null,
  measureControlVisible: false,
  elevationProfileControlVisible: false,
  spatialAnalysisControlVisible: false,
  editedMap: Object.assign({}, emptyMap),
  editedMapChanged: false,
  loading: false,
  error: null,
  mapsListVisible: false,
  mapLoading: false,
  mapSaving: false
};


export function mapBuilderReducer(state = initialState, action: MapBuilderActions) {
  switch (action.type) {

    case MapBuilderActionTypes.EDIT_LAYER_STYLE:
    return Object.assign({}, state, {
      styleEditedLayer: action.payload
    });

    case MapBuilderActionTypes.CANCEL_EDIT_LAYER_STYLE:
    case MapBuilderActionTypes.SAVE_LAYER_STYLE:
    return Object.assign({}, state, {
      styleEditedLayer: null
    });

    case MapBuilderActionTypes.SHOW_TRANSFORM_OPTIONS:
    return Object.assign({}, state, {
      transformOptionsVisible: true,
      currentTransform: action.payload
    });

    case MapBuilderActionTypes.HIDE_TRANSFORM_OPTIONS:
    return Object.assign({}, state, {
      transformOptionsVisible: false,
      currentTransform: null
    });

    case MapBuilderActionTypes.TOGGLE_MEASURE_CONTROL:
    return Object.assign({}, state, {
      measureControlVisible: !state.measureControlVisible
    });

    case MapBuilderActionTypes.TOGGLE_ELEVATION_PROFILE_CONTROL:
    return Object.assign({}, state, {
      elevationProfileControlVisible: !state.elevationProfileControlVisible
    });

    case MapBuilderActionTypes.OPEN_SPATIAL_ANALYSIS_CONTROL:
    return Object.assign({}, state, {
      spatialAnalysisControlVisible: true
    });

    case MapBuilderActionTypes.CLOSE_SPATIAL_ANALYSIS_CONTROL:
    return Object.assign({}, state, {
      spatialAnalysisControlVisible: false
    });

    case MapBuilderActionTypes.TOGGLE_MAPS_LIST:
    return Object.assign({}, state, {
      mapsListVisible: !state.mapsListVisible
    });

    case MapBuilderActionTypes.SET_EDITED_MAP_NAME:
    return Object.assign({}, state, {
      editedMap: Object.assign({}, state.editedMap, {
        name: action.payload
      })
    });

    case MapBuilderActionTypes.LOAD_MAP:
    return Object.assign({}, state, {
      mapLoading: true,
      error: null
    });

    case MapBuilderActionTypes.LOAD_MAP_SUCCESS:
    return Object.assign({}, state, {
      mapLoading: false,
      editedMap: action.payload,
      error: null
    });

    case MapBuilderActionTypes.LOAD_MAP_FAILURE:
    return Object.assign({}, state, {
      mapLoading: false,
      editedMap: Object.assign({}, emptyMap),
      error: action.payload
    });

    case MapBuilderActionTypes.SAVE_MAP:
    return Object.assign({}, state, {
      mapSaving: true,
      error: null
    });

    case MapBuilderActionTypes.SAVE_MAP_FAILURE:
    return Object.assign({}, state, {
      mapSaving: false,
      error: action.payload
    });

    case MapBuilderActionTypes.SAVE_MAP_SUCCESS:
    return Object.assign({}, state, {
      mapSaving: false,
      error: null
    });

    default:
    return state;
  }
}


export function getStyleEditedLayer(state: IMapBuilderState): IMapLayer | null {
  return state.styleEditedLayer;
}
export function getTransformOptionsVisible(state: IMapBuilderState): boolean {
  return state.transformOptionsVisible;
}
export function getCurrentTransform(state: IMapBuilderState): number | null {
  return state.currentTransform;
}
export function getMeasureControlVisible(state: IMapBuilderState): boolean {
  return state.measureControlVisible;
}
export function getElevationProfileControlVisible(state: IMapBuilderState): boolean {
  return state.elevationProfileControlVisible;
}
export function getSpatialAnalysisControlVisible(state: IMapBuilderState): boolean {
  return state.spatialAnalysisControlVisible;
}
export function getMapsListVisible(state: IMapBuilderState): boolean {
  return state.mapsListVisible;
}
export function getMapBuilderLoading(state: IMapBuilderState): boolean {
  return state.loading;
}
export function getMapBuilderEditedMap(state: IMapBuilderState): IPersistentMap {
  return state.editedMap;
}
export function getMapBuilderEditedIsNewMap(state: IMapBuilderState): boolean {
  return state.editedMap == null || state.editedMap.id == null;
}
export function getMapBuilderEditedMapName(state: IMapBuilderState): string | null {
  return state.editedMap ? state.editedMap.name : null;
}
export function getMapBuilderMapSaving(state: IMapBuilderState): boolean {
  return state.mapSaving;
}
export function getMapBuilderMapLoading(state: IMapBuilderState): boolean {
  return state.mapLoading;
}
export function getMapBuilderError(state: IMapBuilderState): any {
  return state.error;
}
