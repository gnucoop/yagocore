import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';

import { Store } from '@ngrx/store';

import { ILayer, ILayerTreeItem, State } from '../interfaces/index';
import {
  LayersBrowserShowAction, LayersBrowserHideAction,
  LayersBrowserResetExpandedAction, LayersBrowserResetSelectionAction,
  LayersBrowserToggleItemAction, LayersBrowserToggleLayerAction,
  LayersBrowserSetFilterAction
} from '../actions/index';
import * as reducers from '../reducers/index';


@Injectable()
export class LayersBrowserService {
  private _filtered: Observable<{items: number[], layers: number[]}>;

  constructor(private _store: Store<State>) {
    this._filtered = this.getLayersTree()
      .combineLatest(this.getFilter())
      .map((res: [ILayerTreeItem[], string]) => {
        const tree = res[0];
        const filter = res[1];
        if (tree == null || tree.length === 0 || filter == null || filter.trim().length === 0) {
          return {items: [], layers: []};
        }
        let items: number[] = [];
        let layers: number[] = [];
        const cis = tree.map(item => this._getFilteredItems(item, filter));
        cis.forEach(ci => {
          items = items.concat(ci.items);
          layers = layers.concat(ci.layers);
        });
        return {items, layers};
      });
  }

  init(): void {
    this._store.dispatch(new LayersBrowserResetExpandedAction());
    this._store.dispatch(new LayersBrowserResetSelectionAction());
  }

  getLayersTree(): Observable<ILayerTreeItem[]> {
    return this._store.select(reducers.getLayersTree);
  }

  getVisible(): Observable<boolean> {
    return this._store.select(reducers.getLayersBrowserVisible);
  }

  getSelected(): Observable<number[]> {
    return this._store.select(reducers.getLayersBrowserSelected);
  }

  getExpanded(): Observable<number[]> {
    return this._store.select(reducers.getLayersBrowserExpanded);
  }

  show(): void {
    this.init();
    this._store.dispatch(new LayersBrowserShowAction());
  }

  hide(): void {
    this.init();
    this._store.dispatch(new LayersBrowserHideAction());
  }

  toggleItem(item: ILayerTreeItem): void {
    this._store.dispatch(new LayersBrowserToggleItemAction(item));
  }

  toggleLayer(layer: ILayer): void {
    this._store.dispatch(new LayersBrowserToggleLayerAction(layer));
  }

  getFilter(): Observable<string | null> {
    return this._store.select(reducers.getLayersBrowserFilter);
  }

  setFilter(filter: string | null): void {
    this._store.dispatch(new LayersBrowserSetFilterAction(filter || undefined));
  }

  getIsFiltered(): Observable<boolean> {
    return this.getFilter().map(f => f != null && f.trim().length > 0);
  }

  getFilteredItems(): Observable<number[]> {
    return this._filtered.map(f => f.items);
  }

  getFilteredLayers(): Observable<number[]> {
    return this._filtered.map(f => f.layers);
  }

  private _getFilteredItems(
    item: ILayerTreeItem, filter: string
  ): {items: number[], layers: number[]} {
    const re = new RegExp(filter, 'i');
    let layers = item.layers.filter(l => l.label.match(re))
      .filter(l => l.id).map(l => <number>l.id);
    let items: number[] = [];
    const childrenItems = (item.children || []).map(child => this._getFilteredItems(child, filter));
    childrenItems.forEach(childItem => {
      items = items.concat(childItem.items);
      layers = layers.concat(childItem.layers);
    });
    if (layers.length > 0) {
      items.push(item.theme.id);
    }
    return {items, layers};
  }
}
