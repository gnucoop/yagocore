import { Observable } from 'rxjs/Observable';


export interface IAuthService {
  checkExistentUsername(username: string): Observable<boolean>;
  checkExistentEmail(email: string): Observable<boolean>;
}
