/**
 * Represents a user of the platform
 *
 * @export
 * @interface IUser
 */
export interface IUser {
  readonly id: number;

  /**
   * The user's username (unique)
   *
   * @type {string}
   * @memberOf IUser
   */
  readonly username: string;

  /**
   * The user's email (unique)
   *
   * @type {string}
   * @memberOf IUser
   */
  readonly email: string;

  /**
   * The user's first name
   *
   * @type {string}
   * @memberOf IUser
   */
  readonly firstName: string;

  /**
   * The user's last name
   *
   * @type {string}
   * @memberOf IUser
   */
  readonly lastName: string;

  readonly isStaff: boolean;
}
