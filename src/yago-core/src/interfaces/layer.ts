import { ILayerTheme } from './layer-theme';
import { IMapLayerStyle, IMapConditionalLayerStyle } from './map-layer';


export type LayerTypes =
  'TILE' | 'VECTOR' | 'WMS_RASTER';


/**
 * Represents an abstract map layer.
 *
 * @export
 * @interface ILayer
 */
export interface ILayer {
  readonly id?: number;

  readonly layerType: LayerTypes;

  /**
   * The layer's name (unique)
   *
   * @readonly
   * @type {string}
   * @memberOf ILayer
   */
  readonly name: string;

  /**
   * The layer's label
   *
   * @readonly
   * @type {string}
   * @memberOf ILayer
   */
  readonly label: string;

  /**
   * The layer's theme
   *
   * @readonly
   * @type {number | ILayerTheme}
   * @memberOf ILayer
   */
  readonly theme?: number | ILayerTheme;

  /**
   * The layer's metadata URL
   *
   * @readonly
   * @type {string}
   * @memberOf ILayer
   */
  readonly metadata: string;

  readonly boundingBox: GeoJSON.Polygon;

  readonly features?: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>;

  readonly url?: string;

  readonly sourceName?: string;

  readonly dataLabels?: {[name: string]: string};

  readonly style?: IMapLayerStyle | IMapConditionalLayerStyle;

  readonly zIndex?: number;

  readonly visible?: boolean;
}
