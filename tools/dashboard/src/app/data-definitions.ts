/** Interface that describes the payload results from the Firebase database. */
export interface PayloadResult {
  timestamp: number;
  // yago Core bundles
  yago_core_umd: number;
  yago_core_umd_minified_uglify: number;
  yago_core_fesm_2015: number;
  yago_core_fesm_2014: number;
  // yago Md bundles
  yago_md_umd: number;
  yago_md_umd_minified_uglify: number;
  yago_md_fesm_2015: number;
  yago_md_fesm_2014: number;
}
