import { Component } from '@angular/core';

import { Observable } from 'rxjs/Observable';


@Component({
  moduleId: module.id,
  selector: 'map-builder-demo',
  templateUrl: 'map-builder-demo.html',
  styleUrls: ['map-builder-demo.css']
})
export class MapBuilderDemoComponent { }
