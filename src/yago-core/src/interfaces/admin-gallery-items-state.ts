import { IApiListOptions } from './api';
import { IGalleryItem } from './gallery-item';


export interface IAdminGalleryItemsState {
  listOptions: IApiListOptions | null;
  items: IGalleryItem[];
  itemsLoading: boolean;
  item: IGalleryItem | null;
  itemLoading: boolean;
  error: any;
}
