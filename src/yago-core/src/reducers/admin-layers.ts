import { IAdminLayersState, ILayer } from '../interfaces';
import { LayersActionTypes, LayersActions } from '../actions/index';


const initialState: IAdminLayersState = {
  listOptions: null,
  items: [],
  itemsLoading: false,
  item: null,
  itemLoading: false,
  error: null
};


export function adminLayersReducer(state = initialState, action: LayersActions) {
  if (action.admin) {
    switch (action.type) {

      case LayersActionTypes.GET:
      return Object.assign({}, state, {
        item: null,
        itemLoading: true,
        error: null
      });

      case LayersActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        item: action.payload,
        itemLoading: false,
        error: null
      });

      case LayersActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        item: null,
        itemLoading: false,
        error: action.payload
      });

      case LayersActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case LayersActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case LayersActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminLayer(state: IAdminLayersState): ILayer | null {
  return state.item;
}
export function getAdminLayerLoading(state: IAdminLayersState): boolean {
  return state.itemLoading;
}
export function getAdminLayers(state: IAdminLayersState): ILayer[] {
  return state.items;
}
export function getAdminLayersLoading(state: IAdminLayersState): boolean {
  return state.itemsLoading;
}
