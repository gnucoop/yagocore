import { BaseAction } from './base';
import { type } from '../utils/index';


// tslint:disable-next-line
export const GalleryActionTypes = {
  PREVIEW_IMAGE: type('[Gallery] Preview image'),
  PREVIEW_IMAGE_CLOSE: type('[Gallery] Preview image close')
};

export class GalleryPreviewImageAction implements BaseAction {
  type = GalleryActionTypes.PREVIEW_IMAGE;
  constructor(public payload: string, public admin = false) { }
}
export class GalleryPreviewImageCloseAction implements BaseAction {
  type = GalleryActionTypes.PREVIEW_IMAGE_CLOSE;
  payload = undefined;
  constructor(public admin = false) { }
}

export type GalleryActions =
  GalleryPreviewImageAction | GalleryPreviewImageCloseAction;
