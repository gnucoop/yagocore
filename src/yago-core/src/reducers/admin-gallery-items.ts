import { IAdminGalleryItemsState, IGalleryItem } from '../interfaces';
import { GalleryItemsActionTypes, GalleryItemsActions } from '../actions/index';


const initialState: IAdminGalleryItemsState = {
  listOptions: null,
  items: [],
  itemsLoading: false,
  item: null,
  itemLoading: false,
  error: null
};


export function adminGalleryItemsReducer(state = initialState, action: GalleryItemsActions) {
  if (action.admin) {
    switch (action.type) {

      case GalleryItemsActionTypes.GET:
      return Object.assign({}, state, {
        item: null,
        itemLoading: true,
        error: null
      });

      case GalleryItemsActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        item: action.payload,
        itemLoading: false,
        error: null
      });

      case GalleryItemsActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        item: null,
        itemLoading: false,
        error: action.payload
      });

      case GalleryItemsActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case GalleryItemsActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case GalleryItemsActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      case GalleryItemsActionTypes.CREATE_SUCCESS:
      case GalleryItemsActionTypes.UPDATE_SUCCESS:
      case GalleryItemsActionTypes.RESET_CURRENT_ITEM:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminGalleryItem(state: IAdminGalleryItemsState): IGalleryItem | null {
  return state.item;
}
export function getAdminGalleryItemLoading(state: IAdminGalleryItemsState): boolean {
  return state.itemLoading;
}
export function getAdminGalleryItems(state: IAdminGalleryItemsState): IGalleryItem[] {
  return state.items;
}
export function getAdminGalleryItemsLoading(state: IAdminGalleryItemsState): boolean {
  return state.itemsLoading;
}
