import { Action } from '@ngrx/store';

import { type } from '../utils/index';
import {
  IAuth, IAuthLoginPanel, IAuthRegistration, IAuthResetPassword,
  IAuthResponse, IUser
} from '../interfaces/index';


// tslint:disable-next-line
export const AuthActionTypes = {
  LOGIN_DIALOG_SHOW: type('[Auth] Show login dialog'),
  LOGIN_DIALOG_HIDE: type('[Auth] Hide login dialog'),
  SELECT_LOGIN_PANEL: type('[Auth] Select login panel'),
  INIT_TOKEN: type('[Auth] Init token'),
  INIT_USER: type('[Auth] Init user'),
  TOKEN_REFRESH: type('[Auth] Token refresh'),
  TOKEN_REFRESH_FAILURE: type('[Auth] Token refresh failure'),
  TOKEN_REFRESH_SUCCESS: type('[Auth] Token refresh success'),
  LOGIN: type('[Auth] Login'),
  LOGIN_FAILURE: type('[Auth] Login failure'),
  LOGIN_SUCCESS: type('[Auth] Login success'),
  LOGOUT: type('[Auth] Logout'),
  LOGOUT_FAILURE: type('[Auth] Logout failure'),
  LOGOUT_SUCCESS: type('[Auth] Logout success'),
  REGISTER: type('[Auth] Register'),
  REGISTER_FAILURE: type('[Auth] Register failure'),
  REGISTER_SUCCESS: type('[Auth] Register success'),
  RECOVER_PASSWORD: type('[Auth] Recover password'),
  RECOVER_PASSWORD_FAILURE: type('[Auth] Recover password failure'),
  RECOVER_PASSWORD_SUCCESS: type('[Auth] Recover password success'),
  RESET_PASSWORD: type('[Auth] Reset password'),
  RESET_PASSWORD_FAILURE: type('[Auth] Reset password failure'),
  RESET_PASSWORD_SUCCESS: type('[Auth] Reset password success')
};

export class AuthShowLoginDialogAction implements Action {
  type = AuthActionTypes.LOGIN_DIALOG_SHOW;
  constructor(public payload: any = null) { }
}
export class AuthHideLoginDialogAction implements Action {
  type = AuthActionTypes.LOGIN_DIALOG_HIDE;
  constructor(public payload: any = null) { }
}
export class AuthSelectLoginPanelAction implements Action {
  type = AuthActionTypes.SELECT_LOGIN_PANEL;
  constructor(public payload: IAuthLoginPanel) { }
}
export class AuthInitTokenAction implements Action {
  type = AuthActionTypes.INIT_TOKEN;
  constructor(public payload?: string) { }
}
export class AuthInitUserAction implements Action {
  type = AuthActionTypes.INIT_USER;
  constructor(public payload?: IUser) { }
}
export class AuthTokenRefreshAction implements Action {
  type = AuthActionTypes.TOKEN_REFRESH;
  constructor(public payload: any = null) { }
}
export class AuthTokenRefreshFailureAction implements Action {
  type = AuthActionTypes.TOKEN_REFRESH_FAILURE;
  constructor(public payload: any) { }
}
export class AuthTokenRefreshSuccessAction implements Action {
  type = AuthActionTypes.TOKEN_REFRESH_SUCCESS;
  constructor(public payload: IAuthResponse) { }
}
export class AuthLoginAction implements Action {
  type = AuthActionTypes.LOGIN;
  constructor(public payload: IAuth) { }
}
export class AuthLoginFailureAction implements Action {
  type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: any) { }
}
export class AuthLoginSuccessAction implements Action {
  type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: IAuthResponse) { }
}
export class AuthLogoutAction implements Action {
  type = AuthActionTypes.LOGOUT;
  payload = undefined;
  constructor() { }
}
export class AuthLogoutFailureAction implements Action {
  type = AuthActionTypes.LOGOUT_FAILURE;
  constructor(public payload: any) { }
}
export class AuthLogoutSuccessAction implements Action {
  type = AuthActionTypes.LOGOUT_SUCCESS;
  payload = undefined;
  constructor() { }
}
export class AuthRegisterAction implements Action {
  type = AuthActionTypes.REGISTER;
  constructor(public payload: IAuthRegistration) { }
}
export class AuthRegisterFailureAction implements Action {
  type = AuthActionTypes.REGISTER_FAILURE;
  constructor(public payload: any) { }
}
export class AuthRegisterSuccessAction implements Action {
  type = AuthActionTypes.REGISTER_SUCCESS;
  constructor(public payload: IAuthResponse) { }
}
export class AuthRecoverPasswordAction implements Action {
  type = AuthActionTypes.RECOVER_PASSWORD;
  constructor(public payload: string) { }
}
export class AuthRecoverPasswordFailureAction implements Action {
  type = AuthActionTypes.RECOVER_PASSWORD_FAILURE;
  constructor(public payload: any) { }
}
export class AuthRecoverPasswordSuccessAction implements Action {
  type = AuthActionTypes.RECOVER_PASSWORD_SUCCESS;
  constructor(public payload = undefined) { }
}
export class AuthResetPasswordAction implements Action {
  type = AuthActionTypes.RESET_PASSWORD;
  constructor(public payload: IAuthResetPassword) { }
}
export class AuthResetPasswordFailureAction implements Action {
  type = AuthActionTypes.RESET_PASSWORD_FAILURE;
  constructor(public payload: any) { }
}
export class AuthResetPasswordSuccessAction implements Action {
  type = AuthActionTypes.RESET_PASSWORD_SUCCESS;
  constructor(public payload = undefined) { }
}
export type AuthActions = AuthShowLoginDialogAction | AuthHideLoginDialogAction |
  AuthSelectLoginPanelAction |
  AuthInitTokenAction | AuthInitUserAction |
  AuthTokenRefreshAction | AuthTokenRefreshFailureAction | AuthTokenRefreshSuccessAction |
  AuthLoginAction | AuthLoginFailureAction | AuthLoginSuccessAction |
  AuthLogoutAction | AuthLogoutFailureAction | AuthLogoutSuccessAction |
  AuthRegisterAction | AuthRegisterFailureAction | AuthRegisterSuccessAction |
  AuthRecoverPasswordAction | AuthRecoverPasswordFailureAction | AuthRecoverPasswordSuccessAction |
  AuthResetPasswordAction | AuthResetPasswordFailureAction | AuthResetPasswordSuccessAction;
