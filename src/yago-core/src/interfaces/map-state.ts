import {
  IMap, MapBaseLayer, MapCenter, MapImageFormat, MapParams, MapSpatialFilterMode
} from './map';
import { IMapLayer } from './map-layer';


export interface IMapViewState {
  zoomLevel: number;
  center: MapCenter;
  mapParams: MapParams;
}

export interface IMapSelectedFeatures {
  layer: number;
  features: any[];
}

export interface IMapSpatialFilter {
  layer: IMapLayer;
  polygon: number[][];
  mode: MapSpatialFilterMode;
}

export interface IMapShowFeatureInfo {
  feature: IMapSelectedFeatures;
  properties: {[prop: string]: any};
  labels: {[prop: string]: string};
  x: number;
  y: number;
}


export interface IMapState {
  item: IMap | null;
  layers: IMapLayer[];
  viewState: IMapViewState[];
  currentViewState: number;
  loading: boolean;
  error: any;
  selectedLayer: number | null;
  selectedFeatures: IMapSelectedFeatures[];
  filteredFeatures: IMapSelectedFeatures[];
  saveImageRequest: MapImageFormat | null;
  saveImageLoading: boolean;
  print: boolean;
  legendControlOpen: boolean;
  tableControlOpen: boolean;
  baseLayer: MapBaseLayer;
  showFeatureInfo: IMapShowFeatureInfo | null;
}
