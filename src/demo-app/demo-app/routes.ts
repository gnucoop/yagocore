import { Routes } from '@angular/router';
import { HomeComponent } from './demo-app';
import { MapBuilderDemoComponent } from '../map-builder/map-builder-demo';

export const DEMO_APP_ROUTES: Routes = [
  {path: '', component: HomeComponent},
  {path: 'map-builder', component: MapBuilderDemoComponent}
];
