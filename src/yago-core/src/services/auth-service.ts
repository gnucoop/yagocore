import { Inject, Injectable, Optional } from '@angular/core';

import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ROOT_EFFECTS_INIT } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import {
  IAuth, IAuthLoginPanel, IAuthRegistration, IAuthResetPassword, IAuthResponse,
  IAuthService, IUser, State
} from '../interfaces/index';
import {
  AuthActions, AuthActionTypes,
  AuthShowLoginDialogAction, AuthHideLoginDialogAction, AuthSelectLoginPanelAction,
  AuthLoginAction, AuthLoginSuccessAction, AuthLoginFailureAction,
  AuthLogoutAction, AuthLogoutSuccessAction, AuthLogoutFailureAction,
  AuthInitTokenAction, AuthInitUserAction,
  AuthTokenRefreshAction, AuthTokenRefreshSuccessAction, AuthTokenRefreshFailureAction,
  AuthRegisterAction, AuthRegisterSuccessAction, AuthRegisterFailureAction,
  AuthRecoverPasswordAction, AuthRecoverPasswordFailureAction, AuthRecoverPasswordSuccessAction,
  AuthResetPasswordAction, AuthResetPasswordFailureAction, AuthResetPasswordSuccessAction,
  Go, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';
import { AuthConfig, AuthConfigOptional, JwtHelper, AUTH_CONFIG } from './http';


@Injectable()
export class AuthService implements IAuthService {
  constructor(
    private _store: Store<State>,
    private _api: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) { }

  getLoginDialogVisible(): Observable<boolean> {
    return this._store.select(reducers.getLoginDialogVisible);
  }

  getCurrentLoginPanel(): Observable<IAuthLoginPanel> {
    return this._store.select(reducers.getLoginPanel);
  }

  getLoading(): Observable<boolean> {
    return this._store.select(reducers.getAuthLoading);
  }

  getLoggedIn(): Observable<boolean> {
    return this._store.select(reducers.getLoggedIn);
  }

  showLoginPanel(): void {
    this._selectLoginPanel('login');
  }

  showRegistrationPanel(): void {
    this._selectLoginPanel('register');
  }

  showPasswordRecoveryPanel(): void {
    this._selectLoginPanel('recover_password');
  }

  openLoginDialog(): void {
    this._store.dispatch(new AuthShowLoginDialogAction());
  }

  closeLoginDialog(): void {
    this._store.dispatch(new AuthHideLoginDialogAction());
  }

  login(data: IAuth): void {
    this._store.dispatch(new AuthLoginAction(data));
  }

  logout(): void {
    this._store.dispatch(new AuthLogoutAction());
  }

  checkExistentUsername(username: string): Observable<boolean> {
    return this._api
      .get(`${this._config.checkExistentUsername}/${username}`, undefined, false)
      .map(r => r.exists === true);
  }

  checkExistentEmail(email: string): Observable<boolean> {
    return this._api
      .get(`${this._config.checkExistentEmail}/${email}`, undefined, false)
      .map(r => r.exists === true);
  }

  register(user: IAuthRegistration): void {
    this._store.dispatch(new AuthRegisterAction(user));
  }

  getRecoverPasswordMailSent(): Observable<boolean> {
    return this._store.select(reducers.getRecoverPasswordMailSent);
  }

  recoverPassword(email: string): void {
    this._store.dispatch(new AuthRecoverPasswordAction(email));
  }

  resetPassword(data: IAuthResetPassword): void {
    this._store.dispatch(new AuthResetPasswordAction(data));
  }

  getUserIsStaff(): Observable<boolean> {
    return this._store.select(reducers.getAuthUserIsStaff);
  }

  private _selectLoginPanel(panel: IAuthLoginPanel): void {
    this._store.dispatch(new AuthSelectLoginPanelAction(panel));
  }
}


@Injectable()
export class AuthEffects {
  @Effect()
  login$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.LOGIN)
    .map((action: AuthLoginAction) => action.payload)
    .switchMap((options: IAuth) =>
      this._api.create(this._config.loginEndpoint, options, false)
        .map((r) => new AuthLoginSuccessAction(r))
        .catch((err) => Observable.of(new AuthLoginFailureAction(err)))
    );

  @Effect()
  logout$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.LOGOUT)
    .switchMap(() =>
      this._api.create(this._config.logoutEndpoint, {}, true)
        .map((_) => new AuthLogoutSuccessAction())
        .catch((err) => Observable.of(new AuthLogoutFailureAction(err)))
    );

  @Effect({dispatch: false})
  tokenRefreshTimer$: Observable<string | undefined> = this._actions
    .ofType(AuthActionTypes.INIT_TOKEN)
    .map((action: AuthInitTokenAction) => action.payload)
    .do((token: string) => {
      if (token == null) {
        this._stopRefreshTimer();
      } else {
        this._startRefreshTimer(token);
      }
    });

  @Effect()
  initToken$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.INIT_TOKEN)
    .map((action: AuthInitTokenAction) => action.payload)
    .filter((token: string) => token != null)
    .switchMap((_token: string) =>
      this._api.get(this._config.meEndpoint, undefined)
        .map((user: IUser) => new AuthInitUserAction(user))
        .catch((_err) => Observable.of(new AuthInitUserAction(undefined)))
    );

  @Effect()
  loginSuccess$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.LOGIN_SUCCESS)
    .map((action: AuthLoginSuccessAction) => action.payload)
    .do((r: IAuthResponse) => {
      this._startRefreshTimer(r.token);
      localStorage.setItem(this._tokenName, r.token);
      this._store.dispatch(new AuthInitTokenAction(r.token));
    })
    .map(() => new AuthHideLoginDialogAction());

  @Effect({dispatch: false})
  loggedOut$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.LOGOUT_SUCCESS)
    .do(() => {
      this._stopRefreshTimer();
      localStorage.removeItem(this._tokenName);
    });

  @Effect()
  tokenRefresh$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.TOKEN_REFRESH)
    .map((action: AuthTokenRefreshAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getAuthToken))
    .switchMap((v: [any, string]) =>
      this._api.create(this._config.authRefreshEndpoint, {token: v[1]})
        .map((res: IAuthResponse) => new AuthTokenRefreshSuccessAction(res))
        .catch((err) => Observable.of(new AuthTokenRefreshFailureAction(err)))
    );

  @Effect({dispatch: false})
  tokenRefreshSuccess$: Observable<string> = this._actions
    .ofType(AuthActionTypes.TOKEN_REFRESH_SUCCESS)
    .map((action: AuthTokenRefreshSuccessAction) => action.payload.token)
    .do((token: string) => {
      localStorage.setItem(this._tokenName, token);
    });

  @Effect()
  register$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.REGISTER)
    .map((action: AuthRegisterAction) => action.payload)
    .switchMap((user: IAuthRegistration) =>
      this._api.create(this._config.registerEndpoint, user, false)
        .map((r) => new AuthRegisterSuccessAction(r))
        .catch((err) => Observable.of(new AuthRegisterFailureAction(err)))
    );

  @Effect()
  registerSuccess$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.REGISTER_SUCCESS)
    .map((action: AuthRegisterSuccessAction) =>
      new AuthLoginSuccessAction(action.payload));

  @Effect()
  recoverPassword$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.RECOVER_PASSWORD)
    .map((action: AuthRecoverPasswordAction) => action.payload)
    .switchMap((email: string) =>
      this._api.create(this._config.recoverPasswordEndpoint, {email}, false)
        .map(() => new AuthRecoverPasswordSuccessAction())
        .catch((err) => Observable.of(new AuthRecoverPasswordFailureAction(err)))
    );

  @Effect()
  recoverPasswordSuccess$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.RECOVER_PASSWORD_SUCCESS)
    .map(() => new AuthHideLoginDialogAction());

  @Effect()
  resetPassword$: Observable<AuthActions> = this._actions
    .ofType(AuthActionTypes.RESET_PASSWORD)
    .map((action: AuthResetPasswordAction) => action.payload)
    .switchMap((data: IAuthResetPassword) =>
      this._api.create(this._config.resetPasswordEndpoint, data, false)
        .map((_) => new AuthResetPasswordSuccessAction())
        .catch((err) => Observable.of(new AuthResetPasswordFailureAction(err)))
    );

  @Effect()
  resetPasswordSuccess$: Observable<RouterActions> = this._actions
    .ofType(AuthActionTypes.RESET_PASSWORD_SUCCESS)
    .map((_: AuthResetPasswordSuccessAction) => new Go({path: ['/']}));

  private _tokenName: string;
  private _refreshTimer: number;

  constructor(
    private _store: Store<State>, private _actions: Actions, private _api: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig,
    @Inject(AUTH_CONFIG) @Optional() options?: AuthConfigOptional
  ) {
    this._tokenName = new AuthConfig(options).getConfig().tokenName;

    this._actions.ofType(ROOT_EFFECTS_INIT)
      .subscribe(() => this._initAuth(options));
  }

  private _initAuth(_options?: AuthConfigOptional) {
    const tokenName = this._tokenName;
    const token = localStorage.getItem(tokenName) || '';

    let tokenInvalid: boolean;
    try {
      tokenInvalid = JwtHelper.isTokenExpired(token);
    } catch (e) {
      tokenInvalid = true;
    }

    if (tokenInvalid) {
      localStorage.removeItem(tokenName);
      this._store.dispatch(new AuthInitTokenAction());
    } else {
      this._store.dispatch(new AuthInitTokenAction(token));
    }
  }

  private _startRefreshTimer(currentToken: string): void {
    const expiration = (<Date>JwtHelper.getTokenExpirationDate(currentToken)).getTime();
    const now = new Date().getTime();
    const firstDelta = Math.round((expiration - now) * 0.8);
    const delta = this._config.tokenExpirationDelta * 800;

    this._stopRefreshTimer();
    this._refreshTimer = -1;

    setTimeout(() => {
      this._store.dispatch(new AuthTokenRefreshAction());
      this._refreshTimer = <any>setInterval(() => {
        this._store.dispatch(new AuthTokenRefreshAction());
      }, delta);
    }, firstDelta);
  }

  private _stopRefreshTimer(): void {
    if (this._refreshTimer != null && this._refreshTimer !== -1) {
      clearInterval(this._refreshTimer);
    }
  }
}
