import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';

import { Action, Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import {
  ILayer, IMapLayerStyle, IMapLayer, IPersistentMap, MapBaseLayer, State
} from '../interfaces/index';
import {
  ThemesActions, ThemesListAction, LayersActions, LayersListAction,
  MapAddLayerAction, MapActions, MapSetMapAction, MapUpdateLayerStyleAction,
  MapAssignLayerUniqueIdAction,
  MapBuilderActions, MapBuilderActionTypes, MapBuilderInitAction,
  MapBuilderAddSelectedLayersAction, MapBuilderAddSelectedLayerAction,
  MapBuilderAddSelectedLayerFailureAction,
  MapBuilderEditLayerStyleAction, MapBuilderSaveLayerStyleAction,
  MapBuilderCancelEditLayerStyleAction,
  MapBuilderShowTransformOptionsAction, MapBuilderHideTransformOptionsAction,
  MapBuilderCreateGeoStatisticalLayerAction, MapBuilderCreateGeoStatisticalLayerFailureAction,
  MapBuilderCreateGeoStatisticalLayerSuccessAction,
  MapBuilderToggleMeasureControlAction, MapBuilderToggleElevationProfileControlAction,
  MapBuilderOpenSpatialAnalysisControlAction, MapBuilderCloseSpatialAnalysisControlAction,
  MapBuilderToggleMapsListAction,
  MapBuilderSetEditedMapName,
  MapBuilderLoadMapAction, MapBuilderLoadMapFailureAction, MapBuilderLoadMapSuccessAction,
  MapBuilderSaveMapAction, MapBuilderSaveMapFailureAction, MapBuilderSaveMapSuccessAction,
  MapBuilderDeleteMapAction, MapBuilderDeleteMapFailureAction, MapBuilderDeleteMapSuccessAction
} from '../actions/index';
import { LayersManager } from './layers-manager';
import { MapsManager } from './maps-manager';
import { MapService } from './map';
import * as reducers from '../reducers/index';
import { persistentMapToMap } from '../utils/index';
import {
  GeoStatisticTransform, BufferTransform, ChoroplethTransform, ProportionalSymbolsTransform
} from './geostats/index';
import { LayersBrowserService } from './layers-browser';


@Injectable()
export class MapBuilderService {
  private _geostatsTransforms: (typeof GeoStatisticTransform)[];
  get geostatsTransforms(): (typeof GeoStatisticTransform)[] { return this._geostatsTransforms; }

  constructor(
    private _store: Store<State>,
    private _layersBrowser: LayersBrowserService
  ) {
    this._geostatsTransforms = [BufferTransform, ChoroplethTransform, ProportionalSymbolsTransform];
  }

  init(): void {
    this._store.dispatch(new MapBuilderInitAction());
  }

  getLoading(): Observable<boolean> {
    return this._store.select(reducers.getMapBuilderLoading);
  }

  getMapsListVisible(): Observable<boolean> {
    return this._store.select(reducers.getMapsListVisible);
  }

  getTransformOptionsVisible(): Observable<boolean> {
    return this._store.select(reducers.getTransformOptionsVisibile);
  }

  getStyleEditedLayer(): Observable<IMapLayer | null> {
    return this._store.select(reducers.getStyleEditedLayer);
  }

  getEditedMap(): Observable<IPersistentMap> {
    return this._store.select(reducers.getMapBuilderEditedMap);
  }

  getEditedMapName(): Observable<string | null> {
    return this._store.select(reducers.getMapBuilderEditedMapName);
  }

  setEditedMapName(name: string): void {
    this._store.dispatch(new MapBuilderSetEditedMapName(name));
  }

  addSelectedLayers(): void {
    this._store.dispatch(new MapBuilderAddSelectedLayersAction());
  }

  editLayerStyle(layer: IMapLayer): void {
    this._store.dispatch(new MapBuilderEditLayerStyleAction(layer));
  }

  saveLayerStyle(layer: IMapLayer, style: IMapLayerStyle): void {
    this._store.dispatch(new MapBuilderSaveLayerStyleAction({layer, style}));
  }

  cancelEditLayerStyle(): void {
    this._store.dispatch(new MapBuilderCancelEditLayerStyleAction());
  }

  showLayersBrowser(): void {
    this._layersBrowser.show();
  }

  hideLayersBrowser(): void {
    this._layersBrowser.hide();
  }

  showTransformOptions(transform: typeof GeoStatisticTransform): void {
    this._store.dispatch(
      new MapBuilderShowTransformOptionsAction(this._geostatsTransforms.indexOf(transform))
    );
  }

  hideTransformOptions(): void {
    this._store.dispatch(new MapBuilderHideTransformOptionsAction());
  }

  createGeoStatisticalLayer(
    layer: IMapLayer, transform: typeof GeoStatisticTransform, options: {[key: string]: any}
  ): void {
    this._store.dispatch(new MapBuilderCreateGeoStatisticalLayerAction({
      layer: layer.uniqueId,
      transform: this._geostatsTransforms.indexOf(transform),
      options
    }));
  }

  getMeasureControlVisible(): Observable<boolean> {
    return this._store.select(reducers.getMeasureControlVisible);
  }

  getElevationProfileControlVisible(): Observable<boolean> {
    return this._store.select(reducers.getElevationProfileControlVisible);
  }

  getSpatialAnalysisControlVisible(): Observable<boolean> {
    return this._store.select(reducers.getSpatialAnalysisControlVisible);
  }

  openSpatialAnalysisControl(): void {
    this._store.dispatch(new MapBuilderOpenSpatialAnalysisControlAction());
  }

  closeSpatialAnalysisControl(): void {
    this._store.dispatch(new MapBuilderCloseSpatialAnalysisControlAction());
  }

  toggleMeasureControl(): void {
    this._store.dispatch(new MapBuilderToggleMeasureControlAction());
  }

  toggleElevationProfileControl(): void {
    this._store.dispatch(new MapBuilderToggleElevationProfileControlAction());
  }

  toggleMapsList(): void {
    this._store.dispatch(new MapBuilderToggleMapsListAction());
  }

  saveCurrentMap(): void {
    this._store.dispatch(new MapBuilderSaveMapAction());
  }

  loadMap(id: number): void {
    this._store.dispatch(new MapBuilderLoadMapAction(id));
  }

  getMapLoading(): Observable<boolean> {
    return this._store.select(reducers.getMapBuilderMapLoading);
  }

  getMapSaving(): Observable<boolean> {
    return this._store.select(reducers.getMapBuilderMapSaving);
  }

  getError(): Observable<any> {
    return this._store.select(reducers.getMapBuilderError);
  }

  deleteMap(id: number): void {
    this._store.dispatch(new MapBuilderDeleteMapAction(id));
  }
}


@Injectable()
export class MapBuilderEffects {
  @Effect()
  init$: Observable<ThemesActions | LayersActions> = this._actions
    .ofType(MapBuilderActionTypes.INIT)
    .mergeMap(() => [
      new ThemesListAction(),
      new LayersListAction()
    ]);

  @Effect()
  addSelectedLayers$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.ADD_SELECTED_LAYERS)
    .withLatestFrom(
      this._store.select(reducers.getLayersBrowserSelected)
        .filter((s: number[]) => s != null && s.length > 0)
    )
    .mergeMap((r: [MapBuilderActions, number[]]) =>
      r[1].map(lid => new MapBuilderAddSelectedLayerAction(lid)));

  @Effect()
  addSelectedLayer$: Observable<MapActions | MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.ADD_SELECTED_LAYER)
    .mergeMap((action: MapBuilderAddSelectedLayerAction) =>
      this._layersManager.get(action.payload)
        .map((layer: ILayer) => new MapAddLayerAction(layer))
      .catch((error: any) => Observable.of(new MapBuilderAddSelectedLayerFailureAction(error))));

  @Effect()
  saveLayerStyle$: Observable<MapActions> = this._actions
    .ofType(MapBuilderActionTypes.SAVE_LAYER_STYLE)
    .map((action: MapBuilderSaveLayerStyleAction) => action.payload)
    .map((r: {layer: IMapLayer, style: IMapLayerStyle}) =>
      new MapUpdateLayerStyleAction(Object.assign({}, r.layer, {style: r.style}))
    );

  @Effect()
  createGeoStatisticalLayer$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.CREATE_GEOSTATISTICAL_LAYER)
    .map((action: MapBuilderCreateGeoStatisticalLayerAction) => action.payload)
    .withLatestFrom(this._store.select(reducers.getMapLayers))
    .map((r: [{layer: number, transform: number, options: {[key: string]: any}}, IMapLayer[]]) => {
      const def = r[0];
      const layers = r[1];
      const transforms = this._mapBuilderService.geostatsTransforms;

      const layer = layers.find(l => l.uniqueId === def.layer);
      const transform = def.transform >= 0 && def.transform < (transforms || []).length ?
        transforms[def.transform] : null;
      if (layer != null && transform != null) {
         return new MapBuilderCreateGeoStatisticalLayerSuccessAction(
           Object.assign({}, transform.transform(layer, def.options), {
             label: `${transform.getName()}: ${layer.label}`
           })
         );
      }
      return new MapBuilderCreateGeoStatisticalLayerFailureAction({});
    });

  @Effect()
  geostatisticalLayerCreated$: Observable<MapActions> = this._actions
    .ofType(MapBuilderActionTypes.CREATE_GEOSTATISTICAL_LAYER_SUCCESS)
    .map((action: MapBuilderCreateGeoStatisticalLayerSuccessAction) => {
      return new MapAssignLayerUniqueIdAction(action.payload);
    });

  @Effect()
  saveMap$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.SAVE_MAP)
    .withLatestFrom(
      this._mapBuilderService.getEditedMap(),
      this._mapService.getLayers(),
      this._mapService.getBaseLayer()
    )
    .switchMap((r: [Action, IPersistentMap, IMapLayer[], MapBaseLayer]) => {
      const layers = r[2];
      const baseLayer = r[3];
      const map: IPersistentMap = Object.assign({}, r[1], {
        content: {baseLayer, layers}
      });
      if (map != null && map.name != null && map.content != null) {
        return (
          map.id == null ? this._mapsManager.create(map) : this._mapsManager.update(map.id, map)
          )
          .map((m) => new MapBuilderSaveMapSuccessAction(m))
          .catch((err) => Observable.of(new MapBuilderSaveMapFailureAction(err)));
      }
      return Observable.of(new MapBuilderSaveMapFailureAction({}));
    });

  @Effect()
  loadMapToggleMapsList$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.LOAD_MAP)
    .map(() => new MapBuilderToggleMapsListAction());

  @Effect()
  loadMap$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.LOAD_MAP)
    .map((action: MapBuilderLoadMapAction) => action.payload)
    .switchMap((id: number) =>
      this._mapsManager.get(id)
        .map((map: IPersistentMap) => new MapBuilderLoadMapSuccessAction(map))
        .catch((err: any) => Observable.of(new MapBuilderLoadMapFailureAction(err)))
    );

  @Effect()
  loadMapSuccess$: Observable<MapActions> = this._actions
    .ofType(MapBuilderActionTypes.LOAD_MAP_SUCCESS)
    .map((action: MapBuilderLoadMapSuccessAction) => action.payload)
    .map((map: IPersistentMap) => new MapSetMapAction(persistentMapToMap(map)));

  @Effect()
  deleteMap$: Observable<MapBuilderActions> = this._actions
    .ofType(MapBuilderActionTypes.DELETE_MAP)
    .map((action: MapBuilderDeleteMapAction) => action.payload)
    .switchMap((id: number) =>
      this._mapsManager.delete(id)
        .map((map) => new MapBuilderDeleteMapSuccessAction(map))
        .catch((err: any) => Observable.of(new MapBuilderDeleteMapFailureAction(err)))
    );

  constructor(
    private _actions: Actions,
    private _store: Store<State>,
    private _layersManager: LayersManager,
    private _mapsManager: MapsManager,
    private _mapService: MapService,
    private _mapBuilderService: MapBuilderService
  ) { }
}
