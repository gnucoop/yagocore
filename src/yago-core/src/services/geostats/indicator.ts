import * as Color from 'color';

import { GeoStatisticIndicatorType, GeoStatisticTransform } from './base';
import { IMapLayer } from '../../interfaces/index';


export type ColorModel = 'rgb' | 'hex' | 'hsl';
export type ColorParam = Color | string | ArrayLike<number> | number | { [key: string]: any };
const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;


export abstract class IndicatorBasedTransform extends GeoStatisticTransform {
  protected static _getStepsColors(steps: number, startColor: string, endColor: string): string[] {
    const colors: string[] = [];
    const sc = colorConstructor(startColor, 'hex');
    const ec = colorConstructor(endColor, 'hex');
    const redDelta = ec.red() - sc.red();
    const greenDelta = ec.green() - sc.green();
    const blueDelta = ec.blue() - sc.blue();
    const redStep = redDelta !== 0 ? redDelta / steps : 0;
    const greenStep = greenDelta !== 0 ? greenDelta / steps : 0;
    const blueStep = blueDelta !== 0 ? blueDelta / steps : 0;
    for (let i = 0 ; i < steps ; i++) {
      colors.push(
        colorConstructor(
          sc.red(sc.red() + redStep * i)
            .green(sc.green() + greenStep * i)
            .blue(sc.blue() + blueStep * 1),
          'hex'
        ).string()
      );
    }
    colors.push(ec.string());

    return colors;
  }

  protected static _getTextSteps(layer: IMapLayer, indicator: string): string[] {
    const values = IndicatorBasedTransform._getIndicatorValues(layer, indicator);
    return values.sort((a, b) => (a || '').localeCompare(b || ''));
  }

  protected static _getQuantilesSteps(
    layer: IMapLayer, indicator: string, classes: number
  ): [number, number][] | null {
    if (classes <= 1) { throw new Error('Invalid classes number'); }
    let values: number[] = IndicatorBasedTransform._getIndicatorValues(layer, indicator);
    const vMin = Math.min(...values);
    const vMax = Math.max(...values);
    if (vMax - vMin === 0.0) { return null; }
    values = values.sort((a, b) => a - b);
    const perInterval = Math.floor(values.length / classes);
    const steps: [number, number][] = [];
    const lastClass = classes - 1;
    const lastVal = values.length - 1;
    for (let i = 0; i < classes ; i++) {
      const fv = values[i * perInterval];
      const sv = i === lastClass ? values[lastVal] : values[(i + 1) * perInterval];
      steps.push([fv, sv]);
    }
    return steps;
  }

  protected static _getEqualIntervalsSteps(
    layer: IMapLayer, indicator: string, classes: number
  ): [number, number][] | null {
    if (classes <= 1) { throw Error('Invalid classes number'); }
    const values: number[] = IndicatorBasedTransform._getIndicatorValues(layer, indicator);
    const vMin = Math.min(...values);
    const vMax = Math.max(...values);
    const diff = vMax - vMin;
    if (diff === 0.0) { return null; }
    const delta = diff / classes;
    const lastClass = classes - 1;
    const steps: [number, number][] = [];
    for (let i = 0; i < classes ; i++) {
      steps.push([vMin + i * delta, i === lastClass ? vMax : vMin + (i + 1) * delta]);
    }
    return steps;
  }

  protected static _guessPropertyType(val: any): GeoStatisticIndicatorType {
    try {
      const fVal = parseFloat(val);
      if (!isNaN(fVal)) {
        return 'number';
      }
    } catch (e) { }
    return 'string';
  }

  protected static _getIndicators(
    layer: IMapLayer, types: GeoStatisticIndicatorType[] = ['string', 'number']
  ): {indicators: {[name: string]: GeoStatisticIndicatorType}, values: {[name: string]: any[]}} {
    const indicators: {[name: string]: GeoStatisticIndicatorType} = {};
    const indicatorsValues: {[name: string]: any[]} = {};
    (<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features).features.forEach((f) => {
      const cis = Object.keys(f.properties || {});
      cis.forEach(ci => {
        const propValue = (<any>f.properties)[ci];
        const ct = IndicatorBasedTransform._guessPropertyType(propValue);
        if (ct === 'number' && (indicators[ci] == null || indicators[ci] === 'number')) {
          indicators[ci] = 'number';
        } else {
          indicators[ci] = 'string';
        }
        if (indicatorsValues[ci] == null) { indicatorsValues[ci] = []; }
        if (indicatorsValues[ci].indexOf(propValue) === -1) {
          indicatorsValues[ci].push(propValue);
        }
      });
    });
    const filteredIndicators: {[name: string]: GeoStatisticIndicatorType} = {};
    const filteredIndicatorsValues: {[name: string]: any[]} = {};
    Object.keys(indicators).forEach(i => {
      if (types.indexOf(indicators[i]) > -1) {
        filteredIndicators[i] = indicators[i];
        filteredIndicatorsValues[i] = indicatorsValues[i];
      }
    });

    return {indicators: filteredIndicators, values: filteredIndicatorsValues};
  }

  private static _getIndicatorValues(layer: IMapLayer, indicator: string): any[] {
    const values: any[] = [];
    (<GeoJSON.FeatureCollection<GeoJSON.GeometryObject>>layer.features).features.forEach(f => {
      const val = (<any>f.properties)[indicator];
      if (values.indexOf(val) === -1) {
        values.push(val);
      }
    });
    return values;
  }
}
