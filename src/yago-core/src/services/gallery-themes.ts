import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import { IApiListOptions, IGalleryTheme, State } from '../interfaces/index';
import {
  GalleryThemesActions, GalleryThemesActionTypes,
  GalleryThemesCreateAction, GalleryThemesCreateFailureAction, GalleryThemesCreateSuccessAction,
  GalleryThemesGetAction, GalleryThemesGetFailureAction, GalleryThemesGetSuccessAction,
  GalleryThemesListAction, GalleryThemesListFailureAction, GalleryThemesListSuccessAction,
  GalleryThemesListByParentIdAction,
  GalleryThemesListByParentAction, GalleryThemesListByParentFailureAction,
  GalleryThemesListByParentSuccessAction,
  GalleryThemesUpdateAction, GalleryThemesUpdateFailureAction, GalleryThemesUpdateSuccessAction,
  GalleryThemesDeleteAction, GalleryThemesDeleteFailureAction, GalleryThemesDeleteSuccessAction,
  GalleryThemesResetCurrentThemeAction,
  Back, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ModelService } from './model';
import { GalleryThemesManager } from './gallery-themes-manager';


@Injectable()
export class GalleryThemesService extends ModelService<
    IGalleryTheme, GalleryThemesGetAction, GalleryThemesListAction,
    GalleryThemesCreateAction, GalleryThemesUpdateAction, GalleryThemesDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, GalleryThemesGetAction, GalleryThemesListAction,
      GalleryThemesCreateAction, GalleryThemesUpdateAction,
      GalleryThemesDeleteAction
    );
  }

  getAdminThemes(): Observable<IGalleryTheme[]> {
    return this._store.select(reducers.getAdminGalleryThemes);
  }

  getAdminTheme(): Observable<IGalleryTheme | null> {
    return this._store.select(reducers.getAdminGalleryTheme);
  }

  getAdminThemesLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminGalleryThemesLoading);
  }

  getAdminThemeLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminGalleryThemeLoading);
  }

  listByParent(parent?: IGalleryTheme, options?: IApiListOptions): void {
    this._store.dispatch(new GalleryThemesListByParentAction({parent, options}));
  }

  resetCurrentTheme(admin = false): void {
    this._store.dispatch(new GalleryThemesResetCurrentThemeAction(admin));
  }
}

@Injectable()
export class GalleryThemesEffects {
  @Effect()
  listByParentId$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.LIST_BY_PARENT_ID)
    .switchMap((action: GalleryThemesListByParentIdAction) =>
      this._manager.get(action.payload.parent)
        .map((parent: IGalleryTheme) =>
          new GalleryThemesListByParentAction(
            {parent, options: action.payload.options}, action.admin
          ))
        .catch((err: any) => Observable.of(new GalleryThemesListByParentFailureAction(err)))
    );

  @Effect()
  listByParent$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.LIST_BY_PARENT)
    .switchMap((action: GalleryThemesListByParentAction) =>
      this._manager.listByParent(action.payload.parent, action.payload.options)
        .map((themes: IGalleryTheme[]) =>
          new GalleryThemesListByParentSuccessAction(themes, action.admin))
        .catch((err: any) => Observable.of(new GalleryThemesListByParentFailureAction(err)))
    );

  @Effect()
  get$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.GET)
    .switchMap((action: GalleryThemesGetAction) =>
      this._manager.get(action.payload)
        .map((theme: IGalleryTheme) =>
          new GalleryThemesGetSuccessAction(theme, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryThemesGetFailureAction(error, action.admin)))
    );

  @Effect()
  list$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.LIST)
    .switchMap((action: GalleryThemesListAction) =>
      this._manager.list(action.payload)
        .map((themes: IGalleryTheme[]) => new GalleryThemesListSuccessAction(themes, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryThemesListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.CREATE)
    .switchMap((action: GalleryThemesCreateAction) =>
      this._manager.create(action.payload)
        .map((s: IGalleryTheme) => new GalleryThemesCreateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryThemesCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.UPDATE)
    .switchMap((action: GalleryThemesUpdateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: IGalleryTheme) => new GalleryThemesUpdateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new GalleryThemesUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(GalleryThemesActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(GalleryThemesActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.DELETE)
    .switchMap((action: GalleryThemesDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new GalleryThemesDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new GalleryThemesDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<GalleryThemesActions> = this._actions
    .ofType(GalleryThemesActionTypes.DELETE_SUCCESS)
    .map((action: GalleryThemesDeleteSuccessAction) =>
      new GalleryThemesListAction(undefined, action.admin));

  constructor(
    private _actions: Actions,
    private _service: GalleryThemesService,
    private _manager: GalleryThemesManager
  ) { }
}
