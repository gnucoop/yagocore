import { Inject, Injectable } from '@angular/core';

import { IUser } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class UsersManager extends ModelManager<IUser> {
  private _listByThemeEndpoint: string;

  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.usersEndpoint], _apiService, []);

    this._listByThemeEndpoint = `${this.endPoint}/${_config.galleryItemsByThemeSubpath}`;
  }
}
