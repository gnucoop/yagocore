import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { ILayer, IArcGISSource, IArcGISSourceAvabilableLayer } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class ArcGISSourcesManager extends ModelManager<IArcGISSource> {
  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.arcgisSourcesEndpoint], _apiService, ['list']);
  }

  getAvailableLayers(id: number): Observable<IArcGISSourceAvabilableLayer[]> {
    return this._apiService.get(
      this._config.arcgisSourcesEndpoint, id, true, this._config.arcgisSourcesLayersSubpath
    );
  }

  getAvailableLayer(
    id: number, basePath: string, lid: number
  ): Observable<IArcGISSourceAvabilableLayer> {
    return this._apiService.get(
      this._config.arcgisSourcesEndpoint, id, true,
      `${this._config.arcgisSourcesLayerSubpath}/${basePath}:${lid}`
    );
  }

  importAvailableLayer(
    id: number, basePath: string, lid: number, data: any
  ): Observable<ILayer> {
    return this._apiService.create(
      `${this._config.arcgisSourcesEndpoint}/${id}`
      + `/${this._config.arcgisSourcesLayerSubpath}/${basePath}:${lid}`, data, true
    );
  }
}
