import {createPackageBuildTasks} from 'yago-build-tools';

// Create gulp tasks to build the different packages in the project.
createPackageBuildTasks('yago-core');
createPackageBuildTasks('yago-md', ['yago-core']);
// createPackageBuildTasks('yago-examples', ['yago-core']);

import './tasks/ci';
import './tasks/clean';
import './tasks/default';
import './tasks/development';
import './tasks/docs';
import './tasks/e2e';
import './tasks/lint';
import './tasks/publish';
import './tasks/screenshots';
import './tasks/unit-test';
import './tasks/aot';
import './tasks/payload';
import './tasks/coverage';
import './tasks/yago-release';
import './tasks/universal';
import './tasks/validate-release';
import './tasks/icons';
