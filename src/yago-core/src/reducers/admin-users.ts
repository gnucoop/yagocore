import { IAdminUsersState, IUser } from '../interfaces';
import { UsersActionTypes, UsersActions } from '../actions/index';


const initialState: IAdminUsersState = {
  listOptions: null,
  items: [],
  itemsLoading: false,
  item: null,
  itemLoading: false,
  error: null
};


export function adminUsersReducer(state = initialState, action: UsersActions) {
  if (action.admin) {
    switch (action.type) {

      case UsersActionTypes.GET:
      return Object.assign({}, state, {
        item: null,
        itemLoading: true,
        error: null
      });

      case UsersActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        item: action.payload,
        itemLoading: false,
        error: null
      });

      case UsersActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        item: null,
        itemLoading: false,
        error: action.payload
      });

      case UsersActionTypes.LIST:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: true,
        error: null
      });

      case UsersActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload,
        itemsLoading: false,
        error: null
      });

      case UsersActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        items: null,
        itemsLoading: false,
        error: action.payload
      });

      case UsersActionTypes.CREATE_SUCCESS:
      case UsersActionTypes.UPDATE_SUCCESS:
      case UsersActionTypes.RESET_CURRENT_USER:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminUser(state: IAdminUsersState): IUser | null {
  return state.item;
}
export function getAdminUserLoading(state: IAdminUsersState): boolean {
  return state.itemLoading;
}
export function getAdminUsers(state: IAdminUsersState): IUser[] {
  return state.items;
}
export function getAdminUsersLoading(state: IAdminUsersState): boolean {
  return state.itemsLoading;
}
