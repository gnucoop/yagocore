export function guessGeometryTypes(
  features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>
): string[] {
  const types: string [] = [];
  features.features.forEach(f => {
    const fType = f.geometry.type;
    if (types.indexOf(fType) === -1) {
      types.push(fType);
    }
  });
  return types;
}

export function getFirstGeometryType(
  features: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>
): string | null {
  if (features.features == null || features.features.length === 0) {
    return null;
  }
  return features.features[0].geometry.type;
}
