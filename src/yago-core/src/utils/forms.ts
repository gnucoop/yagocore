import { AbstractControl, AsyncValidatorFn } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/takeUntil';

import { IAuthService } from '../interfaces/index';


export class AsyncObservableValidator {
    static create(callback: (value: any) => Promise<any>, debounceTime = 1000): AsyncValidatorFn {
        const changed$ = new Subject<any>();
        return (control: AbstractControl): Observable<any | null> => {
            changed$.next(); // This will signal the previous stream (if any) to terminate.
            return control.valueChanges
                .takeUntil(changed$)
                .take(1)
                .debounceTime(debounceTime)
                .switchMap(value => {
                    return callback(value);
                });
        };
    }
}

export const EMAIL_REGEXP = new RegExp('^[-a-z0-9~!$%^&*_=+}{\'?]+'
  + '(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*'
  + '@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*'
  + '\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|'
  + '[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$', 'i');

export function emailValidator(c: AbstractControl) {
  return EMAIL_REGEXP.test(c.value) ? null : {
    validateEmail: {
      valid: false
    }
  };
}

export function passwordMatchValidator(control: AbstractControl): {[key: string]: any} | null {
  const value = control.value;
  const password: string = value.password;
  const confirmPassword: string = value.confirmPassword;
  if (
    password != null && password.length > 0 &&
    password !== confirmPassword
  ) {
    return {password_match: 'The passwords must match'};
  }
  return null;
}

export function uniqueUsernameValidator(
  service: IAuthService, originalValue?: string
): AsyncValidatorFn {
  let usernameSub: Subscription | null;
  const notUniqueResult: {[key: string]: any} = {username_not_unique: 'Username already in use'};
    return AsyncObservableValidator.create(value => {
      return new Promise<{[key: string]: void} | null>(resolve => {
        if (usernameSub != null) {
          usernameSub.unsubscribe();
          usernameSub = null;
        }
        if (value != null && value.length > 0) {
          usernameSub = service.checkExistentUsername(value)
            .subscribe(
              (r: boolean) => resolve(r && value !== originalValue ? notUniqueResult : null),
              _ => resolve(null)
            );
        } else {
          resolve(null);
        }
      });
    });
}

export function uniqueEmailValidator(
  service: IAuthService, originalValue?: string
): AsyncValidatorFn {
  let emailSub: Subscription | null;
  const notUniqueResult: {[key: string]: any} = {email_not_unique: 'Email already in use'};
    return AsyncObservableValidator.create(value => {
      return new Promise<{[key: string]: void} | null>(resolve => {
        if (emailSub != null) {
          emailSub.unsubscribe();
          emailSub = null;
        }
        if (value != null && value.length > 0 && EMAIL_REGEXP.test(value)) {
          emailSub = service.checkExistentEmail(value)
            .subscribe(
              (r: boolean) => resolve(value !== originalValue && r ? notUniqueResult : null),
              _ => resolve(null)
            );
        } else {
          resolve(null);
        }
      });
    });
}
