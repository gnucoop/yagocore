import { ILayer } from './layer';

export enum MapZoomDirection {
  In,
  Out
}

export enum MapPanDirection {
  Up,
  Right,
  Down,
  Left
}

export interface MapCenter {
  lat: number;
  lng: number;
}

export interface MapParams {
  panOffsetX: number;
  panOffsetY: number;
}

export type MapImageFormat = 'png' | 'jpeg_high' | 'jpeg_medium' | 'jpeg_low';

export type MapBaseLayer = 'none' | 'osm' | 'google_roadmap' |
  'google_satellite' | 'google_terrain' | 'google_hybrid' |
  'bing_road' | 'bing_satellite' | 'bing_hybrid';

export interface IMap {
  readonly id?: number;
  readonly name?: string;
  readonly layers: ILayer[];
  readonly baseLayer: MapBaseLayer;
}

export interface MapTableColumn {
  prop: string;
  name: string;
}

export interface MapTableCell {
  [prop: string]: string | number;
}

export type MapTableFilterOperator = 'and' | 'or' | 'not';

export type MapTableConditionOperator = 'equal' | 'not_equal' | 'less' | 'greater' | 'less_equal' |
  'greater_equal' | 'contains' | 'between';

export interface MapTableCondition {
  field: string | null;
  operator: MapTableConditionOperator;
  params: any[];
}

export interface MapTableFilter {
  operator: MapTableFilterOperator;
  conditions: MapTableCondition[];
}

export interface MapLayerZIndex {
  layer: number;
  zIndex: number;
}

export type MapSpatialFilterMode = 'centroid' | 'whole';
