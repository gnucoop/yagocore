import { IAdminGalleryThemesState, IGalleryTheme } from '../interfaces/index';
import { GalleryThemesActionTypes, GalleryThemesActions } from '../actions/index';


const initialState: IAdminGalleryThemesState = {
  listOptions: null,
  item: null,
  itemLoading: false,
  items: [],
  itemsLoading: false,
  error: null
};

export function adminGalleryThemesReducer(
  state = initialState, action: GalleryThemesActions
): IAdminGalleryThemesState {
  if (action.admin) {
    switch (action.type) {

      case GalleryThemesActionTypes.GET:
      return Object.assign({}, state, {
        itemLoading: true,
        item: null,
        error: null
      });

      case GalleryThemesActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        itemLoading: false,
        item: null,
        error: action.payload
      });

      case GalleryThemesActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        itemLoading: false,
        item: action.payload,
        error: null
      });

      case GalleryThemesActionTypes.LIST:
      return Object.assign({}, state, {
        listOptions: action.payload,
        itemsLoading: true,
        items: null,
        error: null
      });

      case GalleryThemesActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: null,
        error: action.payload
      });

      case GalleryThemesActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: action.payload,
        error: null
      });

      case GalleryThemesActionTypes.CREATE_SUCCESS:
      case GalleryThemesActionTypes.UPDATE_SUCCESS:
      case GalleryThemesActionTypes.RESET_CURRENT_THEME:
      return Object.assign({}, state, {
        item: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getAdminGalleryTheme(state: IAdminGalleryThemesState): IGalleryTheme | null {
  return state.item;
}
export function getAdminGalleryThemeLoading(state: IAdminGalleryThemesState): boolean {
  return state.itemLoading;
}

export function getAdminGalleryThemes(state: IAdminGalleryThemesState): IGalleryTheme[] {
  return state.items;
}
export function getAdminGalleryThemesLoading(state: IAdminGalleryThemesState): boolean {
  return state.itemsLoading;
}
