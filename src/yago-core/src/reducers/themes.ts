import { IThemesState, ILayerTheme } from '../interfaces/index';
import { ThemesActionTypes, ThemesActions } from '../actions/index';


const initialState: IThemesState = {
  listOptions: null,
  item: null,
  itemLoading: false,
  items: [],
  itemsLoading: false,
  error: null
};

export function themesReducer(
  state = initialState, action: ThemesActions
): IThemesState {
  if (!action.admin) {
    switch (action.type) {

      case ThemesActionTypes.GET:
      return Object.assign({}, state, {
        itemLoading: true,
        item: null,
        error: null
      });

      case ThemesActionTypes.GET_FAILURE:
      return Object.assign({}, state, {
        itemLoading: false,
        item: null,
        error: action.payload
      });

      case ThemesActionTypes.GET_SUCCESS:
      return Object.assign({}, state, {
        itemLoading: false,
        item: action.payload,
        error: null
      });

      case ThemesActionTypes.LIST:
      return Object.assign({}, state, {
        listOptions: action.payload,
        itemsLoading: true,
        items: null,
        error: null
      });

      case ThemesActionTypes.LIST_FAILURE:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: null,
        error: action.payload
      });

      case ThemesActionTypes.LIST_SUCCESS:
      return Object.assign({}, state, {
        itemsLoading: false,
        items: action.payload,
        error: null
      });

      default:
      return state;

    }
  }
  return state;
}

export function getTheme(state: IThemesState): ILayerTheme | null {
  return state.item;
}
export function getThemeLoading(state: IThemesState): boolean {
  return state.itemLoading;
}

export function getThemes(state: IThemesState): ILayerTheme[] {
  return state.items;
}
export function getThemesLoading(state: IThemesState): boolean {
  return state.itemsLoading;
}
