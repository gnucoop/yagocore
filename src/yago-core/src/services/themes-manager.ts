import { Inject, Injectable } from '@angular/core';

import { ILayerTheme } from '../interfaces/index';
import { ModelManager } from './model-manager';
import { ApiService, ApiServiceConfig, API_SERVICE_CONFIG } from './api';


@Injectable()
export class ThemesManager extends ModelManager<ILayerTheme> {
  constructor(
    _apiService: ApiService,
    @Inject(API_SERVICE_CONFIG) private _config: ApiServiceConfig
  ) {
    super([_config.themesEndpoint], _apiService, ['get', 'list']);
  }
}
