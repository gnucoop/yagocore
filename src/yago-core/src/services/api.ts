import { Inject, Injectable, InjectionToken } from '@angular/core';
import { Headers, Http, RequestOptionsArgs } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { IApiListOptions } from '../interfaces/index';
import { composeUrl } from '../utils/index';
import { AuthHttp } from './http';

import * as pako from 'pako';


export class ApiServiceConfig {
  baseUrl: string;
  arcgisSourcesEndpoint: string;
  arcgisSourcesLayersSubpath: string;
  arcgisSourcesLayerSubpath: string;
  wmsSourcesEndpoint: string;
  wmsSourcesLayersSubpath: string;
  wmsSourcesLayerSubpath: string;
  themesEndpoint: string;
  layersEndpoint: string;
  loginEndpoint: string;
  logoutEndpoint: string;
  authRefreshEndpoint: string;
  tokenExpirationDelta: number;
  meEndpoint: string;
  checkExistentUsername: string;
  checkExistentEmail: string;
  registerEndpoint: string;
  recoverPasswordEndpoint: string;
  resetPasswordEndpoint: string;
  mapsEndpoint: string;
  galleryThemesEndpoint: string;
  galleryThemesByParentSubpath: string;
  galleryItemsEndpoint: string;
  galleryItemsByThemeSubpath: string;
  usersEndpoint: string;
}

export const API_SERVICE_CONFIG = new InjectionToken<ApiServiceConfig>('API_SERVICE_CONFIG');


@Injectable()
export class ApiService {
  private _baseUrl: string;

  constructor(
    private _nonAuthHttp: Http,
    private _authHttp: AuthHttp,
    @Inject(API_SERVICE_CONFIG) config: ApiServiceConfig
  ) {
    this._baseUrl = config.baseUrl;
  }

  get(endPoint: string, id?: number, auth = true, subPath?: string): Observable<any> {
    return this._http(auth).get(this._generateApiUrl({endPoint, id, subPath})).map(r => r.json());
  }

  list(endPoint: string, options?: IApiListOptions, auth = true): Observable<any[]> {
    return this._http(auth).get(this._generateApiUrl({endPoint, options})).map(r => r.json());
  }

  create(endPoint: string, data: any, auth = true): Observable<any> {
    const deflated = this._tryGzip(data);
    return this._http(auth)
      .post(this._generateApiUrl({endPoint}), deflated.data, deflated.options)
      .map(r => r.json());
  }

  update(endPoint: string, id: number, data: any, auth = true): Observable<any> {
    const deflated = this._tryGzip(data);
    return this._http(auth)
      .put(this._generateApiUrl({endPoint, id}), deflated.data, deflated.options)
      .map(r => r.json());
  }

  delete(endPoint: string, id: number, auth = true): Observable<any> {
    return this._http(auth).delete(this._generateApiUrl({endPoint, id})).map(r => r.json());
  }

  private _tryGzip(data: any): {data: any, options?: RequestOptionsArgs} {
    const strData = JSON.stringify(data);
    if (strData.length > 1000000) {
      try {
        const gzipped = this._uint8ToBase64(pako.gzip(strData));
        const headers = new Headers();
        headers.append('Content-encoding', 'gzip');
        headers.append('Content-type', 'application/json');
        return {data: gzipped, options: {headers}};
      } catch (e) { }
    }
    return {data: data};
  }

  private _uint8ToBase64(u8Arr) {
    const CHUNK_SIZE = 0x8000; // arbitrary number
    let index = 0;
    const length = u8Arr.length;
    let result = '';
    let slice;
    while (index < length) {
      slice = u8Arr.subarray(index, Math.min(index + CHUNK_SIZE, length));
      result += String.fromCharCode.apply(null, slice);
      index += CHUNK_SIZE;
    }
    return btoa(result);
  }

  private _http(auth = true): Http | AuthHttp {
    return auth ? this._authHttp : this._nonAuthHttp;
  }

  private _generateApiUrl(
    params: {endPoint: string, id?: number, options?: IApiListOptions, subPath?: string}
  ): string {
    const parts = [this._baseUrl, params.endPoint];
    if (params.id != null) {
      parts.push(`${params.id}`);
    }
    if (params.subPath != null) {
      parts.push(params.subPath);
    }
    return `${composeUrl(parts)}${this._listOptionsToQueryParameters(params.options)}`;
  }

  private _listOptionsToQueryParameters(options?: IApiListOptions): string {
    let params = '';
    if (options) {
      const paramsArray: string[] = [];
      if (options.limit) {
        paramsArray.push(`limit=${options.limit}`);
      }
      if (options.skip) {
        paramsArray.push(`offset=${options.skip}`);
      }
      if (paramsArray.length > 0) {
        params = `?${paramsArray.join('&')}`;
      }
    }
    return params;
  }
}
