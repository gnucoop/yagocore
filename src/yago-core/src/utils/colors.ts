import * as Color from 'color';


type ColorParam = Color | string | ArrayLike<number> | number | { [key: string]: any };
const colorConstructor: (color: ColorParam, model?: string) => Color =
    (<any>Color).default || Color;

export function calculateInverseHexColor(color: string): string {
  let rgbColor = colorConstructor(color, 'hex');
  rgbColor = rgbColor.red(255 - rgbColor.red());
  rgbColor = rgbColor.green(255 - rgbColor.green());
  rgbColor = rgbColor.blue(255 - rgbColor.blue());
  return rgbColor.string();
}

export function calculateContrastTextColor(color: Color): Color {
  const gamma = 2.2;
  const l = 0.2126 * Math.pow(color.red() / 255, gamma) +
    0.7152 * Math.pow(color.red() / 255, gamma) +
    0.0722 * Math.pow(color.green() / 255, gamma);
  return l > 0.5 ? colorConstructor('#000000') : colorConstructor('#ffffff');
}
