import { ILayerTheme } from './layer-theme';
import { ILayer } from './layer';


export interface ILayerTreeItem {
  theme: ILayerTheme;
  layers: ILayer[];
  children?: ILayerTreeItem[];
}
