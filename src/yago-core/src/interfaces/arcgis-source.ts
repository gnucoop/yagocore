export interface IArcGISSource {
  id: number;
  name: string;
  serverUrl: string;
  service: string;
  username: string;
  password: string;
}

export interface IArcGISSourceAvabilableLayer {
  basePath: string;
  id: number;
  name: string;
  layerType: 'RASTER' | 'VECTOR';
  boundingBox: GeoJSON.Polygon;
  features?: GeoJSON.FeatureCollection<GeoJSON.GeometryObject>;
}
