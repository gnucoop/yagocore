import {Component} from '@angular/core';


@Component({
  selector: 'app-e2e-home',
  template: `<p>e2e website!</p>`
})
export class HomeComponent {}

@Component({
  moduleId: module.id,
  selector: 'app-e2e-app',
  templateUrl: 'e2e-app.html',
})
export class E2EAppComponent { }
