import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';

import { Store } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import { IUser, State } from '../interfaces/index';
import {
  UsersActions, UsersActionTypes,
  UsersCreateAction, UsersCreateFailureAction, UsersCreateSuccessAction,
  UsersGetAction, UsersGetFailureAction, UsersGetSuccessAction,
  UsersListAction, UsersListFailureAction, UsersListSuccessAction,
  UsersUpdateAction, UsersUpdateFailureAction, UsersUpdateSuccessAction,
  UsersDeleteAction, UsersDeleteFailureAction, UsersDeleteSuccessAction,
  UsersResetCurrentUserAction,
  Back, RouterActions
} from '../actions/index';
import * as reducers from '../reducers/index';
import { ModelService } from './model';
import { UsersManager } from './users-manager';


@Injectable()
export class UsersService extends ModelService<
    IUser, UsersGetAction, UsersListAction,
    UsersCreateAction, UsersUpdateAction, UsersDeleteAction> {
  constructor(store: Store<State>) {
    super(
      store, UsersGetAction, UsersListAction,
      UsersCreateAction, UsersUpdateAction, UsersDeleteAction
    );
  }

  getAdminUsers(): Observable<IUser[]> {
    return this._store.select(reducers.getAdminUsers);
  }

  getAdminUser(): Observable<IUser | null> {
    return this._store.select(reducers.getAdminUser);
  }

  getAdminUsersLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminUsersLoading);
  }

  getAdminUserLoading(): Observable<boolean> {
    return this._store.select(reducers.getAdminUserLoading);
  }

  resetCurrentUser(admin = false): void {
    this._store.dispatch(new UsersResetCurrentUserAction(admin));
  }
}

@Injectable()
export class UsersEffects {
  @Effect()
  get$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.GET)
    .switchMap((action: UsersGetAction) =>
      this._manager.get(action.payload)
        .map((theme: IUser) =>
          new UsersGetSuccessAction(theme, action.admin))
        .catch((error: any) =>
          Observable.of(new UsersGetFailureAction(error, action.admin)))
    );

  @Effect()
  list$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.LIST)
    .switchMap((action: UsersListAction) =>
      this._manager.list(action.payload)
        .map((themes: IUser[]) => new UsersListSuccessAction(themes, action.admin))
        .catch((error: any) =>
          Observable.of(new UsersListFailureAction(error, action.admin)))
    );

  @Effect()
  create$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.CREATE)
    .switchMap((action: UsersCreateAction) =>
      this._manager.create(action.payload)
        .map((s: IUser) => new UsersCreateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new UsersCreateFailureAction(error, action.admin)))
    );

  @Effect()
  update$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.UPDATE)
    .switchMap((action: UsersUpdateAction) =>
      this._manager.update(action.payload.id, action.payload)
        .map((s: IUser) => new UsersUpdateSuccessAction(s, action.admin))
        .catch((error: any) =>
          Observable.of(new UsersUpdateFailureAction(error, action.admin)))
    );

  @Effect()
  updated$: Observable<RouterActions> = this._actions
    .ofType(UsersActionTypes.UPDATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  created$: Observable<RouterActions> = this._actions
    .ofType(UsersActionTypes.CREATE_SUCCESS)
    .map(() => new Back());

  @Effect()
  delete$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.DELETE)
    .switchMap((action: UsersDeleteAction) =>
      this._manager.delete(action.payload.id)
        .map(() => new UsersDeleteSuccessAction(action.payload, action.admin))
        .catch((err: any) => Observable.of(new UsersDeleteFailureAction(err, action.admin)))
    );

  @Effect()
  deleted$: Observable<UsersActions> = this._actions
    .ofType(UsersActionTypes.DELETE_SUCCESS)
    .map((action: UsersDeleteSuccessAction) =>
      new UsersListAction(undefined, action.admin));

  constructor(
    private _actions: Actions,
    private _service: UsersService,
    private _manager: UsersManager
  ) { }
}
