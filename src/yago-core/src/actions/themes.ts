import { BaseAction } from './base';
import { type } from '../utils/index';
import { IApiListOptions, ILayerTheme } from '../interfaces/index';


// tslint:disable-next-line
export const ThemesActionTypes = {
  GET: type('[Themes] Get'),
  GET_FAILURE: type('[Themes] Get failure'),
  GET_SUCCESS: type('[Themes] Get success'),
  LIST: type('[Themes] List'),
  LIST_FAILURE: type('[Themes] List failure'),
  LIST_SUCCESS: type('[Themes] List success'),
  CREATE: type('[Themes] Create'),
  CREATE_FAILURE: type('[Themes] Create failure'),
  CREATE_SUCCESS: type('[Themes] Create success'),
  UPDATE: type('[Themes] Update'),
  UPDATE_FAILURE: type('[Themes] Update failure'),
  UPDATE_SUCCESS: type('[Themes] Update success'),
  DELETE: type('[Themes] Delete'),
  DELETE_FAILURE: type('[Themes] Delete failure'),
  DELETE_SUCCESS: type('[Themes] Delete success'),
  RESET_CURRENT_THEME: type('[Themes] Reset current theme')
};

export class ThemesGetAction implements BaseAction {
  type = ThemesActionTypes.GET;
  constructor(public payload: number, public admin = false) { }
}
export class ThemesGetFailureAction implements BaseAction {
  type = ThemesActionTypes.GET_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class ThemesGetSuccessAction implements BaseAction {
  type = ThemesActionTypes.GET_SUCCESS;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesListAction implements BaseAction {
  type = ThemesActionTypes.LIST;
  constructor(public payload?: IApiListOptions, public admin = false) { }
}
export class ThemesListFailureAction implements BaseAction {
  type = ThemesActionTypes.LIST_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class ThemesListSuccessAction implements BaseAction {
  type = ThemesActionTypes.LIST_SUCCESS;
  constructor(public payload: ILayerTheme[], public admin = false) { }
}
export class ThemesCreateAction implements BaseAction {
  type = ThemesActionTypes.CREATE;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesCreateFailureAction implements BaseAction {
  type = ThemesActionTypes.CREATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class ThemesCreateSuccessAction implements BaseAction {
  type = ThemesActionTypes.CREATE_SUCCESS;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesUpdateAction implements BaseAction {
  type = ThemesActionTypes.UPDATE;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesUpdateFailureAction implements BaseAction {
  type = ThemesActionTypes.UPDATE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class ThemesUpdateSuccessAction implements BaseAction {
  type = ThemesActionTypes.UPDATE_SUCCESS;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesDeleteAction implements BaseAction {
  type = ThemesActionTypes.DELETE;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesDeleteFailureAction implements BaseAction {
  type = ThemesActionTypes.DELETE_FAILURE;
  constructor(public payload: any, public admin = false) { }
}
export class ThemesDeleteSuccessAction implements BaseAction {
  type = ThemesActionTypes.DELETE_SUCCESS;
  constructor(public payload: ILayerTheme, public admin = false) { }
}
export class ThemesResetCurrentThemeAction implements BaseAction {
  type = ThemesActionTypes.RESET_CURRENT_THEME;
  payload = undefined;
  constructor(public admin = false) { }
}

export type ThemesActions =
  ThemesGetAction | ThemesGetFailureAction | ThemesGetSuccessAction |
  ThemesListAction | ThemesListFailureAction | ThemesListSuccessAction |
  ThemesCreateAction | ThemesCreateFailureAction | ThemesCreateSuccessAction |
  ThemesUpdateAction | ThemesUpdateFailureAction | ThemesUpdateSuccessAction |
  ThemesDeleteAction | ThemesDeleteFailureAction | ThemesDeleteSuccessAction |
  ThemesResetCurrentThemeAction;
